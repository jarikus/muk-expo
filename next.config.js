require('dotenv').config()
const path = require('path')
const withImages = require('next-images')

const nextConfig = {
  webpack: (config, { isServer }) => {
    config.resolve.modules.push(path.resolve('./'))
    const originalEntry = config.entry
    config.entry = async () => {
      const entries = await originalEntry()

      if (
        entries['main.js'] &&
        !entries['main.js'].includes('./client/polyfills.js')
      ) {
        entries['main.js'].unshift('./client/polyfills.js')
      }

      return entries
    }
    if (isServer) {
      require('./scripts/generate-sitemap')
    }
    return config
  },
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  publicRuntimeConfig: {
    localeSubpaths:
      typeof process.env.LOCALE_SUBPATHS === 'string'
        ? process.env.LOCALE_SUBPATHS
        : 'all',
  },
  env: {
    MONGODB_URI: process.env.MONGODB_URI,
    DB_NAME: process.env.DB_NAME,
    WEB_API: process.env.WEB_API,
    WEBSITE_URL: process.env.WEBSITE_URL,
    EMAIL_FROM: process.env.EMAIL_FROM,
  },
}

module.exports = withImages(nextConfig)
