export const AVAILABLE_FILE_FORMAT = [
  '.jpg',
  '.jpeg',
  '.png',
  '.pdf',
  '.doc',
  '.docx',
  '.xls',
  '.xlsx',
  '.pptx',
]

export const LINKS = {
  youtube: 'https://www.youtube.com/user/MediaMuk',
  facebook: 'https://www.facebook.com/MUKGROUP',
  telegram: 'https://t.me/MUKGroup',
  twitter: 'https://twitter.com/muk_ua',
  linkedin: 'https://www.linkedin.com/company/muk',
  login: 'https://b2b.muk.ua/login',
  signup: 'https://b2b.muk.ua/signup',
  catalog: 'https://muk.ua/upload/muk-catalog/katalog_muk_ukr.pdf',
}

export const CONTACTS = {
  phone1: '+38 (044) 594-98-98',
  phone2: '+38 (044) 492-29-29',
  fax: '+38 (044) 490-51-71',
  email1: 'info@muk.ua',
  email2: 'tellmuk@muk.ua',
  emailHr: 'hr@muk.com.ua',
}
