import journal1 from 'public/images/journals/1.png'
import journal2 from 'public/images/journals/2.png'
import journal3 from 'public/images/journals/3.jpg'
import journal4 from 'public/images/journals/4.jpg'
import journal5 from 'public/images/journals/5.jpg'
import journal6 from 'public/images/journals/6.jpg'
import journal7 from 'public/images/journals/7.jpg'
import journal8 from 'public/images/journals/8.png'

export const journals = [
  {
    id: 1,
    img: journal1,
    url: 'https://muk.ua/upload/muk-review/1/muk-review-1.pdf',
  },
  {
    id: 2,
    img: journal2,
    url: 'https://muk.ua/upload/muk-review/2/muk-review-2.pdf',
  },
  {
    id: 3,
    img: journal3,
    url: 'https://muk.ua/upload/muk-review/3/muk-review-3.pdf',
  },
  {
    id: 4,
    img: journal4,
    url: 'https://muk.ua/upload/muk-review/4/muk-review-4.pdf',
  },
  {
    id: 5,
    img: journal5,
    url: 'https://muk.ua/upload/muk-review/5/muk-review-5.pdf',
  },
  {
    id: 6,
    img: journal6,
    url: 'https://muk.ua/upload/muk-review/6/2017-06+MukBookUA+cover.pdf',
  },
  {
    id: 7,
    img: journal7,
    url: 'https://muk.ua/upload/muk-review/7/Book_2018-07-UA-120dpi.pdf',
  },
  {
    id: 8,
    img: journal8,
    url: 'https://muk.ua/upload/muk-review/8/Book_UA.pdf',
  },
]
