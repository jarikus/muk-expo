export const langOptions = [
  { value: 'ru', label: 'Рус', name: 'Русский язык' },
  { value: 'en', label: 'EN', name: 'English' },
  { value: 'uk', label: 'Укр', name: 'Українська мова' },
]
