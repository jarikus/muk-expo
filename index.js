const { setConfig } = require('next/config')
setConfig(require('./next.config'))

require('./server')
const fetch = require('node-fetch')

fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'get',
  headers: { 'Content-Type': 'application/json' },
})
  .then((res) => res.json())
  .then((json) => console.log(json))
