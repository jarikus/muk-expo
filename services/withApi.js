import React, { Component } from 'react'
import { ApiSrv } from './index'
import { i18n } from 'utils/with-i18next.js'
import { getConfig } from 'environment'

export default (WrapComponent) => {
  return class AppApiChild extends Component {
    constructor(props) {
      super(props)
      this.api = new ApiSrv({})
    }

    render() {
      return <WrapComponent {...this.props} api={this.api} />
    }
  }
}

export const withAppApi = (App) => {
  return class AppApi extends Component {
    static async getInitialProps(appContext) {
      const {
        ctx: { req },
      } = appContext
      const lang = req ? req.language : i18n.language
      const config = getConfig(req)

      const api = new ApiSrv({ ctx: appContext.ctx, baseUrl: config.apiUrl })

      appContext.ctx.lang = lang
      appContext.ctx.api = api
      appContext.ctx.isServer = typeof window === 'undefined'
      appContext.ctx.config = config

      let appProps = {}

      if (App.getInitialProps) {
        appProps = await App.getInitialProps(appContext)
      }

      return {
        ...appProps,
        lang,
        config,
      }
    }

    constructor(props) {
      super(props)
      this.api = new ApiSrv({ baseUrl: props.config.apiUrl })
    }

    render() {
      return <App {...this.props} api={this.api} />
    }
  }
}
