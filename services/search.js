export class SearchApi {
  constructor(api) {
    this.api = api
  }

  searchData = async ({query = {}, lang = 'ru'}) => {
    try {
      let url = `/search/byText?locale=${lang}&domain=muk.ua&searchText=${query}&limit_start=0&limit_count=10`
      const res = await this.api.get(url)
      if (res.error || res.code) return {}
      return { search: res.search, total: res.total }
    } catch (error) {
      console.error('Error', error)
    }
  }
}
