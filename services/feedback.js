export class FeedbackApi {
  constructor(api) {
    this.api = api
  }

  sendFeedback = async (options = {}) => {
    try {
      let url = '/api/feedback'
      options.domain = 'muk.ua'
      if (!options.options) options.options = 'cloud_microsoft'
      const res = await this.api.post(url, options)
      if (res.error || res.code) return {}
      return { res }
    } catch (error) {
      console.error('Error', error)
    }
  }
}
