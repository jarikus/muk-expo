import { VacancyModel, VacancyListModel } from 'models/vacancies'

export class VacancyApi {
  constructor(api) {
    this.api = api
  }

  getVacanciesList = async (ctx = {}, options = {}) => {
    try {
      const { lang } = ctx
      const lng = lang ? lang : ctx.res.locals.language
      const { limit_start = 0, limit_count = 10 } = options

      let url = `/jobsList?domain=muk.ua&locale=${lng}&type=1&limit_start=${limit_start}&limit_count=${limit_count}`

      if (!!options.jobsCountry)
        options.jobsCountry.forEach((item) => (url += `&country=${item}`))
      if (!!options.jobsOptions)
        options.jobsOptions.forEach((item) => (url += `&optionsP1[]=${item}`))
      if (!!options.jobsDepartment)
        options.jobsDepartment.forEach((item) => (url += `&optionsP2[]=${item}`))

      const res = await this.api.get(url)

      if (res.error || res.code) return []
      const list =
        res && res.list && res.list.map((item) => new VacancyListModel(item))
      return {
        list,
        count: +res.count,
      }
    } catch (error) {
      console.error('Error getVacanciesList', error)
    }
  }

  getVacancy = async (ctx) => {
    const {
      query: { id },
      lang,
    } = ctx
    const lng = lang ? lang : ctx.res.locals.language
    const uri = `/jobsOne?domain=muk.ua&locale=${lng}&id=${id}`
    const res = await this.api.get(uri)

    let vacancy, message

    if (!res.message) vacancy = new VacancyModel(res)
    if (res.message) message = res

    return {
      vacancy,
      message,
    }
  }

  getJobsOptions = async (ctx) => {
    try {
      const { lang } = ctx
      const lng = lang ? lang : ctx.res.locals.language
      const uriOptions = `/jobsOptions?domain=muk.ua&locale=${lng}`
      const { countries, departments, other } = await this.api.get(uriOptions)

      return { jobsCountry : countries, jobsOptions : other, jobsDepartment: departments }
    } catch (error) {
      console.error('Err getJobsOptions', error)
    }
  }

  sendBackResume = async (data = {}) => {
    const formData = new FormData()

    for (let item in data) {
      formData.append(item, data[item])
    }

    formData.append('domain', 'muk.ua')
    formData.append('locale', 'ru')
    const headers = new Headers({})

    const url = '/api/resume'

    const resp = await fetch(url, {
      method: 'POST',
      headers: headers,
      body: formData,
    })

    return await resp.json()
  }
}
