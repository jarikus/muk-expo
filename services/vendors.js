import { VendorModel, VendorListModel } from 'models/vendors'

export class VendorApi {
  constructor(api) {
    this.api = api
  }

  getVendorList = async (ctx = {}, options = {}) => {
    try {
      const { lang } = ctx
      const lng = lang ? lang : ctx.res.locals.language
      const { limit_start = 0, limit_count = 200 } = options

      let url = `/vendorsList?domain=muk.ua&locale=${lng}&limit_start=${limit_start}&limit_count=${limit_count}`
      if (options.result) url += `&result=${options.result}`

      const res = await this.api.get(url)
      if (res.error || res.code) return []

      const list =
        res && res.list && res.list.map((item) => new VendorListModel(item))
      return {
        list,
        count: +res.count,
      }
    } catch (error) {
      console.error('error', error)
    }
  }

  getVendor = async (ctx) => {
    const {
      query: { url },
      lang,
    } = ctx
    const lng = lang ? lang : ctx.res.locals.language
    const uri = `/vendors/vendorOne?domain=muk.ua&locale=${lng}&url=${url}`
    const res = await this.api.get(uri)

    let vendor, message

    if (!res.message) vendor = new VendorModel(res)
    if (res.message) message = res

    return {
      vendor,
      message,
    }
  }
}
