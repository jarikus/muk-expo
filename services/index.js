import { ApiClient } from 'utils/fetchService'
import { NewsApi } from './news'
import { VendorApi } from './vendors'
import { VacancyApi } from './vacancies'
import { FeedbackApi } from './feedback'
import { SolutionsApi } from './solutions'
import { SearchApi } from './search'

export class ApiSrv {
  constructor({ ctx, baseUrl }) {
    const api = new ApiClient(ctx, baseUrl)
    this._api = api
    this.news = new NewsApi(api)
    this.vendor = new VendorApi(api)
    this.feedback = new FeedbackApi(api)
    this.vacancies = new VacancyApi(api)
    this.solutions = new SolutionsApi(api)
    this.search = new SearchApi(api)
  }
}
