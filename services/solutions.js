import { SolutionModel, SolutionListModel } from 'models/solutions'

export class SolutionsApi {
  constructor(api) {
    this.api = api
  }

  getSolutionList = async (ctx = {}, options = {}) => {
    try {
      const { lang } = ctx
      const lng = lang ? lang : ctx.res.locals.language
      const { divisonLink, vendorLink, groupLink, groupName } = options

      // let url = `/catalog/listGroup?domain=muk.ua&locale=${lng}`
      let url = `/catalog/listGroup?domain=muk.ua&locale=ru`
      if (divisonLink) url += `&divisonLink=${divisonLink}`
      if (vendorLink) url += `&vendorLink=${vendorLink}`
      if (groupLink) url += `&groupLink=${groupLink}`
      if (groupName) url += `&groupName=${groupName}`

      const { solutions, error, code } = await this.api.get(url)
      if (error || code) return []

      return Object.values(solutions).map((division) => {
        return {
          link: division.link,
          division_name: division.division_name,
          groups: Object.values(division.groups).map((group) => {
            return {
              name: group.name,
              link: group.link,
              vendors: group.vendors,
            }
          }),
        }
      })
    } catch (error) {
      console.error('error', error)
    }
  }

  getSolution = async (ctx) => {
    const {
      query: { division, group, solution },
      lang,
    } = ctx
    const lng = lang ? lang : ctx.res.locals.language
    const uri = `/catalog/getSolutions?domain=muk.ua&locale=${lng}&divisionLink=${division}&groupLink=${group}&vendorLink=${solution}`

    const res = await this.api.get(uri)
    let solutionItem, message

    if (!res.message) solutionItem = new SolutionModel(res)
    if (res.message) message = res

    return {
      message,
      solution: solutionItem,
    }
  }
}
