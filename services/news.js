import { NewsModel, NewsSingleModel } from 'models/news'

export class NewsApi {
  constructor(api) {
    this.api = api
  }

  getNews = async (ctx = {}, type, options = {}) => {
    try {
      const { lang } = ctx
      const lng = lang ? lang : ctx.res.locals.language
      const { limit_start = 0, limit_count = 20 } = options

      let url = `/newsList?domain=muk.ua&locale=${lng}&type=${type}&limit_start=${limit_start}&limit_count=${limit_count}`

      const res = await this.api.get(url)
      if (res.error || res.code) return []

      const list =
        res && res.list && res.list.map((item) => new NewsModel(item))
      return {
        list,
        count: +res.count,
      }
    } catch (error) {
      console.error('error', error)
    }
  }

  getSingleNews = async (ctx) => {
    const {
      query: { id },
      lang,
    } = ctx
    const lng = lang ? lang : ctx.res.locals.language
    const res = await this.api.get(
      `/newsOne?domain=muk.ua&locale=${lng}&id=${id}`
    )
    let news, message

    if (!res.message) news = new NewsSingleModel(res)
    if (res.message) message = res

    return {
      news,
      message,
    }
  }
}
