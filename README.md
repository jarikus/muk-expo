## Demo - https://muk-app.herokuapp.com

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/zeit/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Run Production

```bash
npm run build
npm run start
```
## Heroku CLI

```bash
$ heroku login
$ heroku git:remote -a muk-app

$ git add .
$ git commit -am "make it better"
$ git push heroku master
```

# Existing Git repository

```bash
$ heroku git:remote -a muk-app
```
