const dev = process.env.NODE_ENV !== 'production'
import { BASE_URL } from 'environment'

export class ApiClient {
  constructor(ctx, baseUrl) {
    this.baseUrl =
      baseUrl || dev
        ? (process.browser && window.location.origin) || 'http://localhost:3030'
        : BASE_URL.API
    this._ctx = ctx || {}
  }

  _request({ method, url, data, config = {} }) {
    return new Promise((resolve, reject) => {
      let payload = {
        method,
        headers: {
          ...config.headers,
        },
        ...config,
      }

      if (data) {
        if (payload.method === 'GET') {
          url += '?' + stringify(data)
        } else {
          payload.body = JSON.stringify(data)
        }
      }

      fetch(this.baseUrl + url, payload)
        .then((response) => {
          if (response.status === 401) {
            throw new Error()
          }
          return response.json()
        })
        .then((json) => {
          if (json) {
            resolve(json)
          } else {
            reject('Error')
          }
        })
        .catch((err) => {
          console.error(err)
          reject(err)
        })
    })
  }

  get(url, params, config) {
    return this._request({
      method: 'GET',
      url,
      data: params,
      config,
    })
  }

  post(url, body, config) {
    return this._request({
      method: 'POST',
      url,
      data: body,
      config,
    })
  }

  put(url, body, config) {
    return this._request({
      method: 'PUT',
      url,
      data: body,
      config,
    })
  }

  patch(url, body, config) {
    return this._request({
      method: 'PATCH',
      url,
      data: body,
      config,
    })
  }

  delete(url, body, config) {
    return this._request({
      method: 'DELETE',
      url,
      data: body,
      config,
    })
  }
}
