import React, { Component } from 'react'
const __NEXT_CACHE_STORE__ = '__NEXT_CACHE_STORE__'

class Cache {
	constructor(data) {
		this._data = data || {}
	}

	set(key, value) {
		this._data[key] = value
	}

	get(key) {
		return this._data[key]
	}

	getAll() {
		return this._data
	}
}

function getOrCreate(initData) {
	const isServer = typeof window === 'undefined'

	if (isServer) {
		return new Cache(initData)
	}

	if (!window[__NEXT_CACHE_STORE__]) {
		window[__NEXT_CACHE_STORE__] = new Cache(initData)
	}
	return window[__NEXT_CACHE_STORE__]
}

export function removeCache() {
	if (typeof window !== 'undefined') {
		window[__NEXT_CACHE_STORE__] = new Cache()
	}
}

export default App => {
	return class AppCache extends Component {
		static async getInitialProps(appContext) {
			const cache = getOrCreate({})

			appContext.ctx.cache = cache

			let appProps = {}

			if (App.getInitialProps) {
				appProps = await App.getInitialProps(appContext)
			}

			return {
				...appProps,
				initialCache: cache.getAll(),
			}
		}

		constructor(props) {
			super(props)
			this.cache = getOrCreate(props.initialCache)
		}

		render() {
			return <App {...this.props} cache={this.cache} />
		}
	}
}
