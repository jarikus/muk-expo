import { parseISO, format } from 'date-fns'

export const createMarkup = (desc) => {
  return { __html: desc }
}

export function formatTime(data) {
  try {
    return format(parseISO(data), 'yyyy/MM/dd')
  } catch {
    return data
  }
}

export function truncate(string) {
  if (typeof string !== 'string') return string
  if (string.length > 120) {
    string = string.substring(0, 120) + '...'
    return string
  } else {
    return string
  }
}

export function debouncedDecorator(fn, delay = 200) {
  let timerId
  return function (...args) {
    if (timerId) {
      clearTimeout(timerId)
    }
    timerId = setTimeout(() => {
      fn.call(this, ...args)
      timerId = null
    }, delay)
  }
}

export function throttleDecorator(func, delay = 200) {
  let timeout = null
  return function (...args) {
    if (!timeout) {
      timeout = setTimeout(() => {
        func.call(this, ...args)
        timeout = null
      }, delay)
    }
  }
}

export function validateEmail(email) {
  try {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
  } catch (error) {
    console.error('validateEmail', error)
  }
}

export const jsonParse = (json) => {
  try {
    return JSON.parse(json)
  } catch (error) {
    console.error('jsonParse', error)
    return json
  }
}

export function filteredArray(arr) {
  try {
    if (!Array.isArray(arr)) return arr
    return arr.filter((el) => el)
  } catch (e) {
    console.error('filteredArray', e)
    return arr
  }
}
