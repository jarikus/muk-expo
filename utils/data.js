import humanWork from 'public/images/ico-human-work.png'
import certificate from 'public/images/certificate.jpg'
import server from 'public/images/fake-data/server.png'

import profesion1 from 'public/images/training/1.jpg'
import profesion2 from 'public/images/training/2.jpg'
import profesion3 from 'public/images/training/3.jpg'
import profesion4 from 'public/images/training/4.jpg'
import profesion5 from 'public/images/training/5.jpg'

import imgEducationalCenter from 'public/images/training/review-preview.jpg'
import courseProgramPreview from 'public/images/training/preview-course.png.jpg'

import businessTeam from 'public/images/fake-data/business-team.jpg'
import magazine from 'public/images/fake-data/magazine.png'

import brand1 from 'public/images/training/brands/u842.png'
import brand2 from 'public/images/training/brands/u843.png'
import brand3 from 'public/images/training/brands/u844.png'
import brand4 from 'public/images/training/brands/u845.png'
import brand5 from 'public/images/training/brands/u846.png'

import division1 from 'public/images/divisionBG/division1.jpg'
import division2 from 'public/images/divisionBG/division2.jpg'
import division3 from 'public/images/divisionBG/division3.jpg'
import division4 from 'public/images/divisionBG/division4.jpg'
import division5 from 'public/images/divisionBG/division5.jpg'
import division6 from 'public/images/divisionBG/division6.jpg'

import courseImg from 'public/images/training/autocad-course.jpg'
import unityConcept from 'public/images/training/unity-concept.jpg'

import brand from 'public/images/service/brand.png'
import master from 'public/images/service/master-lomaster.jpg'

export const tabs = [
  {
    name: '',
  },
]

export const tabsMap = [
  {
    idx: 1,
    title: 'Украина, г. Киев, ул. Донецкая, 16/2 03151',
    phones: ['+38 (044) 594-98-98', '+38 (044) 492-29-29'],
    emails: ['info@muk.ua'],
    active: true,
  },
  {
    idx: 2,
    title: 'Украина, г. Киев, ул. Донецкая, 16/2 03151',
    phones: ['+38 (044) 333', '+38 (044) 5808080808'],
    emails: ['info@muk.ua', 'info@muk.ua'],
    active: false,
  },
  {
    idx: 3,
    title: 'Украина, г. Киев, ул. Донецкая, 16/2 03151',
    phones: ['+38 (044) 594-98-98'],
    emails: ['info@muk.ua', 'info@muk.ua'],
    active: false,
  },
  {
    idx: 4,
    title: 'Украина, г. Киев, ул. Донецкая, 16/2 03151',
    phones: ['+38 (044) 99999999', '+38 (044) 8888888'],
    emails: ['info@muk.ua'],
    active: false,
  },
]

export const tags = [
  'Вычислительная инфраструктура ЦОД',
  'Сетевая инфраструктура ЦОД',
]
export const tagsLink = [
  '/solutions/computing-data-center-infrastructure',
  '/solutions/data-center-network-infrastructure',
]

export const catalogItems = [
  {
    icon: humanWork,
    links: [
      'SAN инфраструктура',
      'Гиперконвергентная инфраструктура HCI',
      'Дисковые системы хранения',
      'Ленточные библиотеки и накопители',
      'Серверы non-x86 архитектуры',
      'Серверы x86 архитектуры',
    ],
    catalogLinks: [
      '/solutions/computing-data-center-infrastructure',
      '/solutions/data-center-network-infrastructure',
    ],
  },
  {
    icon: humanWork,
    links: [
      'Беспроводная сеть (Wi-Fi)',
      'Интернет вещей (IoT)',
      'Коммутация (switching)',
      'Маршрутизация (routing)',
      'Оптическая сеть',
      'Программно-определяемая сеть',
      'Сетевой менеджмент',
    ],
  },
]

export const divisionBG = [
  {
    img: division1,
  },
  {
    img: division2,
  },
  {
    img: division3,
  },
  {
    img: division4,
  },
  {
    img: division5,
  },
  {
    img: division6,
  },
]

export const certificates = [
  {
    image: certificate,
    name: 'ЛОЩИНА',
  },
  {
    image: certificate,
    name: 'ЛОЩИНА',
  },
  {
    image: certificate,
    name: 'ЛОЩИНА',
  },
  {
    image: certificate,
    name: 'ЛОЩИНА',
  },
]

export function responsiveCard(data = {}) {
  const { desktopItems = 1, tabletItems = 1, mobileItems = 1 } = data
  return {
    desktop: {
      breakpoint: { max: 3000, min: 1200 },
      items: desktopItems,
      slidesToSlide: 1,
    },
    tablet: {
      breakpoint: { max: 1199, min: 768 },
      items: tabletItems,
      slidesToSlide: 1,
    },
    mobile: {
      breakpoint: { max: 767, min: 0 },
      items: mobileItems,
      slidesToSlide: 1,
    },
  }
}

export const products = [
  {
    id: 1,
    name: 'Продукты AMD',
    list: new Array(8).fill({
      name: 'Сетевые хранилища',
      link: 'network-storage',
    }),
  },
]

export const listCategories = new Array(6).fill({
  name: 'Серверы напольного исполнения',
  description:
    'Мощные, надежные, устанавливаемые на полу серверы для малых и средних предприятий',
  link: '/category/servers/sc440',
  image: server,
})

export const forFromCourseList = [
  {
    id: 1,
    img: profesion1,
    title: 'Инженеров и проектировщиков',
  },
  {
    id: 2,
    img: profesion2,
    title: 'Дизайнеров интерьера',
  },
  {
    id: 3,
    img: profesion3,
    title: 'Строителей и мастеров по ремонту квартир',
  },
  {
    id: 4,
    img: profesion4,
    title: 'Мебельщиков',
  },
  {
    id: 5,
    img: profesion5,
    title: 'Студентов профильных ВУЗов и техникумов',
  },
]

export const reviewEducationalCenter = new Array(4).fill({
  image: imgEducationalCenter,
  title: 'ФИО или название компании',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
  review:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
})

export const courseProgramList = new Array(3).fill({
  image: courseProgramPreview,
  title: 'Урок № 1 - Знакомство с программой',
  description: [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.',
  ],
})

export const staffGallery = new Array(6).fill({
  title: 'Название фото',
  image: businessTeam,
})

export const magazineGallery = new Array(6).fill({
  title: 'Название газеты',
  image: magazine,
})

export const vacancyCategories = new Array(6).fill({
  title: 'Информационные технологии',
  list: [
    {
      name: 'Администратор',
      link: '/administrator',
    },
    {
      name: 'Ведущий консультант',
      link: '/consultant',
    },
    {
      name: 'Администратор',
      link: '/administrator',
    },
    {
      name: 'Ведущий консультант',
      link: '/consultant',
    },
    {
      name: 'Администратор',
      link: '/administrator',
    },
    {
      name: 'Ведущий консультант',
      link: '/consultant',
    },
  ],
})

export const brandsList = [
  {
    icon: brand1,
    link: '/',
  },
  {
    icon: brand2,
    link: '/',
  },
  {
    icon: brand3,
    link: '/',
  },
  {
    icon: brand4,
    link: '/',
  },
  {
    icon: brand5,
    link: '/',
  },
]

export const courseList = new Array(4).fill({
  name: 'AutoCAD Civil 3D 2018/2019 – Углубленный курс (Генплан)',
  image: courseImg,
  category: 'autocad',
  link: 'autocad',
  date: '15.05.2020',
  price: '15 000 грн.',
  oldPrice: '65 000 грн.',
  sale: true,
})

export const categoryCourseList = new Array(6).fill({
  name: 'Курсы по конфигурированию и инсталляции оборудования',
  image: unityConcept,
  link: 'autocad',
  list: [
    'Серверы стандартной архитектуры',
    'Системы хранения данных',
    'Блейд-системы',
    'Курсы по сетевым технологиям',
  ],
})

export const newsItMarketStudy = new Array(6).fill({
  name: '7 НОВЫХ курсов по vSphere 7',
  date: '15/04/2020',
  description: 'VMware анонсировала выход новой версии vSphere',
})

export const servicePriceList = [
  {
    id: 0,
    name: 'Диагностика',
    ot: false,
    price: '270.00',
  },
  {
    id: 3,
    name: 'Профилактика системы охлаждения с заменой термопасты',
    ot: false,
    price: '450.00',
  },
  {
    id: 4,
    name: 'Нстройка, установка программного обеспечения',
    ot: false,
    price: '450.00',
  },
  {
    id: 5,
    name: 'Модернизация компьютера',
    ot: false,
    price: 'от 250.00',
  },
  {
    id: 6,
    name: 'Замена материнской платы',
    ot: false,
    price: 'от 700.00',
  },
  {
    id: 7,
    name:
      'Замена видеоплаты, процессора, вентилятора, модулей памяти с диагностикой после замены',
    ot: false,
    price: 'от 250.00',
  },
  {
    id: 8,
    name: 'Смена пароля',
    ot: false,
    price: 'от 1300.00',
  },
  {
    id: 9,
    name: 'Замена кнопок на клавиатуре',
    ot: false,
    price: 'от 250.00',
  },
  {
    id: 10,
    name: 'Замена экрана',
    ot: false,
    price: 'от 690.00',
  },
  {
    id: 11,
    name: 'Замена жесткого диска, планки памяти',
    ot: false,
    price: 'от 230.00',
  },
]
export const serviceBrandList = [
  {
    id: 0,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 1,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 2,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 3,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 4,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 5,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 67,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 7,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 8,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 23,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 234,
    image: brand,
    url: 'https://google.com/',
  },
  {
    id: 345,
    image: brand,
    url: 'https://google.com/',
  },
]
export const tariffs = [
  {
    title: 'Сервис 7x24x6/8',
    region: 'Киев',
    description:
      'Устранение неполадки 7 дней в неделю, 24 часа в сутки, предоставление подмены или устранени неисправности в течении 6 часов с 9:00 до 18:00  8 часов с 18:00 до 9:00',
    price: 5000,
    hit: false,
  },
  {
    title: 'Сервис 7x24x6/8',
    region: 'Киев',
    description:
      'Устранение неполадки 7 дней в неделю, 24 часа в сутки, предоставление подмены или устранени неисправности в течении 6 часов с 9:00 до 18:00  8 часов с 18:00 до 9:00',
    price: 5000,
    hit: true,
  },
  {
    title: 'Сервис 7x24x6/8',
    region: 'Киев',
    description:
      'Устранение неполадки 7 дней в неделю, 24 часа в сутки, предоставление подмены или устранени неисправности в течении 6 часов с 9:00 до 18:00  8 часов с 18:00 до 9:00',
    price: 5000,
    hit: false,
  },
  {
    title: 'Сервис 7x24x6/8',
    region: 'Киев',
    description:
      'Устранение неполадки 7 дней в неделю, 24 часа в сутки, предоставление подмены или устранени неисправности в течении 6 часов с 9:00 до 18:00  8 часов с 18:00 до 9:00',
    price: 5000,
    hit: false,
  },
]

export const Master = {
  images: [
    {
      image:
        'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8PDxIQEBAQEA8NDw8ODQ8PEBAPDxAPFRIWFhURFRUYHSggGBomGxYVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGC0dHSAtKy0rLS0tKy0rLSstLS0tLS0tLS0tLS0tKystKy0tLS0tLS0tLS0tLS0tLS0tKy0tLf/AABEIALcBEwMBEQACEQEDEQH/xAAbAAEBAAIDAQAAAAAAAAAAAAAEAwACAQUGB//EADwQAAICAQEEBgYIBgIDAAAAAAECAAMRBBIhMVEFEyJBYXEGUoGRobEUMlNyosHR8CNCQ4KS4WJjM7LC/8QAGgEAAwEBAQEAAAAAAAAAAAAAAAECAwQFBv/EADMRAQEAAgEEAQMCBQIFBQAAAAABAhEDBBIhMUEFE1EiYTJxkaGxFIEjQsHR4QYVQ1Lw/9oADAMBAAIRAxEAPwD5bPRecyAZAMgGQDIBkAyAcgQCgEZNwIEoqxkqqwJVVjJVVgSqpGlVUgW1VSBKrXGTda4BQVwDYVwJsK4Bz1cBtnVwG3BrgGprgGprgaZrgE2rgE2SI0mSBpMkDSZYlJ7MDdRIasgGQDIBkAyAZAOQIBuogSiiMlFECVVYyVVYyWRYJWRIy2sqQJZUglVUjCq1wJZEiCzVgnIG7id24f6gKwVwJsK4Bz1UAzqoBwaoBqa4Boa4BNq4zSauASZIGkyRGiyQNF0gaRWBuimbdkAyAZAMgGQDIBuBAtqKIyUUQJVVjJZVjJZVgldEjJZEgldEjJdEgFkrgSy1xERp1AOSu0OR4RUEKFIOFAfa2gc4UKATs4MD3twyZ388n9YvgnOi6q+s2VWB1ywBG8EA4Jz5yZnNbXcLLq+3PVS0M6qAcGqAaNXANGrjCTVwCTVwCL1xhF0gpB0gEHSI0ikD283M3SyAZAMgGQDIByBAKKIEqojJVRGlVVjJdFgS6LGmrosEkIkZEIkCXRIgulcCXWuAc9RZtgqCysMOnEjH8y+ziP2VJunbJPJtenzjBG/uz8YtlPM3BekNBZeBQpKV3g9bYBv6sEZRP+RO7PcMyseP7l/aMeo62dJh32bvxFujehV0fYQfwC+dnPaVjxAz3HHsPnNOTixxm8PDn6T6neqy7eSayMdF34BG/s5PAcjMI72nVR7DU1QJoaoBNq4BJq4wi9cYReuAQdIAd0jNB0gaJWI3lJm6mQDIBkAyAZAN1ECUURksojJVBAl0WNK6LGkhFgRFawSRWkZE1pERFaQBCVxAhKoA8js7O1tBQVTGFOcg7xxIk+d+FWSzV9IgYO8YBOPae+bZTundHBxZ/Z5PtZevj/st0eqiy9i4DoatkEkdkLnHvOY+Der/ADc31jj8Y5ft/wBaXqj1uWLAbiBgbJPJ/Pfw8N807NzTyePq7xZ981ll8pafTk0iwnLbbVPgYG0uDkeYIOPGcnJOzK4vq+HKcvHjyT5ZsbpO1tTVHstNDVGEmrgST1xhB64wg6QA7pGB7EgB3SM0CsDeNmTrZAMgGQDIByIBRRGSqiCVkEZVZFjSuixkQiwSTWsEk1pGRNaRAmtIEVWkQJqTwiBWyCTuxEaqVxGodJtLncR9XBIGc9x8JXHydtc/U9P93Hx4s9fzeY6S6Vr02rBuW4qEWo2V5sqBz2TaAMq5Jx44Blf6icWVnuXyMuk5es6bGes5dO4t9J6LCRVVqGsYKAv0dw5OcdndgDhnfulcfU4e9vK5voXU91upr+btOi9O4TD5DWEsykhtnOOznwAG/wA5ly8kzz7o9vpuD7XHjx7Pu0GwwDEAHvGDiY457je4do1ib+Ocbh5S4ixJq4y0k1cEpGknHicDuj2ND21YJHI4MeyHdIwNYkYGsSMDWJGECsD28NMnYyAZAMgGQDdRAVRRGlVBGSyCMquggmkoI0kVrBJNaxkVWsCKrSIiq0iBVaQBNaSQVTVk43b+ZwPfErRahdkDZ7WTls8RyxJUoVyc49nGI1NRUlq7LohGwEPZA2gDkbR75PbNr77pv9N0u2yW6itLgFYo7bJ7R3eG+Rbq6a48XdN6VA5YwcHPHPkZW4y1dstqbOGyD4wmvgXfymKt43Zj2Wk9UErBL5U5woPEnljiT4COW30Vmvfh1FF11t3ZRBphWrByWNtljD/x7O7YK9/HiBzhvLevSsscZj73XY6piVCFQCmRwwfbK0i0CxJUZ0axJRDWJGBrEgBbEjA5WMPATJ2sgGQDIByIBusCVURkqgjJdBGmkII0k1iCSaxGkmtYEXUsRF1LAFVrEC60iBVaSaqGVVDvPPd3ybVSF0afA2+zgHgd+fDEna5ipXVkE7txHnEem6pEenndR0JRqNRZfam1tBa0DZxspu2uON5z3cMTs4uHHW7NvE6/6vnx37XFda90nRdB/RF29Lla2Y5oa1jUxxwAbPV7+9cScuPDzJPLPpvqfUfoy5dXG3X7vRrXtbJO7aALZIJB9U4zwnFLuPo9er8DdI6xazkjLHs1Ipxw79/BR+c04+O5eIw5+fDhxueV8f5F1SLZhsFrymCx3Cs534G/E6sJ9vy8jPlvVZSSX9/2cV6YIoUcFHlk95PiTk+2c9y3dvWxx1JE7EgWh7ElJotiRkNYkohbFjIS1YwOVjN86mTtZAMgGQDkQCixkqohErIIyXrEaaTWI00msQSVWIyKqWBF1LERdSxAypYgVUskzKd3vBz3xVULGWJJ4njJVoqivI7ye4DhjmZNXjFVTwk7Vpxq7RVWzkE7A4DizZwAPMkCPHG5WSFllMMbll4gNLswyQqtxYKzFc+3j7p6ExuPivi+o6ni6jkuWOOnD7ZzsgEqC3ZI3Y78GVrGe2XHO/L9N3p1XR/pFXS+oS0HbJrsqr/qW2MpBVFPEdlSeWSTznHz4f8AG7cX2vS8k/0mOeV9fLfTl7H6y5gjuR5IO5R4D2zux48ePHWL5fqusvVc0k9b1HfUaOyvbLuW6xtpfu43HzPH3TzuTOZXw+k6Xgy4uPWd3fy3Kc84kOjQ9lZHESpUWDWj4RxNFsWVE0W1ZSRLVjIS1ZQGKwD5rM3cyAZAMgGywCixpqywJZIyIrEaaRWI00qsQSVWIyLqEEl1CIGVLEDKlk0F1LEcLrWSqFVLJXI6T0o6du6Neu7ZNums/hsir21sGScNzK8Ad3ZPDMxzy7XXxcUzmt+Xo+iukK9TStlT7Vb4YDeCCRnBB4HBlSys7hcbqr9K6YWUFVBLjDhez2ipDBd+MbxxzL4s+zOVl1HFOXiuE+Zp5i7pOins2siONzIwy4PLHGenlyYa3t8Z/wC1dXeTtwwtSbXavUVM2j0zsoVitlv8NWIHBVzluU8/l6vfjjm/3e90X/p7t/V1Ofj8R0T9AX6G7S6m5LtTdqa2GqZUNnUucYRVQdkDdv8APlM+HluPLMsq9XrOlnL0eXFw+Neo9pptIqKb9W4qqQKyK5IO1ndtDnwwvHM6ufqprWDyvpv0f7eXdy/xOiT06Op1Z09Wxp0RyjXahWd22Tg4VdyjceOfZPO7/wAPpMeDc3a9dpH26q3yuXrRyyg7OSoOQOU0x8xy8k7azU2M5yxyQMeyXIzyu6FYspnR7FGPGVE0O1ZUTRLVlEJasZCkRh8xmbuZAMgGQDdYEosZVZRGSyRppCQTSaxGmlViNJdQgRdQiIyoRAyoQBlQk0GVCSqGqo7vCTVkViSqNOlND19DVglXwWqcfWSwfVYZ8fmZFbceXbXjuhujbdKU1GjOwWGdTpbVCLcDx2sfVsB4MMjju3zry6fG+cfH/Vwcn1Hs5LhyTePxfw+i9H6uq6tbEB7w6PuKuOKkDvH+xunHlLjdV6GOWNkywLWs8eUnwub9ys2YQaQ12qSip7bDs11KWc8TjwHec4GPGEl349h8713Qmo6afrdXaaNKna0+jQ9oDm53g2EcTwHAd5nT/pp4uccGf1TDGZTi82fLbU+hVNVRWtHQlDsiohWbI3bdjcR4ZA851fb47j24xy8H1Tnw5JeW+3pfRYH6DQpXZaqoU2LnOLK+w5zyJBPtnB2dlserlZn+qeq7BxHGdFsEqIotglRFEtEpNEtEog7RGQxEYfLZm7mQDIBkA3WBKrGSqRkQkaavXBJVcaaVUI0lVCBUyqIjKoiMqEDOqO4CRTKqERmViRVwuofrJq4UpG7dw4+MmtHRWV9XY6ZJG2XXaHc52sZ8yR7J38dlwj5/6nO3l3+XFWrfTvtqNqsnN6AZYrj6y/8AIfEbuUnn4++d3yPp3W/az7OT+G/2ew0+oDKChBVwGUg5UjGQZ5r6eXU8OTvPDEfovF9eHjvSHpJ79cNMh2dPplR9ZsgZssfetQPdhRk/enV0vF3ZXL8f5eb9V6zHg4ZPnL+ul6gFJxnHdnuHcJ6F3Z5fG3Ozdl8X0pZcTuLEkbgB3fpImMi51Fy13Xek+i6TXc2FfZ1A2nJJKixe/BPEg4z/AMB4THqMJZ3R9J9L6q543jy+PTtHX4cZxvVrW6gHLKDsKBtHvBxHKVjrbRNIyolojiaHaJSRLRKIUiMPlczdzIBkAyAbrAlVjJZIyXSNNISCSa40Uur5DMAWkE0uqBGVREZVEZlUmgyuSuGVxU4ZpwMyK1x1s+xV3bJzkSI1snwlrOjjYoKkBl47S5XZ5GacXL23VcnWdH97HunuOgGCOW7O/hjmPCd0y8bfM5cVwvbZr8nej2q2G6gnsvl6Dybi1f8A9D+7lOPn49Xunp9N9O5/ucXZfcemsOF3jAHE/nOR6V3Zqvn3o8ara31DOQ+ptu1LbQ3EFv4aj+0KJ6nB348c8e/L5761x8fN1Hbctds/w7Gk+8nLEcM8hOivmctXxPSuWzisb/WIyfd3e2T4nnKtMMLctYTZWjqVD2rA1jbgNsE+QE5Ofk7vEmo+m+ndFeGd2V3lf7GNSSM43Cc3c9TtodhI3Z3Hj4y4ztodsqMxLZUTQ7ZSRLZRCmMPlUzdzIBkAyAbrAlVjJZIyXSNNISCSq+88hGktc4O/hx3Ae6BF1nO8bhwG7JJgRdfcN2cZJ7oiLrbHHePLBiBlWefdnGN0AXW3DuBHLO/lJpwscG8NmSqOw0tuycYDdkg7Q+Miza8bpZGI3HfkHB4HhFVQrToxGc7gMnykVpIQCzZAJG7GfEjdEvG69vNPqesB2GOdndtjabPAgkd+czuwx7Zt811XNMubLGzztLWUurqte31uAwOzvV1Gdw78ePHfK/jw1krp87wc87d2O61XTBt6OsuXezUXDZHFbQhBX3zzM8dbj6nDzlP3eR9HqTZSigErRWu0oAztqBndyHz8p7kzkxx3+HzX1Lp+XPPO4+7a7nSpa52VAGQd7bvdz+A84Z5STb5/h4J39vz+FqqObM+OIyAufECRdK/1GUvbjNT8QbpvRjUbur2CmGr2GKmt14OuMEEcY8cce3WV20w67lx5/uYTX5n7Oz9H+lHs0+y+esqIpvPEPYqjLg94OQfb4TzeTj7c7H2XFzzlwmePqqWGETRbsSoih2yoiiWyiDtlEMYw+UzN3MgGQDIBusCUWMlkjJdI00iuCSqj/uNJdZ8z4HHxPfAi6jvzwPfuBEC2WhyRv4L+ZiIur3/AAENFsuod2d3kM++IbMTfu4DlJPZlff44+EnStkV5zx3gcfbJXDaeOTv8OAk1WNMKleyeQ4HIxJ9tL4qlS8N+OfjEqOi19CU3OqHZXZNqZ3bQPFU8QxO6dvDl3Yar5/6rwXHl+5jdRzoR1x7RZeJ6w8eHLMfJOyTXlydNl9zLduv3eP9I9ZdoHems7en1y2V2qFJNLNWR1wxw4Ln7vtnN1Mt1l+X0v0vn78bhfeP+CfQXpALQNlydsFcldnO/gSdxbh+U7eLGcnHLfh5H1jn5eLmsx9V6ql1YZZlHZITZYElubc48pr08HGzk3eT8eNfn902pvXHaX/FpcyxyZXjyws3PLi2vUKOsypZyVOFbaxjjx4QmWFvbr02mHJjPu/N/q46AoKm8tYMua3WnOCMAhrADv39kHu7I5zm6vXdP5PpvpnnppPxf8nWGc7sothlRFEtMqJolpjIO0yiGJjD5XM3cyAZAMgG4gFFjSqkZEJGmrpAiazGilVmNJdRgRlTREXUYEXUYgZUZNMuoyTLraJcJraTVSko0mxcvnddgLUK44EAnPM8pl23bbumnVdN6N7UQrxrYts43uuCCoPng+ydPBn2ZfzcPXdP9/i7P9wTSGX+GwJCbTjONnw8TOmZaurHznJ01xkmF8yOl6UU9dVSHaiy19hrK9kuA42ODAjvPdOPreTXZPy97/0/wZdvLllPM08poRZ0Mx01zOby5as1qepdO/JYbzuVt2cAkbjOX7ufF5xe7n03F1UmOeO/3+Y9L0d6RU7eyxWtv5RtbVTeCv3HwM9Tg6zDmmr4r4/6j9A5+ly78P1T+/8Au9RT0rllZlzuGBjjy/Ymt4Jr9Nefj1mffLnjvSGp6WYNuAwhyFO7BzyMrHgmlTrblnJlj4iHQmrr1DXXLksLDVkjAUYBZUPAjPy8Jzc/jUfSdNjcePfru8uxucYGBvGcnPGYRvbBLGlMxLTKIS0ykiWmMDExh8tmbuZAMgGQDZYBRY01ZYEskaaQhjSTWYypNZjiSqjBJdRgRdRiKl1NEDKmiBdbSVQqtojJraSqEI0StkVsM7+HfjlJsaSkuV4qSQc4B4gSYq6eZ6d0n0YWXKxakt1to2e1UTxIPHYz7vLh18PLLNZ+Hk9f9PvNe7i9vJ+jmq+n9JK2SakAYHO/FeW4+LETg6nL7nNNeo93oennRdLcbfN9vUek/oydR0hotYgDGhjXqQdkqaCGOSDx4uP7h7HcZSw5O3H9ydd6KaY13ChUqe6vq8kF0HD6oz2eWR7pnlwT3GvH1eXrPzHhtP0B03pyVq7SVHsBrayp5FMnI+E34ebmw8e4x6jp+i5vOU8vUHo3XXV6dbmqXJZ9af6oywOwpXO1kbs7seM6sepym/GnnZ9B00svvTvlVUUKoCqowqgYAExttu21qNjSkUaxoyFsaUkW1oyDtaMDkxh8wmbuZAMgGQDkQJVYyUWMl0gS6RppFcaaVXGkmuCS6swIurMRF1QIurMRlV5iplV5kmSmZKoQmYlL1kyVRUExHtRScYxxhYqVuNyhQoUeAAzJ0q5XWmpzK0hqxMAm2Yy2k+Y4naD5jTR3zKSNZmOFRrMykiWGMC2xgY5gHzWZu1kAyAZAOVgFVjJVBGS6CNNIrEEk1iOJpNYjSVWIEXUIJLqECMqEQMqWIF1LJMqtYlaJrWSZCJFVaXRJK5FeriVoLW63qxuAz4x9uy7sZ7o+h6XLtsufcCB8plbqt+2ZTwe+rQHBKgnhvG+V3IuF/Cep11dYyzKPNlH5x90Lst+HXDppGOEKtywy/rHLaWWOOPukJq1bduz3jI3S9VlbPhs4jTR7BGkawSiFsEZCWiMhbFjCOxGHzCZO5kAyAZAORGFFMCWTEaV0x+8xlSa9n95jTSq9n95gilVbPh+KNJlZHh+KBF1Y8PxQIyrHh+KKkXV5L+KIF1eS/i/SIy618F/F+kkyq18F/F+kVVCq6/BfxfpJtVIVXV4D8X6SbVyLrWRv/fxk7Vq+0b7gOLY/wH5zTHC1zcvNMfddJ0im4nbIH9uPfNe3x5cF6jDump5dOl/ELfsH1WsJB/xnJlju+Hs8fNqTu9Irq79rAFp5HZssQ+WDn3yZjyTxI0y5Omvm5LbetbgrKPCrtH/LM1wx5PmOTl6jpZ4xyn9avRVqj9ZNQfP6Mo+U68Zfl5mfPjb4yn9/+5mxYMZ6xfYj/IzS616Tx8luXjL+h1AOP5j59n5znyj0uPuvtZx4D5yGlgtg8BHE0W3PqiUgO3PKMCWBuUoktkwD5TM3cyAZAMgHIMAorHmIyVRjzEZLo/isaaRXZ92BUmuz7saKXU/3I0m1P4JBJlTf8UgRtJPq1++Ijas+pX74gbUG9Sv3yTMqDfZ1++SqGUhvs6/eZNXP5GVo/wBnX75NaTf4Valzv7K/dI/MRSws8LfnQr6nBx1rnwTePlNscbfh5/LyY4XV5K6/Woj5LXXZ7m7GR5ZlXHKMZz8PJ4subptTra6SFLamwnmuf/Rh8opyXf5b/wChwzndJMXH0eu7f9HD946yu0b/AGmPs7v+Vz58+fD4vL/Qyro7AGKaB4Gy1fylTDKeo5eTq+PP3yW/7QyvTEcerX7trn5ibTfzHJcsN/pqqVA/zN/k2PgYsnb08t/5Yqta8NlT4naJ+ImGVv5elx4YX3hGzVKOAUeQEz26e2T0g7H1v374ENZY3rj9+2VE7Fsvb11/ftlaTsZ9Q3rD3RmkbmPj7IaJx2uRgHyKQ7mQDIBziAZsnkYBsKzyMottxUfVMWi2qtJ9Voy2slDeq0ei3F0ob1GhpO4RXQ/qPGW4VXS/qPGjcLqqs+zeBXRtKWfZvBJtPW/ZPF4B1L2/ZPF4LR1N1v2LSfCvJ9GptH9BpNxiplTqdZb9g0jtn5XM7+CV1lhH/hYeIOIu2flXdb40g1Kk5KuCeJLKT8ZrOTKeHBydHxZZby8X+bn6LSRh1Vh/2bLfnJtyybcc4uP+GyCjojShzYCA53bRKk45ZJJxKwzuE1MVc0+9P1ZEFax/WHtb/U0nNb8PNz+ncfu5f3/8JHqfXU+zP5S/uZ/hzZfT+GfKbik8/Yn+5Xfn+CnS8Mvv+3/lTTGtPq59osA+EyzmWXuO/i5sOOamU/otZqF5A+W0PmJM4v8AZWXW2erKl1in+Uj2iV9r92V+o5//AE3/APv5ODSh7m92fyiuGvlePWZZf/HY0OgQ97DzUSbdfLpwyuXvGxN+iKz/AFB7VEnvrfs/cS3ohPtR/jK7qm4wZ+jlH9Qe6OZJ0n9G/wCwfH9YbJ8aidzIByGMA2FhgNN1vMC0qmpPKPZaWTUnlGntIr1R5RpuJNeqPL5Rl2l1ao8vlBFhlWqPL5QToyrUHl8oEbTqPA/CIjabvA/CIHU2+B+ERmVWDkfhIqobUw5GTVw2oryMitJYXXYBwBktJWtiOfqkDzErHXyy5Jnf4HV6vSOPrFT5TpwznqR5PNwcky7uTy4rXTY/iCw+RwI8py/Gj4c+lxv6pXYVvtLs6coo5OhJnJfF/U9rHLuw/wCG6+zoOwEu6oxO8kHHwnTx83HjHl9Z0/V8nzNDM6IcEYPgJ048vd6eDz9HzY+cllcHh8pWnJj2z+JSs44/CZZ93w9Lp+bpcf4tkjUKO73gTny487fb2OPrOn1qf4Yb88CPcZFxsdWPJjl6TsrsPKKVeqLZp7PD3ypUXGiW6a3w98radUS3S2+HvlbToR9Hb4e+Mmn0Z+XxjD//2Q==',
      status: true,
    },
    {
      image: master,
      status: false,
    },
    {
      image: master,
      status: false,
    },
    {
      image: master,
      status: false,
    },
  ],
  name: 'Петров Никита',
  post: 'Специалист по ремонту ПК',
  info: [
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.',
  ],
}
