module.exports = {
  apps: [
    {
      name: 'muk-app',
      script: 'npm',
      args: 'run start',
    },
  ],
}
