import { formatTime } from 'utils/common'

export class SolutionListModel {
  constructor(data = {}) {
    this.id = data.id
    this.title = data.title
  }
}

export class SolutionModel {
  constructor(data = {}) {
    this.title = data.title
    this.description = data.description
    this.img = data.img,
    this.usefulLinks = data.usefulLinks
  }
}
