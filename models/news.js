import { formatTime } from 'utils/common'

export class NewsModel {
  constructor(data = {}) {
    this.id = data.id
    this.title = data.title
    this.description_short = data.description_short
    this.image = data.image
    this.date = data.date && formatTime(data.date)
  }
}

export class NewsSingleModel {
  constructor(data = {}) {
    this.id = data.id
    this.title = data.title
    this.description = data.description
    this.image = data.image
  }
}
