export class VacancyModel {
  constructor(data = {}) {
    this.id = data.id
    this.title = data.title
    this.description = data.description
    this.date = data.date_create
    this.address = data.address
    this.img = data.img
  }
}

export class VacancyListModel {
  constructor(data = {}) {
    this.id = data.id
    this.title = data.title
    this.description = data.description
    this.department = data.optionsP2
    this.country = data.country_iso
  }
}
