import { truncate } from 'utils/common'

export class VendorModel {
  constructor(data = {}) {
    this.id = data.id
    this.name = data.name
    this.description = data.description
    this.img = data.img && `https://muk.ua/business/logo/${data.img}`
    this.address = data.address
    this.site = data.site
  }
}

export class VendorListModel {
  constructor(data = {}) {
    this.id = data.id
    this.name = data.name
    this.description = data.description && truncate(data.description)
    this.img = data.img && `https://muk.ua/business/logo/${data.img}`
    this.link = data.url
    this.vendorURL = null
  }
}
