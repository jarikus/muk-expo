export const BASE_URL = {
  FRONT: 'https://front.muk.ua',
  API: 'https://backend.muk.ua',
}

const hosts = {
  'front.muk.ua': {
    apiUrl: BASE_URL.front,
  },
  'backend.muk.ua': {
    apiUrl: BASE_URL.api,
  },
}

const defaultHost = {
  apiUrl: null,
}

export function getConfig(req) {
  let host, proto

  if (req) {
    // host = req.headers.host || req.headers.host['x-forwarded-host']
    // proto = req.headers.host['x-forwarded-proto'] || 'https'
  } else {
    host = window.location.hostname
    proto = window.location.protocol
    proto = proto.slice(0, proto.length - 1)
  }

  const config = hosts[host] ? hosts[host] : defaultHost

  config.url = `${proto}://${host}`

  return config
}
