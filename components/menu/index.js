import React, { useState, useEffect } from 'react'
import { Link } from 'utils/with-i18next.js'
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from 'reactstrap'
import { SearchInput } from 'components/form'
import clsx from 'clsx'
import { HoverControlledDropdown } from 'components/dropdown'
import Photo from 'public/images/common/close-up-portrait.jpg'
import { LINKS } from 'constants/common'
import { SolutionCard } from 'components/card'

export const HeaderMenu = ({ mode = '', t }) => {
  const className = clsx('flex menu', mode)

  const menuList = [
    {
      name: t('header.menu.solutions.main'),
      url: null,
      isDropdown: true,
      menu: () => <CatalogMenu t={t} />,
      id: 0,
    },
    {
      name: t('header.menu.news.main'),
      url: null,
      isDropdown: true,
      menu: [
        <Link href={'/press/news'}>
          <a className='menu-link'>{t('header.menu.news.link1')}</a>
        </Link>,
        <Link href={'/press/promo'}>
          <a className='menu-link'>{t('header.menu.news.link2')}</a>
        </Link>,
        <Link href={'/company/muk-review'}>
          <a className='menu-link'>{t('header.menu.news.link3')}</a>
        </Link>,
        <a
          className='menu-link'
          href={LINKS.youtube}
          target='_blank'
          rel='noopener noreferrer'
        >
          {t('header.menu.news.link4')}
        </a>,
        <Link href={'/'}>
          <a className='menu-link'>{t('header.menu.news.link6')}</a>
        </Link>,
      ],
      id: 1,
    },
    {
      name: (
        <Link href='/company/vendors'>{t('header.menu.products.main')}</Link>
      ),
      url: null,
      isDropdown: true,
      id: 2,
    },
    {
      name: t('header.menu.services.main'),
      url: null,
      isDropdown: true,
      menu: [
        <Link href={'/training'}>
          <a className='menu-link'>{t('header.menu.services.link1')}</a>
        </Link>,
        <Link href={'/service'}>
          <a className='menu-link'>{t('header.menu.services.link2')}</a>
        </Link>,
        <Link href={'/'}>
          <a className='menu-link'>{t('header.menu.services.link3')}</a>
        </Link>,
      ],
      id: 3,
    },
    {
      name: t('header.menu.about.main'),
      url: null,
      isDropdown: true,
      menu: [
        <Link href={'/company'}>
          <a className='menu-link'>{t('header.menu.about.link1')}</a>
        </Link>,
        <Link href={'/'}>
          <a className='menu-link'>{t('header.menu.about.link2')}</a>
        </Link>,
        <Link href={'/'}>
          <a className='menu-link'>{t('header.menu.about.link3')}</a>
        </Link>,
        <Link href={'/company/vacancyi'}>
          <a className='menu-link'>{t('header.menu.about.link4')}</a>
        </Link>,
        <Link href={'/'}>
          <a className='menu-link'>{t('header.menu.about.link5')}</a>
        </Link>,
        <Link href={'/'}>
          <a className='menu-link'>{t('header.menu.about.link6')}</a>
        </Link>,
        <Link href={'/contact_us'}>
          <a className='menu-link'>{t('header.menu.about.link7')}</a>
        </Link>,
      ],
      id: 4,
    },
  ]

  return (
    <>
      <div className={className}>
        {menuList.map(({ name, url, isDropdown, menu, id }) => {
          if (url) {
            return (
              <div key={id}>
                <Link href={url}>
                  <a>{name}</a>
                </Link>
              </div>
            )
          }
          if (isDropdown) {
            return <HoverControlledDropdown key={id} name={name} menu={menu} />
          }
        })}
      </div>
    </>
  )
}

export const MobileMenu = ({ mode, t }) => {
  const className = clsx('flex menu', mode)

  const menuList = [
    {
      name: t('Решения'),
      url: '/catalog',
    },
    {
      name: t('Вендоры'),
      url: '/company/vendors',
    },
    {
      name: t('Сервисный центр'),
      url: '/service',
    },
    {
      name: t('О компании'),
      url: '/company',
    },
  ]

  return (
    <div className={className}>
      {menuList.map((item) => {
        return (
          <div key={item.name}>
            <Link href={item.url}>
              <a>{item.name}</a>
            </Link>
          </div>
        )
      })}
    </div>
  )
}

export const CatalogMenu = ({ t }) => {
  const [activeTab, setActiveTab] = useState('1')
  const [solutions, setSolutions] = useState([])
  const [vendors, setVendors] = useState([])

  useEffect(() => {
    if (window) {
      const { solutions, vendors } = window.__NEXT_CACHE_STORE__._data
      setSolutions(solutions)
      setVendors(vendors)
    }
  }, [])

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab)
  }

  const className = clsx('catalog-menu d-flex')
  return (
    <Row className={className}>
      <div className='controls'>
        <Nav tabs vertical pills>
          <NavItem>
            <NavLink
              className={clsx({ active: activeTab === '1' })}
              onClick={() => toggle('1')}
            >
              {t('header.menu.solutions.link1')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={clsx({ active: activeTab === '2' })}
              onClick={() => toggle('2')}
            >
              {t('header.menu.solutions.link2')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href={LINKS.catalog} target='_blank'>
              {t('header.menu.solutions.link3')}
            </NavLink>
          </NavItem>
        </Nav>
        <SearchInput placeholder={t('form.searchByName')} mode='black' />
        <div className='wrapper-image mt-5'>
          <img src={Photo} alt='' className='img-responsive' />
        </div>
      </div>
      <div className='content'>
        <TabContent activeTab={activeTab}>
          <TabPane tabId='1'>
            <div className='container mt-3'>
              <Row>
                {solutions.length > 0 &&
                  solutions.map((division, index) => (
                    <React.Fragment key={index}>
                      <Col xs='12' className='mt-4'>
                        <Link
                          href={{
                            pathname: '/solutions/[division]',
                            query: { division: division.link },
                          }}
                          as={`/solutions/${division.link}`}
                        >
                          <a>
                            <h3 style={{ fontWeight: 'bold' }}>
                              {division.division_name}
                            </h3>
                          </a>
                        </Link>
                      </Col>

                      {division.groups.map((group, ind) => (
                        <Col xl='4' md='6' key={ind} className='mt-4'>
                          <SolutionCard
                            t={t}
                            title={group.name}
                            linkGroup={group.link}
                            color={group.color}
                            linkDivision={division.link}
                          />
                        </Col>
                      ))}
                    </React.Fragment>
                  ))}
              </Row>
            </div>
          </TabPane>
          <TabPane tabId='2'>
            <div className='container mt-3 mb-5'>
              <Row>
                {vendors.map((item, key) => {
                  if (!item.img) {
                    return null
                  }
                  return (
                    <Col md='3' sm='4' key={key} className='mt-5'>
                      <Link
                        href={{
                          pathname: '/solutions/[group]',
                          query: { group: item.link },
                        }}
                        as={`/group/${item.link}`}
                      >
                        <a className='vendor-logo d-block'>
                          <img
                            src={item.img}
                            alt=''
                            className='img-responsive'
                          />
                        </a>
                      </Link>
                    </Col>
                  )
                })}
              </Row>
            </div>
          </TabPane>
        </TabContent>
      </div>
    </Row>
  )
}
