import React from 'react'
import Footer from 'components/footer'
import Header from 'components/header'

export function Layout({ children, header, className = '' }) {
  return (
    <div className='wrapper'>
      <Header mode={header} />
      <div className={`main ${className}`}>{children}</div>
      <Footer />
    </div>
  )
}
