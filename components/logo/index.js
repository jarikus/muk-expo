import React from 'react'
import { Link } from 'utils/with-i18next.js'
import { logo } from './logo.module.scss'
import logoImg from 'public/images/logo.png'
import simpleLogo from 'public/images/simple-logo.png'
import clsx from 'clsx'

const Logo = ({ mode = '' }) => {
  const className = clsx(logo, mode)
  return (
    <Link href='/'>
      <a className={className}>
        <img
          className='img-responsive'
          src={mode === 'white' ? simpleLogo : logoImg}
          alt='Logotype'
        />
      </a>
    </Link>
  )
}

export default Logo
