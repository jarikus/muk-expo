import React, { useState } from 'react'
import Head from 'next/head'
import { withTranslation } from 'utils/with-i18next.js'
import { ChangeLanguage } from 'components/language'
import { ChangeCity } from 'components/city'
import Logo from '../logo'
import FormSearch from '../form/formSearch'
import { ButtonPartner, ButtonRed } from 'components/form/button'
import { HeaderMenu, MobileMenu } from '../menu'
import clsx from 'clsx'
import { CallbackModal } from 'components/modals/callback-modal'
import { FormCallback } from 'components/form'
import { LINKS, CONTACTS } from 'constants/common'

function Header({ t, mode = '' }) {
  const [toggleMenu, setToggleMenu] = useState(false)
  const className = clsx(
    'header',
    toggleMenu && 'menu-open',
    mode === 'home' ? 'header--home' : mode
  )

  return (
    <>
      <Head>
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <header className={className}>
        <div className='flex header__mobile'>
          <Logo />
          <div className='logo-description'>{t('header.dLogo')}</div>
        </div>
        <div className='flex header__mobile header__mobile--button'>
          <div className='oval' />
          <div
            className='toggle-menu'
            onClick={() => setToggleMenu(!toggleMenu)}
          >
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div className='container flex header__body'>
          <div className='flex header__top'>
            <ChangeCity mode={mode} />
            <ChangeLanguage mode={mode} />
            <a
              className='reg'
              href={LINKS.signup}
              target='_blank'
              rel='noopener noreferrer'
            >
              {t('header.registration')}
            </a>
          </div>
          <div className='flex header__middle'>
            <div className='flex'>
              <Logo mode={mode} />
              <div className='logo-description'>{t('header.dLogo')}</div>
            </div>
            <FormSearch t={t} />
            <CallbackModal
              buttonLabel={(props) => (
                <ButtonRed className='btn__md' {...props}>
                  {t('header.btnCallback')}
                </ButtonRed>
              )}
            >
              {(props) => <FormCallback t={t} {...props} />}
            </CallbackModal>
            <div className='header__contact flex'>
              <a href={`tel: ${CONTACTS.phone1}`}>{CONTACTS.phone1}</a>
              <a href={`tel: ${CONTACTS.phone2}`}>{CONTACTS.phone2}</a>
            </div>
            <ButtonPartner
              href={LINKS.login}
              target='_blank'
              rel='noopener noreferrer'
            >
              {t('header.btnPartner')}
            </ButtonPartner>
          </div>
          <div className='flex header__bottom'>
            <HeaderMenu mode={mode} t={t} />
          </div>
        </div>
        <div className='header__mobile--body'>
          <div>
            <FormSearch t={t} />
          </div>
          <div>
            <MobileMenu mode={mode} t={t} />
          </div>
          <div className='contact-group'>
            <div className='header__contact flex'>
              <a href={`tel: ${CONTACTS.phone1}`}>{CONTACTS.phone1}</a>
              <a href={`tel: ${CONTACTS.phone2}`}>{CONTACTS.phone2}</a>
            </div>
            <ButtonRed className='btn__md'>{t('header.btnCallback')}</ButtonRed>
          </div>
          <div className='flex header-button-group'>
            <ButtonPartner
              href={LINKS.login}
              target='_blank'
              rel='noopener noreferrer'
            >
              {t('header.btnPartner')}
            </ButtonPartner>
            <a
              className='reg'
              href={LINKS.signup}
              target='_blank'
              rel='noopener noreferrer'
            >
              {t('header.registration')}
            </a>
          </div>
        </div>
      </header>
    </>
  )
}

export default withTranslation('common')(Header)
