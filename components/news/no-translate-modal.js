import React from 'react'
import { ButtonRed } from 'components/form'
import { i18n } from 'utils/with-i18next'
import { langOptions } from 'constants/language'

export function NoTranslateModal({ t, setOpenModal, available_translation }) {
  return (
    <div className='pt-3 pb-5 px-3'>
      <h3 className='display-4 mb-4'>Материала на этом языке нет</h3>
      <p className='lead'>Вы можете посмотреть его на следующих языках:</p>
      <hr className='mt-2' />
      <div>
        {available_translation &&
          available_translation.map((item, index) => {
            return (
              <ButtonRed
                onClick={() => {
                  i18n.changeLanguage(item.locale)
                  setOpenModal(false)
                }}
                key={index}
                className='mr-4 mt-3'
              >
                {langOptions.find((lang) => item.locale === lang.value).name}
              </ButtonRed>
            )
          })}
      </div>
    </div>
  )
}
