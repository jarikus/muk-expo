import React from 'react'
import Carousel from 'react-multi-carousel'
import Icon from '../icons'
import certificate from 'public/images/certificate.jpg'

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5,
    slidesToSlide: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 3,
    slidesToSlide: 1,
  },
  mobile: {
    breakpoint: { max: 480, min: 0 },
    items: 2,
    slidesToSlide: 1,
  },
}

const Certificates = () => {
  const style = {
    display: 'block',
    width: '30px',
    height: '30px',
    position: 'absolute',
    left: '50%',
    bottom: 0,
  }
  const ArrowLeft = (arrowProps) => {
    const { carouselState, children, ...restArrowProps } = arrowProps
    return (
      <span {...restArrowProps} style={style}>
        {children}
      </span>
    )
  }

  const body = (
    <Carousel
      additionalTransfrom={0}
      infinite
      autoPlay
      autoPlaySpeed={8000}
      ssr
      responsive={responsive}
      minimumTouchDrag={80}
      focusOnSelect={false}
      customLeftArrow={
        <ArrowLeft>
          <Icon component='arrLeft' />
        </ArrowLeft>
      }
      customRightArrow={
        <ArrowLeft>
          <Icon component='arrRight' />
        </ArrowLeft>
      }
    >
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
      <div className='certificate'>
        <img src={certificate} alt='' />
        <div className='certificate__name'>ЛОЩИНА</div>
      </div>
    </Carousel>
  )
  return body
}
export default Certificates
