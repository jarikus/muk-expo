import React, { useState } from 'react'
import { ButtonRed, Input, DragAndDrop } from 'components/form'
import { Form, FormGroup, Col } from 'reactstrap'
import { Formik } from 'formik'
import { ApiSrv } from 'services'
import { BASE_URL } from 'environment'
import { validateEmail } from 'utils/common'
import { Alert } from 'components/alerts'

export function FormVacancy({ t, mode }) {
  const [status, setStatus] = useState(null)
  const api = new ApiSrv({ baseUrl: BASE_URL.FRONT })

  const onSubmit = async (values) => {
    const res = await api.vacancies.sendBackResume(values)
    setStatus(res.message)
  }

  const Button = ({ disabled }) => (
    <>
      <ButtonRed disabled={disabled}>
        {mode === 'modal' ? 'Отправить резюме' : 'Отправить заявку на вакансию'}
      </ButtonRed>
      <p className='text-policy mt-3 d-block'>
        <span className='text-danger'>*</span> - обязательно для заполнения
      </p>
    </>
  )
  return (
    <Formik
      initialValues={{
        fullName: '',
        mail: '',
        phone: '',
        link: '',
        file: '',
        title: '',
      }}
      validate={(values) => {
        const errors = {}
        if (!values.phone.trim()) errors.phone = true
        if (!values.mail || !validateEmail(values.mail)) errors.mail = true
        if (!values.fullName.trim()) errors.fullName = true
        if (!values.link.trim() && !values.file) errors.link = true
        return errors
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        onSubmit(values)
        setSubmitting(false)
        resetForm()
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleSubmit,
        setFieldValue,
        isSubmitting,
      }) => (
        <Form onSubmit={handleSubmit}>
          <FormGroup row>
            <Col md='6'>
              <Input
                type='text'
                value={values.fullName}
                onChange={handleChange}
                name='fullName'
                invalid={errors.fullName && touched.fullName}
                placeholder={t('Введите ФИО*')}
              />
              <br />
              <Input
                type='text'
                value={values.mail}
                onChange={handleChange}
                name='mail'
                invalid={errors.mail}
                placeholder={t('Введите e-mail*')}
              />
              <br />
              <Input
                type='text'
                value={values.phone}
                onChange={handleChange}
                name='phone'
                invalid={errors.phone && touched.phone}
                placeholder={t('Введите телефон*')}
              />
              <br />
              {mode === 'modal' && (
                <>
                  <Input
                    type='text'
                    name='title'
                    value={values.title}
                    onChange={handleChange}
                    placeholder={t('Какую вакансию ищете?')}
                  />
                  <br />
                </>
              )}
            </Col>
            <Col md='6'>
              <DragAndDrop
                t={t}
                name='file'
                onChange={(value) => setFieldValue('file', value)}
              />
              <br />
              <Input
                type='text'
                onChange={handleChange}
                name='link'
                invalid={errors.link && !values.file && !values.link}
                value={values.link}
                placeholder={t('или вставьте ссылку на резюме')}
              />
              <br />
              {mode === 'modal' && <Button disabled={isSubmitting} />}
            </Col>
            {mode !== 'modal' && (
              <Col>
                <Button disabled={isSubmitting} />
              </Col>
            )}
          </FormGroup>
          {status && (
            <Alert color='success' state={status}>
              Ваше резюме успешно отправлено!
            </Alert>
          )}
        </Form>
      )}
    </Formik>
  )
}
