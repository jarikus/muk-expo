import React, { useState } from 'react'
import coupon from 'public/images/training/coupon.jpg'
import clsx from 'clsx'
import { ButtonRed, Input } from '../'
import whatsappImage from 'public/images/icons/social/whatapp.png'
import vkImage from 'public/images/icons/social/vk.png'
import viberImage from 'public/images/icons/social/viber.png'
import emailpImage from 'public/images/icons/social/email.png'

const socialContacts = [
  { name: 'Whatsapp', image: whatsappImage, id: 0 },
  { name: 'Вконтакте', image: vkImage, id: 1 },
  { name: 'Viber', image: viberImage, id: 2 },
  { name: 'E-mail', image: emailpImage, id: 3 },
]

export function CouponForm({ t }) {
  return (
    <div className='coupon-form'>
      <div className='container'>
        <div className='row'>
          <div className='com-md-6 col-sm-9 col-12'>
            <h3 className='title h3 font-weight-bold'>
              {t('form.coupon-form.title')}
            </h3>
          </div>
        </div>
        <div className='row'>
          <div className='col-md-6'>
            <div className='img-wrapper d-flex overflow-hidden position-relative align-items-center'>
              <div className='shaded'>
                <span className='text-white font-weight-bold'>
                  {t('form.coupon-form.sale10')}
                </span>
              </div>
              <img className='img-responsive' src={coupon} alt='' />
            </div>
          </div>
          <div className='col-md-6'>
            <p className='font-weight-bold mb-1'>
              {t('form.coupon-form.subtitle')}
            </p>
            <p className='text'>{t('form.coupon-form.whereSend')}</p>
            <div className='row'>
              {socialContacts.map((item, index) => (
                <div className='col-6 mb-2' key={index}>
                  <SocItem {...item} />
                </div>
              ))}
            </div>
            <div className='row mt-3'>
              <div className='col-sm-6 col-12 mb-3'>
                <Input className='input' placeholder={t('form.enterPhone')} />
              </div>
              <div className='col-sm-6 col-12 mb-3'>
                <ButtonRed>{t('form.getCoupon')}</ButtonRed>
              </div>
              <div className='col-sm-6 col-12'>
                <p className='policy'>{t('form.coupon-form.couponSent')}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

const SocItem = ({ image, name }) => {
  const [selected, setSelected] = useState(false)
  const classNames = clsx('soc-item', selected && 'selected')
  return (
    <div className={classNames} onClick={() => setSelected(!selected)}>
      <img className='img-responsive' src={image} alt='' />
      <span className='font-weight-bold ml-3'>{name}</span>
    </div>
  )
}
