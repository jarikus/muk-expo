import React from 'react'
import { Formik } from 'formik'
import { ButtonRed } from './button'
import { Input } from './input'
import { Textarea } from './textarea'
import { ApiSrv } from 'services'
import { BASE_URL } from 'environment'
import clsx from 'clsx'

const FormConsultation = ({ t, title, image, inputs = {}, sendEmail }) => {
  const api = new ApiSrv({ baseUrl: BASE_URL.FRONT })

  const onSubmit = async (values) => {
    await api.feedback.sendFeedback({ sendEmail, ...values })
  }

  return (
    <Formik
      initialValues={{ phone: '', description: '' }}
      validate={(values) => {
        const errors = {}
        if (!values.phone.trim()) {
          errors.phone = true
        }
        if (!values.description.trim()) {
          errors.description = true
        }
        return errors
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        onSubmit(values)
        setSubmitting(false)
        resetForm()
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form
          className='form form-Consultation'
          onSubmit={handleSubmit}
          style={{ backgroundImage: `url(${image})` }}
        >
          <h2 className='title title__section'>{title}</h2>
          <div className='manager__box'>
            <div className='manager manager__name'>{t('s4.form.name')}</div>
            <div className='manager manager__post'>{t('s4.form.post')}</div>
          </div>
          {inputs.phone && (
            <div className='input-group mb-3'>
              <Input
                className='input'
                name='phone'
                type='text'
                placeholder='+38 (___) ___-__-__'
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phone}
                invalid={errors.phone && touched.phone}
              />
            </div>
          )}
          {inputs.email && (
            <div className='input-group mb-3'>
              <Input
                className='input'
                name='email'
                type='email'
                placeholder={t('s4.form.email')}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.phone}
                invalid={errors.phone && touched.phone}
              />
            </div>
          )}
          {inputs.description && (
            <div className='input-group mb-3'>
              <Textarea
                className={clsx(
                  'input',
                  errors.description &&
                    touched.description &&
                    'is-invalid form-control'
                )}
                name='description'
                placeholder={t('s4.form.placeholder')}
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.description}
              />
            </div>
          )}
          <ButtonRed type='submit' disabled={isSubmitting}>
            {t('s4.form.btnSubmit')}
          </ButtonRed>
        </form>
      )}
    </Formik>
  )
}
FormConsultation.defaultProps = {
  title: '',
  inputs: {
    phone: true,
    description: true,
    email: false,
  },
}

export default FormConsultation
