import React from 'react'
import { Formik } from 'formik'
import { ButtonRed } from './button'
import { Input } from './input'
import { Textarea } from './textarea'
import { ApiSrv } from 'services'
import { BASE_URL } from 'environment'
import clsx from 'clsx'

export const FormCallback = ({ t, toggle }) => {
  const api = new ApiSrv({ baseUrl: BASE_URL.FRONT })

  const onSubmit = async (values) => {
    await api.feedback.sendFeedback(values)
    toggle(false)
  }

  return (
    <Formik
      initialValues={{ phone: '', description: '', options: t('s4.form.t_2') }}
      validate={(values) => {
        const errors = {}
        if (!values.phone.trim()) {
          errors.phone = true
        }
        if (!values.description.trim()) {
          errors.description = true
        }
        return errors
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        onSubmit(values)
        setSubmitting(false)
        resetForm()
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form
          className='form form-callback shadow-none'
          onSubmit={handleSubmit}
        >
          <h2 className='title title__section text-center'>
            {t('s4.form.t_2')}
          </h2>
          <div className='input-group mb-3 justify-content-center'>
            <Input
              className='input m-auto'
              name='phone'
              type='text'
              placeholder='+38  (___) ___-__-__'
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.phone}
              invalid={errors.phone && touched.phone}
            />
          </div>
          <div className='input-group mb-3 justify-content-center'>
            <Textarea
              className={clsx(
                'input m-auto',
                errors.description &&
                  touched.description &&
                  'is-invalid form-control'
              )}
              name='description'
              placeholder={t('s4.form.placeholder')}
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.description}
            />
          </div>
          <div className='input-group mb-3 justify-content-center'>
            <ButtonRed type='submit' disabled={isSubmitting}>
              {t('s4.form.btnCallback')}
            </ButtonRed>
          </div>
        </form>
      )}
    </Formik>
  )
}
