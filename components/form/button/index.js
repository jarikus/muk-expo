import React from 'react'
import { Link } from 'utils/with-i18next.js'
import Icon from 'components/icons'

const ButtonRed = ({ children, href, className = '', ...rest }) => {
  return (
    <>
      {href ? (
        <Link href={href}>
          <a className={`custom-btn custom-btn__red ${className}`} {...rest}>
            {children}
          </a>
        </Link>
      ) : (
        <button
          className={`custom-btn custom-btn__red ${className}`}
          {...rest}
        >
          {children}
        </button>
      )}
    </>
  )
}

const ButtonPartner = ({ children, href, className = '', ...rest }) => {
  return (
      <a href={href} className={`custom-btn custom-btn__partner ${className}`} {...rest}>
        <Icon component='user' />
        {children}
      </a>
  )
}

export { ButtonRed, ButtonPartner }
