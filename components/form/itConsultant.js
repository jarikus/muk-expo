import React from 'react'
import { Col, Form, FormGroup } from 'reactstrap'
import { ButtonRed, Input, Textarea, Select } from './'
import consultant from 'public/images/common/it-consultant.jpg'
import { Formik } from 'formik'
import { ApiSrv } from 'services'
import { BASE_URL } from 'environment'
import clsx from 'clsx'

const options = [
  { value: 'Какая техника нужна?', label: 'Какая техника нужна?' },
  { value: 'Сервисная служба', label: 'Сервисная служба' },
]
const defaultOption = options[0]

const FormITConsultant = ({ t }) => {
  const api = new ApiSrv({ baseUrl: BASE_URL.FRONT })

  const onSubmit = async (values) => {
    await api.feedback.sendFeedback(values)
  }

  return (
    <Formik
      initialValues={{ phone: '', description: '', options: '' }}
      validate={(values) => {
        const errors = {}
        if (!values.phone.trim()) {
          errors.phone = true
        }
        if (!values.description.trim()) {
          errors.description = true
        }
        return errors
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        onSubmit(values)
        setSubmitting(false)
        resetForm()
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        setFieldValue,
      }) => (
        <Form
          className='form form-consultant m-auto pb-0'
          onSubmit={handleSubmit}
        >
          <FormGroup className='pt-4 mb-0' row>
            <Col md={4}>
              <div className='consultant consultant-desktop overflow-hidden'>
                <img src={consultant} className='img-responsive' alt='' />
              </div>
            </Col>
            <Col md={8}>
              <FormGroup className='p-2 mb-0' row>
                <Col md={12}>
                  <h2 className='title title__section mt-3'>
                    Получите консультацию эксперта <br />
                    по IT решениям для компаний
                  </h2>
                  <div className='manager manager__block mb-3'>
                    <div className='consultant consultant-mobile overflow-hidden'>
                      <img src={consultant} className='img-responsive' alt='' />
                    </div>
                    <div className='mb-3'>
                      <div className='manager manager__post mb-3'>
                        Звоните +38 (044) 594-98-98 или оставьте заявку и
                        предложим лучшие варианты для бесперебойной работы
                        вашего бизнеса
                      </div>
                      <div className='manager manager__name'>
                        Елена Иванова <br />
                        Эксперт по подбору техники для бизнеса
                      </div>
                    </div>
                  </div>
                </Col>
                <Col md={6} className='mb-2'>
                  <Input
                    className='input'
                    name='phone'
                    type='text'
                    placeholder='+38  (___) ___-__-__'
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.phone}
                    invalid={errors.phone && touched.phone}
                  />
                  <Select
                    name='options'
                    options={options}
                    defaultOption={defaultOption}
                    value={defaultOption}
                    onChange={(e) => setFieldValue('options', e.value)}
                  />
                </Col>
                <Col md={5}>
                  <Textarea
                    className={clsx(
                      'input',
                      errors.description &&
                        touched.description &&
                        'is-invalid form-control'
                    )}
                    name='description'
                    placeholder={t('s4.form.placeholder')}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.description}
                  />
                </Col>
              </FormGroup>
              <FormGroup className='p-2' row>
                <Col md={12}>
                  <ButtonRed type='submit' disabled={isSubmitting}>
                    {t('s4.form.btnSubmit')}
                  </ButtonRed>
                  <p className='policy d-block mt-4'>
                    Позвоним в течении 15 минут, ответим на все вопросы
                    <br /> + предоставим лучшие цены
                  </p>
                </Col>
              </FormGroup>
            </Col>
          </FormGroup>
        </Form>
      )}
    </Formik>
  )
}

export default FormITConsultant
