import React, { useState } from 'react'
import { Col, Form, FormGroup } from 'reactstrap'
import { Input, Select } from 'components/form'

const opt = [
  { value: 'Вендор', label: 'Вендор' },
  { value: 'Вендор 2', label: 'Вендор 2' },
]

export function SearchBlock({
  options = opt,
  input = {},
  t,
  boxShadow = '0 0 6.25rem rgba(0, 0, 0, 0.1)',
  onChange,
}) {
  const { placeholder = t('form.searchByName') } = input
  const onSubmit = (values) => onChange(values)
  const defaultOption = options[0]
  return (
    <Form
      className='form form-search pt-2 pb-2'
      onSubmit={onSubmit}
      style={{ boxShadow }}
    >
      <FormGroup row className='mb-0 pl-3 pr-3'>
        <Col sm={6}>
          <Select
            name='theme'
            options={options}
            defaultOption={defaultOption}
            value={defaultOption}
          />
        </Col>
        <Col sm={6}>
          <div className='search'>
            <Input name='search' placeholder={placeholder} />
          </div>
        </Col>
      </FormGroup>
    </Form>
  )
}

export function SearchBlockSimple({
  input = {},
  t,
  boxShadow = '0 0 6.25rem rgba(0, 0, 0, 0.1)',
  onChange,
}) {
  const [state, setState] = useState('')
  const { placeholder = t('form.searchByName') } = input

  const onSubmit = (e) => {
    e.preventDefault()
    const value = e.target['search'].value
    onChange(value)
  }

  const handleChange = (value) => {
    onChange(value)
    setState(value)
  }

  return (
    <Form
      className='form form-search pt-2 pb-2'
      onSubmit={onSubmit}
      style={{ boxShadow, maxWidth: '24rem' }}
    >
      <FormGroup row className='mb-0 pl-3 pr-3'>
        <Col sm={12}>
          <div className='search d-flex'>
            <Input
              name='search'
              value={state}
              placeholder={placeholder}
              onChange={(e) => handleChange(e.target.value)}
            />
            {state && (
              <div
                className='position-absolute'
                onClick={() => {
                  handleChange('')
                  setState('')
                }}
              >
                x
              </div>
            )}
          </div>
        </Col>
      </FormGroup>
    </Form>
  )
}
