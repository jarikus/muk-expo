import React from 'react'
import dynamic from 'next/dynamic'
// import { Controller } from 'react-hook-form'
const Dropdown = dynamic(() => import('react-dropdown'), {
  ssr: false,
})

export const Select = (props) => {
  const { defaultOption } = props

  return (
    <Dropdown
      className='dropdown-select'
      {...props}
      value={defaultOption}
      defaultValue={defaultOption}
    />
  )
}
