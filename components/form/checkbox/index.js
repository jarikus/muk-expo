import React from 'react'
import { FormGroup, Label, Input } from 'reactstrap'

export function Checkbox({ children, ...rest }) {
  return (
    <FormGroup className='simple-checkbox' check>
      <Label check>
        <Input type='checkbox' {...rest} />
        <span>{children}</span>
      </Label>
    </FormGroup>
  )
}

export function CustomCheckbox({ children, ...rest }) {
  return (
    <FormGroup className='custom-checkbox'>
      <Label check>
        <Input type='checkbox' {...rest} />
        <span>{children}</span>
      </Label>
    </FormGroup>
  )
}
