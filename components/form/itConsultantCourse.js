import React from 'react'
import { Col, Form, FormGroup } from 'reactstrap'
import { ButtonRed, Input, Textarea } from './'
import consultant from 'public/images/common/it-consultant.jpg'

export const FormITConsultantCourse = ({ t }) => {
  const onSubmit = (values) => {
    console.log(values)
  }

  return (
    <Form className='form form-consultant m-auto pb-0' onSubmit={onSubmit}>
      <FormGroup className='pt-4 mb-0' row>
        <Col md={4}>
          <div className='consultant consultant-desktop overflow-hidden'>
            <img src={consultant} className='img-responsive' alt='' />
          </div>
        </Col>
        <Col md={8}>
          <FormGroup className='p-2 mb-0' row>
            <Col md={12}>
              <h2 className='title title__section text-uppercase'>
                {t('НЕ НАШЛИ ПОДХОДЯЩИЙ КУРС?')}
              </h2>
              <div className='manager manager__block mb-3'>
                <div className='consultant consultant-mobile overflow-hidden'>
                  <img src={consultant} className='img-responsive' alt='' />
                </div>
                <div className='mb-3'>
                  <div className='manager manager__post mb-3'>
                    {t(
                      'Задайте свой вопрос и получите консультацию эксперта и получите индвидуальное предложение на обучение'
                    )}
                  </div>
                  <div className='manager manager__name'>
                    {t('Елена Иванова')}
                  </div>
                  <div className='manager manager__name'>
                    {t('Эксперт по подбору техники для бизнеса')}
                  </div>
                </div>
              </div>
            </Col>
            <Col md={6} className='mb-2'>
              <Input
                name='phone'
                type='tel'
                placeholder='+38 (___) ___-__-__'
                defaultValue=''
              />
              <div className='mt-3'>
                <Textarea
                  name='description'
                  placeholder={t('s4.form.placeholder')}
                  defaultValue=''
                  rows='6'
                />
              </div>
              <div className='mt-3 mb-3'>
                <ButtonRed type='submit'>{t('s4.form.btnSubmit')}</ButtonRed>
              </div>
            </Col>
            <Col md={5}>
              <p className='font-weight-bold'>
                {t('После нее вы будете знать')}
              </p>
              <ul className='red-list'>
                <li className='red-list-item mb-3'>
                  {t('Какой курс подойдет вам')}
                </li>
                <li className='red-list-item mb-3'>
                  {t('На что нужно смотреть при обучении')}
                </li>
                <li className='red-list-item mb-3'>
                  {t('Как повысить квалификацию без потери времени')}
                </li>
                <li className='red-list-item mb-3'>
                  {t('Как защитить себя от компаний дающие слабое обучение')}
                </li>
              </ul>
            </Col>
          </FormGroup>
        </Col>
      </FormGroup>
    </Form>
  )
}
