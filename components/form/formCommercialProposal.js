import React from 'react'
import { Formik } from 'formik'
import { ButtonRed } from './button'
import { Input } from './input'
import { Textarea } from './textarea'
import { ApiSrv } from 'services'
import { BASE_URL } from 'environment'
import clsx from 'clsx'

const FormCommercialProposal = ({ t }) => {
  const api = new ApiSrv({ baseUrl: BASE_URL.FRONT })

  const onSubmit = async (values) => {
    await api.feedback.sendFeedback(values)
  }

  return (
    <Formik
      initialValues={{ phone: '', description: '' }}
      validate={(values) => {
        const errors = {}
        if (!values.phone.trim()) {
          errors.phone = true
        }
        if (!values.description.trim()) {
          errors.description = true
        }
        return errors
      }}
      onSubmit={(values, { setSubmitting, resetForm }) => {
        onSubmit(values)
        setSubmitting(false)
        resetForm()
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
      }) => (
        <form className='form form-CommercialProposal' onSubmit={handleSubmit}>
          <h2 className='title title__section'>{t('s7.t')}</h2>
          <h5 className='title title__description'>{t('s7.d')}</h5>
          <div className='flex content'>
            <div>
              <div className='manager manager__name'>{t('s7.name')}</div>
              <div className='manager manager__post'>{t('s7.post')}</div>
              <div className='input-group mb-3'>
                <Input
                  className='input'
                  name='phone'
                  type='text'
                  placeholder='+38  (___) ___-__-__'
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.phone}
                  invalid={errors.phone && touched.phone}
                />
              </div>
              <div className='input-group mb-3'>
                <Textarea
                  className={clsx(
                    'input',
                    errors.description &&
                      touched.description &&
                      'is-invalid form-control'
                  )}
                  name='description'
                  placeholder={t('s4.form.placeholder')}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.description}
                />
              </div>
              <ButtonRed type='submit' disabled={isSubmitting}>
                {t('s7.btnSubmit')}
              </ButtonRed>
            </div>
            <div className='dot-items'>
              <div className='dot-item'>{t('s7.ul.li1')}</div>
              <div className='dot-item'>{t('s7.ul.li2')}</div>
              <div className='dot-item'>{t('s7.ul.li3')}</div>
              <div className='dot-item'>{t('s7.ul.li4')}</div>
            </div>
          </div>
        </form>
      )}
    </Formik>
  )
}

export default FormCommercialProposal
