import React, { useState } from 'react'
import { ApiSrv } from 'services'
import clsx from 'clsx'
import { BASE_URL } from 'environment'
import { debouncedDecorator } from 'utils/common'
import { ClickAwayListner } from 'utils/click-away'
import { Link, i18n } from 'utils/with-i18next'

const FormSearch = ({ t }) => {
  const [results, setResults] = useState(null)
  const [open, setOpen] = useState(false)
  const api = new ApiSrv({})

  const handleChange = debouncedDecorator(async (value) => {
    if (!value) {
      setOpen(false)
      return
    }

    const { search, total } = await api.search.searchData({
      query: value,
      lang: i18n.language,
    })

    setResults({ search, total })
    setOpen(true)
  }, 500)

  return (
    <ClickAwayListner onClose={() => setOpen(false)}>
      <form className='search' onSubmit={handleChange}>
        <input
          type='text'
          name='search'
          placeholder={t('header.placeholderSearch')}
          onChange={(e) => handleChange(e.target.value)}
          onFocus={() => setOpen(true)}
          autocomplite='off'
        />
        {results && results.search && (
          <div className={clsx('search__container d-none', open && 'd-block')}>
            <ul className='search__list'>
              {results.search.map((item, index) => (
                <li key={index} className='search__item'>
                  <Link href={item.link}>
                    <a>{item.title}</a>
                  </Link>
                </li>
              ))}
            </ul>
            {results.total === '0' && (
              <div className='search__notfound'>
                По этому запросу ничего не найдено
              </div>
            )}
          </div>
        )}
      </form>
    </ClickAwayListner>
  )
}

export default FormSearch
