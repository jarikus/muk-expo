import React from 'react'

export const Textarea = (props) => (
  <div className='input-group'>
    <textarea className='input' {...props} />
  </div>
)
