import React, { useState } from 'react'
import Range from 'rc-slider/lib/Range'
import { Label, FormGroup, Input } from 'reactstrap'
import { wrapper } from './range.module.scss'

export function PriceRange() {
  const [min, setMin] = useState(0)
  const [max, setMax] = useState(250000)

  const onChange = (e) => {
    setMin(e[0])
    setMax(e[1])
  }
  const onMinChange = (data) => setMin(+data)
  const onMaxChange = (data) => setMax(+data)

  return (
    <>
      <div className={`${wrapper} justify-content-between`}>
        <input
          type='number'
          value={min}
          onChange={(e) => onMinChange(+e.target.value)}
          className='input'
        />
        <input
          type='number'
          value={max}
          onChange={(e) => onMaxChange(+e.target.value)}
          className='input'
        />
      </div>
      <WrapRange min={min} max={max} onChange={onChange} />
      <div className='mt-3'>
        <FormGroup check className='mb-3 align-items-center d-flex'>
          <Label check>
            <Input type='checkbox' onChange={() => onChange([0, 30000])} /> до
            30 000 грн.
          </Label>
        </FormGroup>
        <FormGroup check className='mb-3'>
          <Label check>
            <Input type='checkbox' onChange={() => onChange([30000, 50000])} />{' '}
            30 000 грн - 50 000 грн.
          </Label>
        </FormGroup>
        <FormGroup check className='mb-3'>
          <Label check>
            <Input type='checkbox' onChange={() => onChange([50000, 90000])} />{' '}
            50 000 грн - 90 000 грн.
          </Label>
        </FormGroup>
        <FormGroup check className='mb-3'>
          <Label check>
            <Input type='checkbox' onChange={() => onChange([90000, 250000])} />{' '}
            от 90 000 грн.
          </Label>
        </FormGroup>
      </div>
    </>
  )
}

function WrapRange({ onChange, min, max }) {
  const [value] = useState([0, 250000])
  return (
    <Range
      min={value[0]}
      max={value[1]}
      allowCross={false}
      value={[min, max]}
      defaultValue={value}
      step={100}
      dotStyle={{ backgroundColor: 'red', borderColor: 'red' }}
      onChange={onChange}
      trackStyle={[{ backgroundColor: 'red' }, { backgroundColor: 'green' }]}
      handleStyle={[{ backgroundColor: 'red' }, { backgroundColor: 'gray' }]}
      railStyle={{ backgroundColor: 'gray' }}
    />
  )
}
