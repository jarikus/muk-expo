import React from 'react'
import logo from 'public/images/logo-footer.png'
import { withTranslation, Link } from 'utils/with-i18next'
import { FormFooter } from 'components/form/formFooter'
import Social from 'components/social'
import { CallbackModal } from 'components/modals/callback-modal'
import { FormCallback } from 'components/form'
import { CONTACTS } from 'constants/common'

function Footer({ t }) {
  const linkNews = [
    { label: t('footer.newsMenu.company'), link: '/press/news' },
    { label: t('footer.newsMenu.promo'), link: '/press/promo' },
    { label: t('footer.newsMenu.training'), link: '/press/lnews' },
  ]

  const linkAbout = [
    {
      name: t('footer.about.catalog'),
      link: '/catalog',
    },
    {
      name: t('footer.about.vendors'),
      link: '/company/vendors',
    },
    {
      name: t('footer.about.vacancies'),
      link: '/company/vacancyi/',
    },
    {
      name: t('footer.about.bidders'),
    },
    {
      name: t('footer.about.journal'),
      link: '/company/muk-review/',
    },
  ]

  return (
    <footer className='footer'>
      <div className='container flex'>
        <div>
          <img className='footer__logo img-responsive' src={logo} alt='' />
          <Link href='/'>
            <a className='text-brown'>{t('footer.privacyPolicy')}</a>
          </Link>
          <Link href='/'>
            <a className='text-brown'>{t('footer.termsOfUse')}</a>
          </Link>
          <div className='text-brown'>{t('footer.copyright')}</div>
        </div>
        <div>
          <div className='footer__links-title'>{t('footer.tNews')}</div>
          {linkNews.map(({ label, link }, index) => (
            <Link href={link} key={index}>
              <a className='footer__link'>{label}</a>
            </Link>
          ))}
        </div>
        <div>
          <div className='footer__links-title'>{t('footer.tAbout')}</div>
          {linkAbout.map(({ name, link = '/' }, index) => (
            <Link href={link} key={index}>
              <a className='footer__link'>{name}</a>
            </Link>
          ))}
        </div>
        {/* For future */}
        {false && (
          <div>
            <FormFooter t={t} />
          </div>
        )}
        <div className='footer__contact flex'>
          <a className='footer__phone' href={`tel:${CONTACTS.phone1}`}>
            {CONTACTS.phone1}
          </a>
          <a className='footer__phone' href={`tel:${CONTACTS.phone2}`}>
            {CONTACTS.phone2}
          </a>
          <a className='footer__email' href={`mailto:${CONTACTS.email1}`}>
            {CONTACTS.email1}
          </a>

          <CallbackModal
            buttonLabel={(props) => (
              <p className='footer__callback cursor-pointer' {...props}>
                {t('footer.callback')}
              </p>
            )}
          >
            {(props) => <FormCallback t={t} {...props} />}
          </CallbackModal>
          <Social />
        </div>
      </div>
    </footer>
  )
}

export default withTranslation('common')(Footer)
