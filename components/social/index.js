import React from 'react'
import Icon from 'components/icons'
import { LINKS } from 'constants/common'

const socialLinks = [
  {
    name: 'facebook',
  },
  {
    name: 'telegram',
  },
  {
    name: 'twitter',
  },
  {
    name: 'youtube',
  },
  {
    name: 'linkedin',
  },
]

const Social = () => (
  <div className='social flex'>
    {socialLinks.map(({name}, index) => (
      <a
        key={index}
        target='_blank'
        rel='noopener noreferrer'
        href={LINKS[name]}
      >
        <Icon component={name} />
      </a>
    ))}
  </div>
)

export default Social
