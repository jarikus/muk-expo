import React from 'react'
import { Link } from 'utils/with-i18next'

export function OurVendors({ list, t }) {
  if (list && list.length === 0) return null
  return (
    <div className='container'>
      <div className='our-vendors-title font-weight-bold h5'>
        {t('journalsPage.ourVendors')}
      </div>
      <div className='our-vendors flex'>
        {list &&
          list.map((vendor) => {
            if (!vendor.img) return null
            const href = {
              pathname: '/company/vendors/[url]',
              query: { url: vendor.link },
            }
            const as = `/company/vendors/${vendor.link}`
            return (
              <div key={vendor.id}>
                {vendor.link ? (
                  <Link href={href} as={as}>
                    <a>
                      <img src={vendor.img} alt={vendor.name} />
                    </a>
                  </Link>
                ) : (
                  <img src={vendor.img} alt={vendor.name} />
                )}
              </div>
            )
          })}
      </div>
    </div>
  )
}
