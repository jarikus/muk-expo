import React from 'react'
import styles from './vendors.module.scss'
import { Link } from 'utils/with-i18next'

export function VendorPreview({ link, vendorURL, img, name, description }) {
  const href = {
    pathname: '/company/vendors/[url]',
    query: { url: link },
  }
  const as = `/company/vendors/${link}`

  return (
    <div className={`${styles.card} d-flex`}>
      <div className={`${styles.image} text-center`}>
        {link ? (
          <Link href={href} as={as}>
            <a>
              <img src={img} alt={name} />
            </a>
          </Link>
        ) : (
          <img src={img} alt={name} />
        )}
        {vendorURL && (
          <a href={vendorURL}>
            <span>{name}</span>
          </a>
        )}
      </div>
      <p>
        {link ? (
          <Link href={href} as={as}>
            <a>
              <strong>{name}</strong>
            </a>
          </Link>
        ) : (
          <strong>{name}</strong>
        )}

        {description}
      </p>
    </div>
  )
}
