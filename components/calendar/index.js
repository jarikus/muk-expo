import React, { useState } from 'react'
import dynamic from 'next/dynamic'
import { Link } from 'utils/with-i18next'
import { isSameDay, format } from 'date-fns'
import { Button } from 'reactstrap'
import { ButtonRed } from 'components/form'

const ReactCalendar = dynamic(() => import('react-calendar'), {
  ssr: false,
})

const fakeCourses = [
  {
    name: 'AutoCAD Civil 3D 2018/2019 – Углубленный курс (Генплан)',
    days: 5,
    hours: 10,
    dateStart: '2020-06-02T17:25:00.489Z',
    link: 'autocad',
    category: 'autocad',
    price: '15 000 грн.',
    oldPrice: '65 000 грн.',
  },
  {
    name: 'AutoCAD Civil 3D 2018/2019 – Углубленный курс (Генплан)',
    days: 5,
    hours: 10,
    dateStart: '2020-06-08T17:25:00.489Z',
    link: 'autocad',
    category: 'autocad',
    price: '15 000 грн.',
    oldPrice: '65 000 грн.',
  },
  {
    name: 'AutoCAD Civil 3D 2018/2019 – Углубленный курс (Генплан)',
    days: 5,
    hours: 10,
    dateStart: '2020-06-02T17:25:00.489Z',
    link: 'autocad',
    category: 'autocad',
    price: '15 000 грн.',
    oldPrice: '65 000 грн.',
  },
  {
    name: 'AutoCAD Civil 3D 2018/2019 – Углубленный курс (Генплан)',
    days: 5,
    hours: 10,
    dateStart: '2020-06-11T17:25:00.489Z',
    link: 'autocad',
    category: 'autocad',
    price: '15 000 грн.',
    oldPrice: '65 000 грн.',
  },
]

export function Calendar({ courses = fakeCourses, t }) {
  const [selectedDate, setSelectedDate] = useState('2020-06-02T17:25:00.489Z')

  const onClickDay = (date) => {
    let d = new Date(date)
    setSelectedDate(d.toISOString())
  }

  const tileClassName = ({ date, view }) => {
    if (view === 'month') {
      if (courses.find((item) => isSameDay(date, new Date(item.dateStart)))) {
        return 'font-weight-bold bg-danger text-white'
      }
      return
    }
  }

  return (
    <div className='d-flex align-items-center rc-calendar'>
      <ReactCalendar
        tileClassName={tileClassName}
        onClickDay={onClickDay}
        locale='ru-RU'
      />
      <div className='courses py-3'>
        {setSelectedDate && (
          <FoundCourses courses={courses} t={t} selectedDate={selectedDate} />
        )}
      </div>
    </div>
  )
}

const FoundCourses = ({ courses = [], selectedDate, t }) => {
  const isFound =
    courses &&
    courses.filter((item) =>
      isSameDay(new Date(item.dateStart), new Date(selectedDate))
    )

  return isFound && isFound.length === 0 ? (
    <div className='course-card text-center'>{t('calendar.noLessons')}</div>
  ) : (
    isFound.map((item, index) => (
      <div className='card course-card my-4' key={index}>
        <div className='font-weight-bold'>{item.name}</div>
        <div className='d-flex flex-wrap mt-3 mb-4'>
          <span className='pr-3'>
            {t('calendar.dayQuantity')} {item.days}
          </span>
          <span className='pr-3'>
            {t('calendar.numberHours')} {item.hours}
          </span>
          <span>
            {t('calendar.startDate')}{' '}
            {format(new Date(item.dateStart), 'dd.MM.yyyy')}
          </span>
        </div>
        <div className='d-flex price mb-4 flex-wrap'>
          <div className='buttons'>
            <div className='text-center'>
              <ButtonRed>{t('common.buyCourse')}</ButtonRed>
              <br />
              <Link href={`/training/${item.category}/${item.link}`}>
                <a>
                  <Button color='link' className='btn text-reset p-0 pt-2'>
                    {t('common.moreDetails')}
                  </Button>
                </a>
              </Link>
            </div>
          </div>
          <div className='mx-2'>
            <div className='actual pt-2'>{item.price}</div>
          </div>
          <div className='mx-2'>
            <span className='d-inline-block old pt-3'>{item.oldPrice}</span>
          </div>
        </div>
      </div>
    ))
  )
}
