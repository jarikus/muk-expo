import React, { useState, useEffect } from 'react'
import { i18n } from 'utils/with-i18next'
import dynamic from 'next/dynamic'
import { langOptions } from 'constants/language'
const Dropdown = dynamic(() => import('react-dropdown'), {
  ssr: false,
})

export function ChangeLanguage() {
  const [lang, setLang] = useState(i18n.language)

  useEffect(() => {
    setLang(i18n.language)
  }, [i18n.language])

  const _onSelect = (e) => i18n.changeLanguage(e.value)

  return (
    <Dropdown
      options={langOptions}
      onChange={(value) => _onSelect(value)}
      value={lang}
      placeholder='Select language'
    />
  )
}
