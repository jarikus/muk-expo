import React, { useState, useEffect, useRef } from 'react'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'

export function CallbackModal(props) {
  const { buttonLabel, className = 'small', children, isOpen = false } = props
  const [modal, setModal] = useState(isOpen)

  useEffect(() => {
    setModal(isOpen)
  }, [isOpen])

  const toggle = () => setModal(!modal)

  const closeBtn = (
    <button className='close text-danger' onClick={toggle}>
      &times;
    </button>
  )
  const ButtonLabel = buttonLabel
  const ChildrenWithProps = children

  return (
    <>
      {buttonLabel && <ButtonLabel onClick={toggle} />}
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle} close={closeBtn} />
        <ModalBody>{<ChildrenWithProps toggle={toggle} />}</ModalBody>
      </Modal>
    </>
  )
}
