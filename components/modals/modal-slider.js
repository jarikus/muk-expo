import React, { useState } from 'react'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
import Icon from '../icons'
import { Master } from 'utils/data'

export function SliderModal(props) {
  const [masterSlider, setMasterSlider] = useState(Master.images)
  const [modal, setModal] = useState(false)
  const [idxCurrent, setIdxCurrent] = useState(0)
  const { t } = props

  const handleMasterPreview = (idx) => {
    setIdxCurrent(idx)
    const arr = masterSlider.map((i,index) => {
      if(index === idx){
        return {
          ...i,
          status: true
        }
      }
      return {
        ...i,
        status: false
      }
    })
    setMasterSlider(arr)
  }
  const onPrev = () => {
    if(idxCurrent > 0) {
      handleMasterPreview(idxCurrent - 1)
    }
  }
  const onNext = () => {
    if(idxCurrent < masterSlider.length-1){
      handleMasterPreview(idxCurrent + 1)
    }
  }


  const toggle = () => setModal(!modal)

  const closeBtn = (
    <button className='close text-danger' onClick={toggle}>
      &times;
    </button>
  )

  return (
    <>
      <div onClick={toggle} className='karta-btn flex'>
        <Icon component='picture' />
        {t('Смотрите фото сервиса')}
      </div>
      <Modal isOpen={modal} toggle={toggle} className='modal-slider'>
        <ModalHeader toggle={toggle} close={closeBtn} />
        <ModalBody>
          <div className='master__slider'>
            <div className="master__preview flex nowrap">
              <div className="master__prev" onClick={onPrev}>
                <Icon component='arrLeft'/>
              </div>
              <img src={masterSlider.filter(i => i.status === true)[0].image} alt=""/>
              <div className="master__next" onClick={onNext}>
                <Icon component='arrRight'/>
              </div>
            </div>
            <div className="master__list flex">
              {masterSlider.map((i, idx) => (
                <div key={idx} className={`${i.status && 'active'}`} onClick={() => handleMasterPreview(idx)}>
                  <img src={i.image} alt=""/>
                </div>
              ))}
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  )
}
