import React, { useState, useEffect } from 'react'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
import { ButtonRed } from 'components/form/button'

export function CustomModal(props) {
  const { buttonLabel, className, children, isOpen = false } = props
  const [modal, setModal] = useState(isOpen)

  useEffect(() => {
    setModal(isOpen)
  }, [isOpen])

  const toggle = () => setModal(!modal)

  const closeBtn = (
    <button className='close text-danger' onClick={toggle}>
      &times;
    </button>
  )

  return (
    <>
      {buttonLabel && <ButtonRed onClick={toggle}>{buttonLabel}</ButtonRed>}
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle} close={closeBtn} />
        <ModalBody>{children}</ModalBody>
      </Modal>
    </>
  )
}
