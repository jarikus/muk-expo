import React, { useState } from 'react'
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
import Icon from '../icons'

export function CertificateModal(props) {
  const { children, t } = props

  const [modal, setModal] = useState(false)

  const toggle = () => setModal(!modal)

  const closeBtn = (
    <button className='close text-danger' onClick={toggle}>
      &times;
    </button>
  )

  return (
    <>
      <div onClick={toggle} className='enlarge'>
        {t('Увеличить')}
        <Icon component='magnifier' />
      </div>
      <Modal isOpen={modal} toggle={toggle} className='modal-certificate'>
        <ModalHeader toggle={toggle} close={closeBtn} />
        <ModalBody>{children}</ModalBody>
      </Modal>
    </>
  )
}
