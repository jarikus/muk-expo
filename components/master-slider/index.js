import React,{useState} from 'react'
import { Master } from 'utils/data'

const MasterSlider = ({ t }) => {
  const [masterSlider, setMasterSlider] = useState(Master.images)
  const handleMasterPreview = (idx) => {
    const arr = masterSlider.map((i,index) => {
      if(index === idx){
        return {
          ...i,
          status: true
        }
      }
      return {
        ...i,
        status: false
      }
    })
    setMasterSlider(arr)
  }

  return(
    <div className="master flex">
      <div className='master__slider'>
        <div className="master__preview">
          <img src={masterSlider.filter(i => i.status === true)[0].image} alt=""/>
        </div>
        <div className="master__list flex">
          {masterSlider.map((i, idx) => (
            <div key={idx} className={`${i.status && 'active'}`} onClick={() => handleMasterPreview(idx)}>
              <img src={i.image} alt=""/>
            </div>
          ))}
        </div>
      </div>
      <div className='master__body'>
        <div className="master__name">{Master.name}</div>
        <div className="master__post">{Master.post}</div>
        <div className="master__info-title">{t('Опыт, обучение, сертификация')}</div>
        <div className="master__info">
          {Master.info.map((i, idx) => (
            <p key={idx}>{i}</p>
          ))}
        </div>
      </div>
    </div>
  )
}

export default MasterSlider