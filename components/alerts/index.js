import React, { useState, useEffect } from 'react'
import { Alert as StrapAlert } from 'reactstrap'

export function Alert({ color, children, state = false }) {
  const [show, setShow] = useState(false)

  useEffect(() => {
    setShow(true)
    manageAlert(state)
  }, [state])

  function manageAlert() {
    let promise = new Promise((resolve) => {
      setTimeout(() => resolve(setShow(false)), 5000)
    })
    promise.then()
  }

  return <>{show ? <StrapAlert color={color} className='text-center'>{children}</StrapAlert> : ''}</>
}
