export const theme = {
  colors: {
    bluelight: '#4c637c',
    blueDark: '#242e3a',
    blue: '#028de6',
    green: '#759e83',
    brownLight: '#a2a2a2',
    red: '#d01018',
    gray: '#555555',
    border: '#e1e1e1',
  },
}
