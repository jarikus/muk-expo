export const worldMapConfig = ({ t }) => {
  return {
    zoom: '200',
    offsetTop: '-60',
    countries: {
      UA: {
        id: 1,
        address: t('world-map.ua.address'),
        phones: ['+38 (044) 594-98-98', '+38 (044) 492-29-29'],
        email: 'info@muk.ua',
      },
      // CN: {
      //   id: 2,
      //   address: 'China, г. Beijing, ул. Донецкая, 16/2 03151',
      //   phones: ['+7 (495) 594-98-98', '+7 (495) 492-29-29'],
      //   email: 'info@muk.cn',
      // },
      // IN: {
      //   id: 3,
      //   address: 'India, г. Deli, ул. Донецкая, 16/2 03151',
      //   phones: ['+7 (495) 594-98-98', '+7 (495) 492-29-29'],
      //   email: 'info@muk.in',
      // },
      BY: {
        id: 4,
        address: t('world-map.by.address'),
        phones: [
          '+375 17 336-98-93',
          '+375 17 336-98-94',
          '+375 17 336-98-95',
          '+375 17 336-98-97',
        ],
        email: 'office@muk.by',
      },
      AM: {
        id: 5,
        address: t('world-map.am.address'),
        phones: ['+374 94 655 044'],
        email: 'office@muk.am',
      },
      AZ: {
        id: 6,
        address: t('world-map.az.address'),
        phones: ['+994 12 464 43 50', '+994 12 310 34 43'],
        email: 'office@muk.az',
      },
      GE: {
        id: 7,
        address: t('world-map.ge.address'),
        phones: ['+995 (32) 260-16-54'],
        email: 'office@muk.ge',
      },
      KZ: {
        id: 8,
        address: t('world-map.kz.address'),
        phones: ['+7 (727) 346-79-01'],
        email: 'office@muk.kz',
      },
      KG: {
        id: 9,
        address: t('world-map.kg.address'),
        phones: ['+38 (044) 492-29-29'],
        email: 'office@muk.kg',
      },
      MK: {
        id: 10,
        address: t('world-map.mk.address'),
        phones: null,
        email: 'office@muk.mk',
      },
      MD: {
        id: 11,
        address: t('world-map.md.address'),
        phones: ['+373 677-00-611'],
        email: 'office@muk.md',
      },
      MN: {
        id: 12,
        address: t('world-map.mn.address'),
        phones: ['+976 75057577'],
        email: 'office@muk.mn',
      },
      SI: {
        id: 13,
        address: t('world-map.si.address'),
        phones: ['+38 (044) 492-29-29', '+38 (044) 594-98-98'],
        email: 'office@muk.mn',
      },
      TJ: {
        id: 14,
        address: t('world-map.tj.address'),
        phones: ['+992 (44) 640-66-99', '+992 (44) 660-66-99'],
        email: 'office@muk.tj',
      },
      TM: {
        id: 15,
        address: t('world-map.tm.address'),
        phones: ['+38 (044) 492-29-29'],
        email: 'Turkmenistan@muk.ua',
      },
      UZ: {
        id: 16,
        address: t('world-map.uz.address'),
        phones: ['+998 (71) 2051012'],
        email: 'office@muk.uz',
      },
      HR: {
        id: 17,
        address: t('world-map.hr.address'),
        phones: ['+38 044 492-29-29', '594-98-98'],
        email: 'int@muk.ua',
      },
    },
  }
}
