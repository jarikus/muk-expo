import React, { useRef, useEffect, useState } from 'react'
import { worldMapConfig } from './map-config'
import { countriesPath } from './countries'
import { TouchScroll } from './touch-scroll'
import { responsiveCard } from 'utils/data'
import { createMarkup, debouncedDecorator } from 'utils/common'
import { detect } from 'detect-browser'
import Carousel from 'react-multi-carousel'
import clsx from 'clsx'

const selectedCountry = 'UA'

export function WorldMap({ t }) {
  const [markers, setMarkers] = useState(null)
  const [key, setKey] = useState(0)
  const [showBanner, setShowBanner] = useState(false)
  const [countryInfo, setCountryInfo] = useState(null)
  const [lineRect, setLineRect] = useState(null)

  const worldMapRef = useRef()
  const worldMapBgRef = useRef()
  const worldMapSVG = useRef()
  const bannerLineRef = useRef()
  const worldMapScrollRef = useRef()
  const carouselRef = useRef()
  const { countries, offsetTop, zoom } = worldMapConfig({ t })

  const sliderOptionsOne = responsiveCard()

  useEffect(() => {
    const viewer = new TouchScroll()
    viewer.init({
      id: 'worldmapscroll',
      draggable: true,
      wait: false,
    })
    if (worldMapSVG.current) {
      calcMapBgWidth()
      setMarkersRect()
    }
    window.addEventListener('resize', reload, { passive: true })
    return () => {
      window.removeEventListener('resize', reload)
    }
  }, [])

  useEffect(() => {
    if (worldMapSVG.current) {
      setTimeout(() => {
        const span = document.getElementById(`marker${selectedCountry}`)
        const svg = document.getElementById(selectedCountry)
        span.classList.add('selected')
        svg.classList.add('selected')
        if (2 > key) setKey(key + 1)
        onMouseEnter()
      }, 1000)
    }
  }, [key])

  useEffect(() => {
    let elem = worldMapScrollRef.current
    let value = elem.offsetWidth

    let scrollIndex
    switch (true) {
      case value > 1440:
        scrollIndex = 5
        break
      case value > 1200:
        scrollIndex = 4
        break
      case value > 992:
        scrollIndex = 3
        break
      default:
        scrollIndex = 2
        break
    }
elem.scrollTo && elem.scrollTo(elem.scrollWidth / scrollIndex, 0)
  }, [])

  useEffect(() => {
    if (worldMapSVG.current) {
      calcMapBgWidth()
    }
  }, [worldMapSVG.current])

  const reload = debouncedDecorator(() => {
    calcMapBgWidth()
    setMarkersRect()
  }, 66)

  function buildLine(id) {
    let target = document.getElementById('marker' + id)
    let bannerLineRect = bannerLineRef.current.getBoundingClientRect()

    let x1 = bannerLineRect.left
    let y1 = bannerLineRect.bottom + (window.scrollY || pageYOffset)
    let x2 = target.getBoundingClientRect().left + target.offsetWidth / 2
    let y2 =
      target.getBoundingClientRect().top +
      target.offsetHeight / 2 +
      (window.scrollY || pageYOffset)
    setLineRect({ x1, x2, y1, y2 })
  }

  function onMouseEnter(event) {
    event && event.stopPropagation()
    event && event.preventDefault()
    const id = event ? event.target.id : 'UA'
    const browser = detect()
    if (window.matchMedia('(hover: hover)').matches || browser.name === 'ie') {
      const country = countries[id]
      setCountryInfo(country)
      setShowBanner(true)

      buildLine(id)

      toggleSelected(id, true)
      countriesPath.forEach((item) => {
        item.id === id ? (item.className = 'selected') : (item.className = '')
      })
    }
  }

  function onMouseLeave(event) {
    setShowBanner(false)
    setCountryInfo(null)
    setLineRect(null)
    toggleSelected(event.target.id, false)
    countriesPath.forEach((item) => (item.className = ''))
  }

  function onClick(event) {
    event.stopPropagation()
    if (!window.matchMedia('(hover: hover)').matches) {
      setCountryInfo(countries[event.target.id])
      setShowBanner(true)

      toggleSelected(event.target.id, true)
      // carouselRef.current.goToSlide(country.id + 1)

      countriesPath.forEach((item) => {
        item.id === event.target.id
          ? (item.className = 'selected')
          : (item.className = '')
      })
    }
  }

  // function onSlide({ currentSlide, nextSlide }) {
  //   let country =
  //     countriesPath.find((i) => {
  //       if (nextSlide === 1) {
  //         return i.number === 16
  //       } else {
  //         return i.number === nextSlide - 1
  //       }
  //     }) || {}
  //
  //   let svg = worldMapSVG.current
  //   const countriesArr = svg && Array.from(svg.childNodes)
  //   countriesArr.forEach((i, ind) => {
  //     if (currentSlide - 1 === ind + 1) {
  //       i.classList.add('selected')
  //     } else {
  //       i.classList.remove('selected')
  //     }
  //   })
  //
  //   setShowBanner(true)
  //   toggleSelected(country.id, true)
  // }

  function toggleSelected(id, boolean) {
    let markerId = `marker${id}`
    console.log('markersArr', markers)
    if (!markers) {
      return
    }
    markers.forEach((i) => (i.selected = false))
    let item = markers && markers.find((item) => item.id === markerId)
    if (item) item.selected = boolean

    let arr = markers
    arr.splice(arr.indexOf(item), 1, item)
    setMarkers(arr)
  }

  function setMarkersRect() {
    let svg = worldMapSVG.current
    const countriesArr = svg && Array.from(svg.childNodes)
    let markersArr = []
    countriesArr.forEach((element) => {
      const attr = element.getAttribute('id')
      const rect = element.getBoundingClientRect()

      let markerX = Math.round(
        rect.left +
          (rect.right - rect.left) / 2 -
          svg.getBoundingClientRect().left
      )

      let markerY = Math.round(
        rect.top +
          (rect.bottom - rect.top) / 2 -
          svg.getBoundingClientRect().top
      )

      let top = offsetTop
      const browser = detect()
      if (browser.name === 'ie') {
        top = 0
      }
      markersArr.push({
        id: `marker${attr}`,
        style: {
          left: `${markerX}px`,
          top: `calc(${markerY}px + ${top}vh)`,
        },
        selected: false,
      })
    })
    setMarkers(markersArr)
  }

  function calcMapBgWidth() {
    document.getElementById('worldmapsvg').style.height = zoom + 'vh'
    document.getElementById('worldmapbg').style.width =
      worldMapSVG.current.getBoundingClientRect().width + 'px'
    document.getElementById('worldmapbg').style.backgroundPosition =
      'center top ' + parseInt(offsetTop) + 'vh'
    document.getElementById('worldmapsvg').style.top = offsetTop + 'vh'

    const browser = detect()
    if (browser.name === 'ie') {
      document.getElementById('worldmapsvg').style.height = 0
      document.getElementById('worldmapsvg').style.padding = 0
      document.getElementById('worldmapbg').style.backgroundPosition =
        'center top'
      document.getElementById('worldmapsvg').style.top = 0
      document.getElementById('worldmapsvg').style.paddingBottom = zoom + 'vh'
    }
  }

  let Marker = ({ selected, style, id }) => (
    <span
      id={id}
      className={clsx('map__marker', selected && 'selected')}
      style={style}
    />
  )

  let Banner = ({ showBanner, countryInfo = {}, mobile }) => {
    const { address, phones, email } = countryInfo || {}
    return (
      <div
        className={clsx(
          'map__banner custom-card text-white',
          showBanner && 'show',
          mobile && 'mobile'
        )}
      >
        {countryInfo && (
          <>
            {address && (
              <div
                className='custom-card__title text-white'
                dangerouslySetInnerHTML={createMarkup(address)}
              />
            )}

            {phones && (
              <div className='custom-card__title text-white d-flex'>
                <span className='d-block mr-2'>{t('world-map.phone')}: </span>
                <div>
                  {phones.map((item, index) => (
                    <a href={`tel${item}`} key={index} className='d-block'>
                      {item}
                    </a>
                  ))}
                </div>
              </div>
            )}
            {email && (
              <div className='custom-card__title text-white'>
                <span>{t('world-map.email')}:</span> {email}
              </div>
            )}
          </>
        )}
        {!mobile && <span ref={bannerLineRef} className='map__banner-line' />}
      </div>
    )
  }

  let LineToBanner = ({ show, rect }) => (
    <line id='map__line' {...rect} className={clsx(show && 'show')} />
  )

  return (
    <div id='worldmap' ref={worldMapRef}>
      <div id='worldmapscroll' ref={worldMapScrollRef}>
        <div id='worldmapbg' ref={worldMapBgRef} key={key}>
          <svg
            id='worldmapsvg'
            ref={worldMapSVG}
            width='1009.6727'
            height='665.96301'
            viewBox='0 0 1009.6727 665.96301'
            preserveAspectRatio='xMidYMin slice'
          >
            {countriesPath.map(({ path, id, title, className }) => (
              <path
                d={path}
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
                onClick={onClick}
                id={id}
                key={id}
                title={title}
                className={className}
              />
            ))}
          </svg>
        </div>
        {markers &&
          markers.map((item, index) => <Marker key={index} {...item} />)}
      </div>
      <Banner showBanner={showBanner} countryInfo={countryInfo} />
      {/* <Carousel
        ref={carouselRef}
        additionalTransfrom={0}
        ssr
        infinite={true}
        arrows={false}
        draggable={true}
        showDots
        responsive={sliderOptionsOne}
        minimumTouchDrag={100}
        focusOnSelect={false}
        sliderClass='pb-3'
        beforeChange={(nextSlide, { currentSlide }) =>
          onSlide({ nextSlide, currentSlide })
        }
      >
        {Object.values(countries).map((item, index) => (
          <Banner showBanner={true} countryInfo={item} key={index} mobile />
        ))}
      </Carousel> */}
      <svg id='map__svg'>
        <LineToBanner rect={lineRect} show={showBanner} />
      </svg>
    </div>
  )
}
