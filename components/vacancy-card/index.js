import React from 'react'
import { Card, CardBody } from 'reactstrap'
import { Link } from 'utils/with-i18next'
import { SharingButtons } from 'components/sharing-buttons'
import { createMarkup } from 'utils/common'

export function VacancyCard({ title, location, link, description, id, t }) {
  const href = {
    pathname: '/company/vacancyi/[id]',
    query: { id },
  }
  const as = `/company/vacancyi/${id}`

  return (
    <Card className='vacancy-card w-100 py-3'>
      <CardBody className='w-100'>
        <div className='row'>
          <div className='col-md-3 col-12'>
            <div className='vacancy-card-title'>{title}</div>
            <div className='vacancy-card-subtitle'>{location}</div>
          </div>
          <div className='col-md-1 col-0'>
            <div className='line-v' />
          </div>
          <div className='col-md-8 col-12'>
            <div
              dangerouslySetInnerHTML={createMarkup(description)}
              className='vacancy-description text mt-3'
            />
            <div className='d-flex justify-content-between align-items-center flex-wrap'>
              <div className='mr-3 detail'>
                <Link href={href} as={as}>
                  <a>{t('common.showMore')}</a>
                </Link>
              </div>
              <div className='d-flex align-items-center'>
                {t('vacancyPage.sayFriends')}: <SharingButtons link={href} />
              </div>
            </div>
          </div>
        </div>
      </CardBody>
    </Card>
  )
}
