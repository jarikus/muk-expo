import React from 'react'
import { brandsList, responsiveCard } from 'utils/data'
import Carousel from 'react-multi-carousel'

const BrandSlider = props => {
  const sliderOptionsOne = responsiveCard({
    desktopItems: 5,
    tabletItems: 4,
    mobileItems: 3,
  })
  return (
    <Carousel
      additionalTransfrom={0}
      ssr
      infinite={true}
      arrows={true}
      draggable={false}
      responsive={sliderOptionsOne}
      minimumTouchDrag={false}
      focusOnSelect={false}
      sliderClass='pb-5'
    >
      {brandsList.map((item, idx) => (
        <div className='brand-item position-relative' key={idx}>
          <div className='brand-card d-flex align-items-center justify-content-center h-100'>
            <img className='img-responsive' src={item.icon} alt='' />
          </div>
        </div>
      ))}
    </Carousel>
  )
}

export default BrandSlider