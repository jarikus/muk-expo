import React from 'react'
import { Link } from 'utils/with-i18next'
import { ProductModal } from 'components/modals'
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  Button,
} from 'reactstrap'
import clsx from 'clsx'
import { ButtonRed, Input } from 'components/form'

export function CourseCard(props) {
  const { name, date, image, link, mode, category, price, oldPrice, t } = props
  const fullLink = `/training/${category}/${link}`

  return (
    <Card className='card card-course mb-5 pt-4 pb-4'>
      <div className='sale'>{t('card.bestSeller')}</div>
      <Link href={fullLink}>
        <a>
          <CardImg top width='100%' src={image} alt='' />
        </a>
      </Link>
      <CardBody>
        <CardTitle>
          <Link href={fullLink}>
            <a>
              <strong className='mt-1 mb-3 d-block fz-18'>{name}</strong>
            </a>
          </Link>
        </CardTitle>
        <CardText>
          {t('card.startDate')} {date}
        </CardText>
        <div className='d-flex align-items-start'>
          <div className='text-center mr-4'>
            {mode !== 'view' && <ModalContentCourse {...props} />}
            <br />
            <Link href={fullLink}>
              <a>
                <Button color='link' className={clsx('btn text-reset pl-0')}>
                  <u>{t('common.moreDetails')}</u>
                </Button>
              </a>
            </Link>
          </div>
          <div className='row price mt-2 align-items-center w-100'>
            <div className='col-sm-7 col-12 text-center'>
              <div className='actual'>{price}</div>
            </div>
            <div className='col-sm-5 col-12'>
              <span className='d-inline-block old'>{oldPrice}</span>
            </div>
          </div>
        </div>
      </CardBody>
    </Card>
  )
}

const ModalContentCourse = ({
  image = '',
  name,
  price,
  oldPrice,
  list,
  date,
  description,
  vendorCode,
  t,
}) => {
  return (
    <ProductModal buttonLabel={t('common.buyCourse')}>
      <div className='title h3'>{t('card.refinementDetails')}</div>
      <div className='row'>
        <div className='col-6'>
          <div className='mb-3'>
            {t('card.leavePhone')}
            <br />
            <span className='font-weight-bold'>{t('card.letsCall')}</span>
          </div>
          <Input className='input mb-3' placeholder={'form.enterPhone'} />
          <ButtonRed>{t('card.clarifyDetails')}</ButtonRed>
          <p className='policy mt-2'>{t('card.personalData')}</p>
        </div>
        <div className='col-6'>
          <Card className='card shadow-none'>
            <div className='font-weight-bold fz-18 mb-3'>{name}</div>
            <div className='row mb-4'>
              <div className='col-12 mb-3'>
                <CardImg top width='100%' src={image} alt='' />
              </div>
              <div className='col-12'>
                {vendorCode && (
                  <p className='article-product mt-2 mb-3'>
                    {t('card.productArticle')}: {vendorCode}
                  </p>
                )}
                {description && <div>{description}</div>}
                {list && (
                  <ul className='mb-4'>
                    {list.map((item, index) => (
                      <li className='mb-2' key={index}>
                        {item}
                      </li>
                    ))}
                  </ul>
                )}
                {date && (
                  <div>
                    {t('common.date')}: {date}
                  </div>
                )}
              </div>
            </div>

            <div className='price'>
              <span className='d-inline-block old'>{oldPrice}</span>
              <div className='actual mt-3'>{price}</div>
            </div>
          </Card>
        </div>
      </div>
    </ProductModal>
  )
}
