import React from 'react'
import { Link } from 'utils/with-i18next'
import { ProductModal } from 'components/modals'
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
} from 'reactstrap'
import clsx from 'clsx'
import { ButtonRed, Input } from 'components/form'

export function PreviewCard({ name, description, image, link = '', t }) {
  return (
    <Card className='mb-5 pt-4 pb-4 pr-3 pl-3 card'>
      <Link href={link}>
        <a>
          <CardImg top width='100%' src={image} alt='' />
        </a>
      </Link>
      <CardBody>
        <CardTitle>
          <Link href={link}>
            <a>
              <strong className='mt-4 mb-3 d-block'>{name}</strong>
            </a>
          </Link>
        </CardTitle>
        <CardText>{description}</CardText>
      </CardBody>
    </Card>
  )
}

export function ProductCard(props) {
  const { name, description, image, link, mode, t, price, oldPrice } = props
  return (
    <Card className='card mb-5 pt-4 pb-4 pr-3 pl-3'>
      <div className='sale'>{t('card.bestSeller')}</div>
      <Link href={link}>
        <a>
          <CardImg top width='100%' src={image} alt='' />
        </a>
      </Link>
      <CardBody>
        {mode !== 'view' && (
          <CardSubtitle className='availability'>
            {t('card.areAvailable')}
          </CardSubtitle>
        )}
        <CardTitle>
          <Link href={link}>
            <a>
              <strong className='mt-4 mb-3 d-block'>{name}</strong>
            </a>
          </Link>
        </CardTitle>
        <CardText>{description}</CardText>
        {mode !== 'view' && (
          <ul className='details mb-4'>
            <li className='mb-2'>Бренд: PowerEdge</li>
            <li className='mb-2'>Форм фактор: Стойка</li>
            <li className='mb-2'>Тип: Блейд сервер</li>
          </ul>
        )}
        <div className='row price mb-4 align-items-center'>
          <div className='col-7'>
            <div className='actual'>{price}</div>
          </div>
          <div className='col-5'>
            <span className='d-inline-block old'>{oldPrice}</span>
          </div>
        </div>
        <div className='d-flex align-items-center justify-content-between'>
          {mode !== 'view' && <ProductModalContent {...props} />}
          <Link href={link}>
            <a>
              <Button
                color='link'
                className={clsx(
                  'btn text-reset pl-0',
                  mode === 'view' && 'pl-0'
                )}
              >
                {t('common.moreDetails')}
              </Button>
            </a>
          </Link>
        </div>
      </CardBody>
    </Card>
  )
}

export const ProductModalContent = ({
  image = '',
  name,
  price,
  oldPrice,
  list,
  date,
  description,
  t,
}) => {
  return (
    <ProductModal buttonLabel='Купить'>
      <div className='title h3'>{t('card.refinementDetails')}</div>
      <div className='row'>
        <div className='col-sm-6 col-12 order-1'>
          <div className='mb-3'>
            {t('card.leavePhone')}
            <br />
            <span className='font-weight-bold'>{t('card.letsCall')}</span>
          </div>
          <Input className='input mb-3' placeholder={t('form.enterPhone')} />
          <ButtonRed>{t('card.clarifyDetails')}</ButtonRed>
          <p className='policy mt-2'>{t('card.personalData')}</p>
        </div>
        <div className='col-sm-6 col-12 order-0 order-sm-2'>
          <Card className='card shadow-none'>
            <div className='font-weight-bold'>{name}</div>
            <div className='row mb-4'>
              <div className='col-6'>
                <p className='article-product mt-2'>
                  {t('card.productArticle')}: 135591137
                </p>
                {description && <div>{description}</div>}
                {list && (
                  <ul className='mb-4'>
                    {list.map((item, index) => (
                      <li className='mb-2' key={index}>
                        {item}
                      </li>
                    ))}
                  </ul>
                )}
                {date && (
                  <div>
                    {t('common.date')}: {date}
                  </div>
                )}
              </div>
              <div className='col-sm-6 col-12'>
                <CardImg top width='100%' src={image} alt='' />
              </div>
            </div>

            <div className='price'>
              <span className='d-inline-block old'>{oldPrice}</span>
              <div className='actual mt-3'>{price}</div>
            </div>
          </Card>
        </div>
      </div>
    </ProductModal>
  )
}
