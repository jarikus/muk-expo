import React from 'react'
import { Link } from 'utils/with-i18next.js'
import division1 from 'public/images/divisionBG/division1.jpg'

export function SolutionCard({
  title,
  color = '#d01018',
  description = 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
  linkGroup,
  linkDivision,
  background = division1,
  t,
}) {
  return (
    <div
      className='solution-card d-flex flex-column justify-content-between align-items-start'
      style={{ backgroundImage: `url(${background})` }}
    >
      <h4 className='solution-card__title'>{title}</h4>
      {description && (
        <>
          <span
            className='solution-card__line'
            style={{ backgroundColor: `${color}` }}
          ></span>
          <p className='solution-card__description'> {description}</p>
        </>
      )}
      <Link
        href={{
          pathname: '/solutions/[division]/[group]',
          query: {
            division: linkDivision,
            group: linkGroup,
          },
        }}
        as={`/solutions/${linkDivision}/${linkGroup}`}
      >
        <a className='solution-card__link'>
          {t('header.menu.solutions.linkName')}
        </a>
      </Link>
    </div>
  )
}
