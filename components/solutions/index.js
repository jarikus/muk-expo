import React from 'react'
import { Link } from 'utils/with-i18next'

export function SolutionsList({ groups = [], link }) {
  return groups.map((group, index, arr) => (
    <React.Fragment key={index}>
      <div className='row'>
        <div className='col-12'>
          <Link
            href={{
              pathname: '/solutions/[division]/[group]',
              query: {
                division: link,
                group: group.link,
              },
            }}
            as={`/solutions/${link}/${group.link}`}
          >
            <a>
              <h5 className='h5 mt-5 mb-4'>{group.name}</h5>
            </a>
          </Link>
        </div>
        {group.vendors.map((vendor, ind) => (
          <div key={ind} className='col-md-3 col-sm-4 col-12'>
            <div className='mb-3'>
              <Link
                href={{
                  pathname: '/solutions/[division]/[group]/[solution]',
                  query: {
                    division: link,
                    group: group.link,
                    solution: vendor.link,
                  },
                }}
                as={`/solutions/${link}/${group.link}/${vendor.link}`}
              >
                <a>{vendor.name}</a>
              </Link>
            </div>
          </div>
        ))}
      </div>
      {arr.length - 1 !== index && <div className='line mt-4' />}
    </React.Fragment>
  ))
}

export function SolutionsListVendors({ vendors = [], divisionLink, link }) {
  return vendors.map((vendor, ind) => (
    <div key={ind} className='col-md-3 col-sm-4 col-12'>
      <div className='mb-3'>
        <Link
          href={{
            pathname: '/solutions/[division]/[group]/[solution]',
            query: {
              division: divisionLink,
              group: link,
              solution: vendor.link,
            },
          }}
          as={`/solutions/${divisionLink}/${link}/${vendor.link}`}
        >
          <a>{vendor.name}</a>
        </Link>
      </div>
    </div>
  ))
}
