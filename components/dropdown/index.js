import React, { useState } from 'react'
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap'
import { debouncedDecorator } from 'utils/common'

export const HoverControlledDropdown = ({ name, menu }) => {
  const [isOpen, updateIsOpen] = useState(false)

  const handleToggle = debouncedDecorator((data) => {
    updateIsOpen(data)
  }, 300)

  let ReactElement
  if (typeof menu === 'function') ReactElement = menu
  return (
    <Dropdown
      isOpen={isOpen}
      onMouseOver={() => handleToggle(true)}
      onFocus={() => handleToggle(true)}
      onMouseLeave={() => handleToggle(false)}
      onBlur={() => handleToggle(false)}
      toggle={() => handleToggle(!isOpen)}
      className='hover-controlled'
    >
      <DropdownToggle caret>{name}</DropdownToggle>
      <DropdownMenu>
        {typeof menu === 'object' &&
          menu.map((item, index) => (
            <React.Fragment key={index}>
              <DropdownItem>{item}</DropdownItem>
            </React.Fragment>
          ))}
        {typeof menu === 'function' && <ReactElement />}
      </DropdownMenu>
    </Dropdown>
  )
}
