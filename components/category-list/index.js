import React from 'react'
import { Link } from 'utils/with-i18next'

export function Categories({ list = [] }) {
  return (
    <>
      {list.map(({ name = '', list = [] }, index, arr) => {
        return (
          <React.Fragment key={index}>
            <div className='row'>
              <div className='col-12'>
                <h5 className='h5 mt-5 mb-4'>{name}</h5>
              </div>
              {list.map(({ name, link }, ind) => (
                <div key={ind} className='col-md-3 col-sm-4 col-12'>
                  <div className='mb-3'>
                    <Link href={`/category/${link}`}>
                      <a>{name}</a>
                    </Link>
                  </div>
                </div>
              ))}
            </div>
            {arr.length - 1 !== index && <div className='line mt-4' />}
          </React.Fragment>
        )
      })}
    </>
  )
}
