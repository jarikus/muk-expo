import React, { useState } from 'react'

import dynamic from 'next/dynamic'
const Dropdown = dynamic(() => import('react-dropdown'), {
  ssr: false,
})

const options = [{ value: 'kyiv', label: 'Киев' }]

export function ChangeCity() {
  const [city, setCity] = useState('kyiv')

  const _onSelect = (e) => {
    setCity(e.value)
  }

  return (
    <Dropdown
      options={options}
      onChange={(value) => _onSelect(value)}
      value={city}
      placeholder='Change city'
    />
  )
}
