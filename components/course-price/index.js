import React from 'react'
import Icon from '../icons'
import { ButtonRed } from '../form/button'
import servicePreview from 'public/images/service/service.png'

const CoursePrice = ({ course, t, small, service }) => {
  const {
    title,
    cod,
    list,
    day,
    hour,
    date,
    price,
    priceOld,
    description,
  } = course
  return (
    <>
      {!small && (
        <h2 className='title title__section title__section--training fz-30'>
          {title}
        </h2>
      )}
      <div className='course flex'>
        <div className='course__text'>
          {small && (
            <h2 className='title title__section title__section--training fz-30'>
              {title}
            </h2>
          )}
          {(service || !small) && (
            <div className='course__title'>{description}</div>
          )}
          {!small && (
            <div className='course__cod'>
              {t('Код')}: {cod}
            </div>
          )}
          <div className='course__info'>
            {(!small || service) &&
              list.map((item, idx) => (
                <div
                  className='course__info-row flex align-items-center'
                  key={idx}
                >
                  <div className='course__info-ico'>
                    <Icon component={item.icon} />
                  </div>
                  {item.title}
                </div>
              ))}
            {!service && (
              <div className='course__info-row course__info-row-super-info flex'>
                <div>
                  {t('calendar.dayQuantity')}: {day}
                </div>
                <div>
                  {t('calendar.numberHours')}: {hour}
                </div>
                <div>
                  {t('calendar.startDate')}: {date}
                </div>
              </div>
            )}
          </div>
          {!service && (
            <div className='course__payment flex align-items-center'>
              <ButtonRed>{t('common.buyCourse')}</ButtonRed>
              <div className='course__price'>{price} грн.</div>
              <div className='course__price-old'>{priceOld} грн.</div>
            </div>
          )}
        </div>
        <div className='course__preview'>
          <img src={servicePreview} alt={title} className='img-responsive' />
        </div>
      </div>
    </>
  )
}
export default CoursePrice
