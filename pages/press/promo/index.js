import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { Layout } from 'components/layout'
import { withTranslation, Link } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { OurVendors } from 'components/vendors'
import { createMarkup } from 'utils/common'
import shapesPhoto from 'public/images/action-photo.jpg'
import { PaginationComponent } from 'components/pagination'
import { ApiSrv } from 'services'
import { compose } from 'redux'

const PromoNewsList = ({ t, newsList = [], count, lang, vendorList = [] }) => {
  const [list, setList] = useState(newsList)
  const api = new ApiSrv({})

  const getNextNews = async ({ activePage = 1 }) => {
    const data = await api.news.getNews({ lang }, 'promo', {
      limit_count: 20,
      limit_start: (activePage - 1) * 20,
    })
    if (data && data.list) setList(data.list)
  }

  useEffect(() => {
    if (newsList) setList(newsList)
  }, [newsList])

  return (
    <>
      <Head>
        <title>{t('promo.title')}</title>
        <meta
          property='og:title'
          content={t('promo.title')}
          key={t('promo.title')}
        />
      </Head>
      <Layout>
        <section className='catalog-journals-header' />
        <section className='section section__catalog-journals'>
          <div className='container'>
            <Breadcrumbs
              className='catalog-journals-breadcrumbs'
              data={[{ name: t('promo.title') }]}
            />
            <h1 className='title title__section title__section--catalog-journals'>
              {t('promo.title')}
            </h1>
            <div className='section section__eight pb-3'>
              <div className='container'>
                <div className='shares'>
                  {list &&
                    list.length > 0 &&
                    list.map((item, index) => (
                      <div key={index}>
                        <div className='share flex'>
                          <div className='share__preview'>
                            <img
                              src={item.image || shapesPhoto}
                              alt={item.title}
                            />
                          </div>
                          <div className='info-group__body'>
                            <div className='info-group__title'>
                              <Link
                                href={{
                                  pathname: '/press/promo/[id]',
                                  query: { id: item.id },
                                }}
                                as={`/press/promo/${item.id}`}
                              >
                                <a>{item.title}</a>
                              </Link>
                            </div>
                            <div className='info-group__date'>
                              {t('s8.text1')}
                              {item.date}
                            </div>
                            <div className='info-group__partners'>
                              {item.partners}
                            </div>
                            <div
                              className='info-group__description'
                              dangerouslySetInnerHTML={createMarkup(
                                item.description_short
                              )}
                            />
                          </div>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
            {count > 20 && (
              <div className='d-flex justify-content-center'>
                <PaginationComponent
                  totalItems={count}
                  pageSize={20}
                  size='sm'
                  nextPageText={'next'}
                  previousPageText={'previous'}
                  // previousPageText={
                  //   <Icon
                  //     component='arrowRight'
                  //     style={{ transform: 'rotate(180deg)' }}
                  //   />
                  // }
                  onSelect={getNextNews}
                  hideNextOrPrevious={true}
                  showFirstLastButton={true}
                />
              </div>
            )}
          </div>
          <div className='mt-5'>
            <OurVendors list={vendorList} t={t} />
          </div>
        </section>
      </Layout>
    </>
  )
}

PromoNewsList.getInitialProps = async (ctx) => {
  try {
    const { getNews } = ctx.api.news
    const res = await getNews(ctx, 'promo')
    const { getVendorList } = ctx.api.vendor
    const vendors = await getVendorList(ctx, { result: 'simple' })
    const { list, count } = res

    return {
      namespacesRequired: ['common'],
      newsList: list,
      count,
      vendorList: vendors.list,
    }
  } catch {
    return {
      namespacesRequired: ['common'],
    }
  }
}
export default compose(withTranslation('common'))(PromoNewsList)
