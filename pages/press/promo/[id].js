import React, { useState, useEffect } from 'react'
import Head from 'next/head'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { OurVendors } from 'components/vendors'
import { createMarkup } from 'utils/common'
import { CustomModal } from 'components/modals/custom-modal'
import { NoTranslateModal } from 'components/news'

const PromoNews = ({ t, news = {}, noArticle = {}, list = {} }) => {
  const [openModal, setOpenModal] = useState(false)
  const { title, description } = news
  const { message, available_translation } = noArticle

  useEffect(() => {
    if (message === 'not translated') setOpenModal(true)
  }, [message])

  return (
    <>
      <Head>
        {title && <title>{title}</title>}
        <meta property='og:title' content={title} key={title} />
      </Head>
      <Layout>
        <section className='catalog-journals-header' />
        <section className='section section__catalog-journals'>
          <div className='container'>
            <Breadcrumbs
              className='catalog-journals-breadcrumbs'
              data={[
                { name: t('news.promoTitle'), link: '/press/promo' },
                { name: title },
              ]}
            />
            <div className='row'>
              <div className='col-12'>
                <h1 className='title title__section title__section--catalog-journals'>
                  {title}
                </h1>
                <div dangerouslySetInnerHTML={createMarkup(description)} />
              </div>
            </div>
          </div>
          <div className='mt-5'>
            <OurVendors t={t} list={list} />
          </div>
        </section>
        <CustomModal isOpen={openModal}>
          <NoTranslateModal
            available_translation={available_translation}
            t={t}
            setOpenModal={setOpenModal}
          />
        </CustomModal>
      </Layout>
    </>
  )
}

PromoNews.getInitialProps = async (ctx) => {
  try {
    const { getSingleNews } = ctx.api.news
    const { getVendorList } = ctx.api.vendor
    const { list } = await getVendorList(ctx)
    const { news, message } = await getSingleNews(ctx, { result: 'simple' })

    return {
      namespacesRequired: ['common'],
      news,
      noArticle: message,
      list,
    }
  } catch {
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(PromoNews)
