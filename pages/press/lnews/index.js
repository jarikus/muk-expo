import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation, Link } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { OurVendors } from 'components/vendors'
import { createMarkup } from 'utils/common'

const NewsListLanguageCentre = ({ t, newsList = [], list }) => {
  return (
    <Layout>
      <section className='catalog-journals-header' />
      <section className='section section__catalog-journals'>
        <div className='container'>
          <Breadcrumbs
            className='catalog-journals-breadcrumbs'
            data={[{ name: t('news.title') }]}
          />
          <h1 className='title title__section title__section--catalog-journals'>
            {t('news.title')}
          </h1>
          <div className='row'>
            <div className='col-12'>
              {/* {newsList.map(({ title, description_short, id }) => {
                return (
                  <div key={id} className='mb-5'>
                    <Link href='/news/:id' as={`/news/${id}`}>
                      <h1 className='title cursor-pointer'>{title}</h1>
                    </Link>
                    <div
                      dangerouslySetInnerHTML={createMarkup(description_short)}
                    />
                  </div>
                )
              })} */}
            </div>
          </div>
        </div>
        <div className='mt-5'>
          <OurVendors t={t} list={list} />
        </div>
      </section>
    </Layout>
  )
}

NewsListLanguageCentre.getInitialProps = async (ctx) => {
  const { getVendorList } = ctx.api.vendor
  const { list } = await getVendorList(ctx, { result: 'simple' })
  return {
    list,
    namespacesRequired: ['common'],
  }
}

export default withTranslation('common')(NewsListLanguageCentre)
