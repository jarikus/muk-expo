import React from 'react'
import App from 'next/app'
import Head from 'next/head'
import { appWithTranslation } from 'utils/with-i18next'
import { compose } from 'redux'
import withCache from 'utils/cache'
import { withAppApi } from 'services/withApi'
import { UnauthorizedError } from 'utils/error'
import { ThemeProvider } from 'styled-components'
import { GlobalStyle, theme } from 'components/styled-components'

import 'style/index.scss'

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    const { api, cache, req, lang } = ctx

    const langOld = cache.get('lang')
    const solutionsOld = cache.get('solutions')
    const langChange = !!langOld && langOld !== lang
    let error = { unauthorized: false, unknow: false, has: false }

    if (!solutionsOld) {
      const solutions = await api.solutions.getSolutionList(ctx, {})
      const { list } = await api.vendor.getVendorList(ctx, { result: 'simple' })
      cache.set('solutions', solutions)
      cache.set('vendors', list)
    }

    cache.set('lang', lang)

    let pageProps = {}

    const skipFetchData = Component.skipInit
    try {
      pageProps = Component.getInitialProps
        ? await Component.getInitialProps(ctx)
        : {}
    } catch (err) {
      console.error('err', err)
      error.has = true
      if (err instanceof UnauthorizedError) {
        error.unauthorized = true
      } else {
        error.unknow = true
      }
    }

    return {
      pageProps,
      lang,
    }
  }

  render() {
    const { Component, pageProps, cache, lang } = this.props
    const title =
      'MUK - авторизованная дистрибуция серверы, сети, ноутбуки, мобильные решения, сервис'
    return (
      <>
        <GlobalStyle />
        <Head>
          <title>{title}</title>
          <meta property='og:title' content={title} key={title} />
        </Head>
        <ThemeProvider theme={theme}>
          <Component cache={cache} lang={lang} {...pageProps} />
        </ThemeProvider>
      </>
    )
  }
}
export default compose(appWithTranslation, withCache, withAppApi)(MyApp)
