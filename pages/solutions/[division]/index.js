import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import FormITConsultant from 'components/form/itConsultant'
import { SearchBlock } from 'components/form'
import { SolutionsList } from 'components/solutions'

const Division = ({ t, cache, division }) => {
  const { solutions } = cache._data
  const item = solutions.find((item) => item.link === division)

  return (
    <Layout>
      <section className='catalog-solutions pb-5'>
        <div className='container'>
          <Breadcrumbs data={[{ name: item.division_name }]} />
          <h1 className='title title__section text-white pb-3'>
            {t('catalogPage.title')}
          </h1>
          <SearchBlock t={t} />
        </div>
      </section>
      <div className='container'>
        <div className='row'>
          <div className='col-12 mt-5'>
            <h2>{item.division_name}</h2>
          </div>
        </div>
        <SolutionsList {...item} />
      </div>
      <div className='container mt-5'>
        <div className='row'>
          <FormITConsultant t={t} />
        </div>
      </div>
    </Layout>
  )
}

Division.getInitialProps = async (ctx) => {
  try {
    const { division } = ctx.query
    return {
      namespacesRequired: ['common'],
      division,
    }
  } catch (e) {
    console.error('e', e)
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(Division)
