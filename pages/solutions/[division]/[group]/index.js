import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import FormITConsultant from 'components/form/itConsultant'
import { SearchBlock } from 'components/form'
import { SolutionsListVendors } from 'components/solutions'

const Group = ({ t, cache, division, group }) => {
  const { solutions } = cache._data
  const item = solutions.find((item) => item.link === division)
  const selectedGroup = item.groups.find((element) => element.link === group)

  return (
    <Layout>
      <section className='catalog-solutions pb-5'>
        <div className='container'>
          <Breadcrumbs
            data={[
              {
                name: item.division_name,
                link: {
                  pathname: '/solutions/[division]',
                  query: {
                    division: item.link,
                  },
                },
                as: `/solutions/${item.link}`,
              },
              { name: selectedGroup.name },
            ]}
          />
          <h1 className='title title__section text-white pb-3'>
            {selectedGroup.name}
          </h1>
        </div>
      </section>
      <div className='container mt-5'>
        <div className='row'>
          <div className='col-12'>
            <h2 className='title title__section pb-3'>
              {t('solutions.vendors')}
            </h2>
          </div>
        </div>
      </div>
      <div className='container'>
        <div className='row mt-5 pb-5'>
          <SolutionsListVendors {...selectedGroup} divisionLink={item.link} />
        </div>
      </div>
      <div className='container mt-5'>
        <div className='row'>
          <FormITConsultant t={t} />
        </div>
      </div>
    </Layout>
  )
}

Group.getInitialProps = async (ctx) => {
  try {
    const { division, group } = ctx.query
    return {
      namespacesRequired: ['common'],
      division,
      group,
    }
  } catch (e) {
    console.log('e', e)
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(Group)
