import React, { useState } from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import FormITConsultant from 'components/form/itConsultant'
import clsx from 'clsx'
import { createMarkup } from 'utils/common'
import {
  TabContent,
  TabPane,
  Card,
  CardBody,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from 'reactstrap'

const Solution = (props) => {
  const [activeTab, setActiveTab] = useState('1')

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab)
  }

  const { t, product, solution = {}, message, division, group, cache } = props
  const { solutions } = cache._data

  const selectedDivision = solutions.find((item) => item.link === division)
  const selectedGroup =
    (selectedDivision &&
      selectedDivision.groups &&
      selectedDivision.groups.find((el) => el.link === group)) ||
    []

  return (
    <Layout header='white'>
      <section className='catalog pb-3'>
        <div className='container'>
          <Breadcrumbs
            mode='white'
            data={[
              {
                name: selectedDivision.division_name,
                link: {
                  pathname: '/solutions/[division]',
                  query: {
                    division: selectedDivision.link,
                  },
                },
                as: `/solutions/${selectedDivision.link}`,
              },
              {
                name: selectedGroup.name,
                link: {
                  pathname: '/solutions/[division]/[group]',
                  query: {
                    division: selectedDivision.link,
                    group: selectedGroup.link,
                  },
                },
                as: `/solutions/${selectedDivision.link}/${selectedGroup.link}`,
              },
              { name: solution.title },
            ]}
          />
          <h1 className='title text-dark mb-4'>{solution.title}</h1>
        </div>
      </section>
      <div className='container mb-3 mt-3'>
        <div className='row'>
          <div className='col-md-7'>
            <img
              className='img-responsive'
              src={solution.img}
              alt={solution.name}
            />
          </div>
          <div className='col-md-5'></div>
        </div>
      </div>
      <div className='container-fluid mt-4 bg-light tabs-container'>
        <div className='container'>
          <div className='row py-5'>
            <div className='col-md-12'>
              <div
                dangerouslySetInnerHTML={createMarkup(solution.description)}
              />
            </div>
          </div>
        </div>
      </div>
      <div className='container my-5'>
        <div className='row'>
          <div className='col-md-12'>
            <h2 className='mb-5 h2'>{t('solutions.usefulLinks')}</h2>
          </div>
          {solution.usefulLinks.map((link, index) => (
            <div className='col-md-4 col-sm-6 col-12' key={index}>
              <a href={link.link} target='_blank'>
                {link.title}
              </a>
            </div>
          ))}
        </div>
      </div>
      <div className='container mt-5'>
        <FormITConsultant t={t} />
      </div>
    </Layout>
  )
}

Solution.getInitialProps = async (ctx) => {
  try {
    const { division, group, solution } = ctx.query
    const { getSolution } = ctx.api.solutions
    const response = await getSolution(ctx)

    return {
      namespacesRequired: ['common'],
      solution: response.solution,
      message: response.message,
      division,
      group,
    }
  } catch (e) {
    console.error('Product', e)
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(Solution)
