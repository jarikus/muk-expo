const FormData = require('form-data')
import { jsonParse } from 'utils/common'

export default async (req, res) => {
  const { body, method } = req
  switch (method) {
    case 'POST':
      const resp = await getSome(body)
      res.status(200).json({ ...resp })
      break
    default:
      res.setHeader('Allow', ['POST'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}

const getSome = async (data) => {
  const formData = new FormData()
  const fields = jsonParse(data)

  for (let field in fields) {
    formData.append(field, fields[field])
  }

  const url = 'https://backend.muk.ua/feedback'

  const resp = await fetch(url, {
    method: 'POST',
    body: formData,
  })

  return await resp.json()
}
