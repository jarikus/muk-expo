import React from 'react'
import { Layout } from 'components/layout'
import { Link, withTranslation } from 'utils/with-i18next'
import { SearchBlock } from 'components/form'

import { Tab, TabList, TabPanel, Tabs, resetIdCounter } from 'react-tabs'
import {
  responsiveCard,
  reviewEducationalCenter,
  serviceBrandList,
  servicePriceList,
  tariffs,
} from 'utils/data'
import Icon from 'components/icons'
import Certificate from 'public/images/service/certificate.png'
import BrandSlider from 'components/brand-slider'
import ThisGivesPreview from 'public/images/service/advantage.jpg'
import FormConsultation from 'components/form-consultation'

import ico1 from 'public/images/service/ico-computer-tree.png'
import ico2 from 'public/images/service/ico-podpis.png'
import ico3 from 'public/images/service/ico-check-setting.png'

import ico4 from 'public/images/service/ico-graph.png'
import ico5 from 'public/images/service/ico-money.png'
import ico6 from 'public/images/service/ico-medal.png'
import serviceImage from 'public/images/service/service.png'
import MasterSlider from 'components/master-slider'

import dynamic from 'next/dynamic'
import { CertificateModal } from 'components/modals/modal-certificate'
import { SliderModal } from 'components/modals/modal-slider'
import Carousel from 'react-multi-carousel'
const Map = dynamic(() => import('components/map'), {
  ssr: false,
})

const Service = (props) => {
  const { t, apiKey } = props
  const sliderOptionsOne = responsiveCard()

  const courseList = [
    {
      icon: 'serviceComputer',
      title: t('servicePage.privateClients'),
    },
    {
      icon: 'medal',
      title: t('servicePage.managementSystem'),
    },
    {
      icon: 'computer',
      title: t('servicePage.spareParts'),
    },
  ]

  return (
    <Layout header='white'>
      <section className='section section__service-one'>
        <div className='container'>
          <h2 className='title title__section title__section--training fz-30'>
            {t('servicePage.title')}
          </h2>
          <div className='course flex'>
            <div className='course__text'>
              {t('servicePage.description')}
              <div className='course__info'>
                {courseList.map((item, idx) => (
                  <div
                    className='course__info-row flex align-items-center'
                    key={idx}
                  >
                    <div className='course__info-ico'>
                      <Icon component={item.icon} />
                    </div>
                    {item.title}
                  </div>
                ))}
              </div>
            </div>
            <div className='course__preview' style={{ minHeight: '300px' }}>
              <img src={serviceImage} alt={''} />
            </div>
          </div>
        </div>
      </section>
      <section className='section section__service-two'>
        <div className='container'>
          <h2 className='title title__section fz-30'>
            {t('servicePage.makeRepairs')}
          </h2>
          <h5 className='title title__description'>
            {t('servicePage.selectWhatRepairs')}
          </h5>
          <SearchBlock t={t} />
          <div className='tabs-service'>
            <Tabs>
              <TabList>
                <Tab>{t('servicePage.laptopsRepairs')}</Tab>
                <Tab>{t('servicePage.phonesRepairs')}</Tab>
                <Tab>{t('servicePage.tabletsRepairs')}</Tab>
                <Tab>{t('servicePage.repairPC')}</Tab>
              </TabList>
              <TabPanel>
                <table className='service-price-list'>
                  <thead>
                    <tr>
                      <th>{t('servicePage.nameWorks')}</th>
                      <th>{t('servicePage.namePrice')}</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {servicePriceList.map((service) => (
                      <tr key={service.id}>
                        <td>{service.name}</td>
                        <td>
                          {service.ot && t('от')}
                          {service.price}
                        </td>
                        <td>
                          <button>{t('common.order')}</button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
            </Tabs>
          </div>
        </div>
      </section>
      <section className='section section__service-three'>
        <div className='container'>
          <h2 className='title title__section title__section--white fz-30'>
            {t('servicePage.warranty')}
          </h2>
          <div className='flex'>
            <div className='remont-certificates-titles'>
              <h5 className='title title__description title__description--white'>
                {t(
                  'Hp, Hitachi Vantara, Fujitsu, Ibm, Dell Emc, Fortinet, Grandstream, Tripp Lite, Oracle, Apc, Cisco, Extreme, Checkpoint, Enot При обращении в сервисный центр при себе необходимо иметь'
                )}
              </h5>
              <h4 className='title title__description title__description--white'>
                {t('servicePage.card')}
              </h4>
              <h5 className='title title__description title__description--white'>
                {t('servicePage.documents')}
              </h5>
            </div>
            <div className='remont-certificates flex'>
              <div>
                <img src={Certificate} alt='' />
                <CertificateModal t={t}>
                  <img src={Certificate} alt='' />
                </CertificateModal>
              </div>
              <div>
                <img src={Certificate} alt='' />
                <CertificateModal t={t}>
                  <img src={Certificate} alt='' />
                </CertificateModal>
              </div>
              <div>
                <img src={Certificate} alt='' />
                <CertificateModal t={t}>
                  <img src={Certificate} alt='' />
                </CertificateModal>
              </div>
            </div>
          </div>
          <div className='service-brand-list-title'>
            {t('servicePage.checkDevice')}
          </div>
          <div className='service-brand-list flex'>
            {serviceBrandList.map((brand) => (
              <div key={brand.id}>
                <div className='service-brand'>
                  <div className='service-brand__preview'>
                    <img src={brand.image} alt='' />
                  </div>
                  <a href={brand.url} target='_blank'>
                    {t('servicePage.verify')}
                  </a>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
      <section className='section section__service-four'>
        <div className='container'>
          <h2 className='title title__section fz-36'>
            {t('servicePage.officially')}
          </h2>
          <div className='black-slider'>
            <BrandSlider />
          </div>
          <div className='this-gives'>
            <div className='title title__section title__this-gives'>
              {t('servicePage.thisGives')}:
            </div>
            <div className='flex this-gives__advantage'>
              <div className='this-gives__preview'>
                <img src={ThisGivesPreview} alt='' />
              </div>
              <div className='this-gives__list'>
                <div>{t('servicePage.repairTime')}</div>
                <div>{t('servicePage.replacement')}</div>
                <div>{t('servicePage.respect')}</div>
                <div>{t('servicePage.guarantee')}</div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section section__service-five'>
        <div className='container'>
          <h2 className='title title__section fz-30'>
            {t('servicePage.businessWorks')}
          </h2>
          <h5 className='title title__description'>
            {t('servicePage.service')}
          </h5>
          <div className='this-gives'>
            <div className='flex this-gives__advantage'>
              <div className='this-gives__preview'>
                <img src={ThisGivesPreview} alt='' />
              </div>
              <div className='this-gives__body'>
                <p>{t('servicePage.concludeAgreement')}</p>
                <p>{t('servicePage.breakdownLeave')}</p>
                <p>{t('servicePage.notAllowing')}</p>
                <p>{t('servicePage.workCertificate')}</p>
                <p>{t('servicePage.more')}</p>
              </div>
              <div className='this-gives__video'>
                <div className='this-gives__video-preview'>
                  <div className='video'>
                    <iframe
                      src='https://www.youtube.com/embed/MN9-UTsJE-Y?controls=0'
                      frameBorder='0'
                      allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture'
                      allowFullScreen
                    ></iframe>
                  </div>
                </div>
                <div className='flex this-gives__video-btn-group'>
                  <a
                    href='#'
                    download
                    className='this-gives__video-btn-download'
                  >
                    {t('servicePage.download')}
                  </a>
                  <Link href='/'>
                    <a className='this-gives__video-btn-preview'>
                      {t('servicePage.viewReviews')}
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section section__service-six'>
        <div className='container'>
          <h2 className='title title__section fz-30'>
            {t('servicePage.rates')}
          </h2>
          <div className='row'>
            {tariffs.map((tariff, idx) => (
              <div className='col-sm-6 col-md-6 col-lg-3 mb-5' key={idx}>
                <div
                  className={`tariff ${tariff.hit && 'hit'}`}
                  data-text={tariff.hit ? t('servicePage.bestseller') : 'none'}
                >
                  <div className='tariff__title'>{tariff.title}</div>
                  <div className='tariff__region'>
                    {t('servicePage.region')}: <span>{tariff.region}</span>
                  </div>
                  <div className='tariff__title-des'>
                    {t('servicePage.timing')}
                  </div>
                  <div className='tariff__des'>{tariff.description}</div>
                  <div className='tariff__price'>
                    {tariff.price} {t('servicePage.priceTime')}
                  </div>
                  <div className='tariff__add'>
                    {t('servicePage.getConsultation')}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
      <section className='section section__service-seven'>
        <div className='container'>
          <h2 className='title title__section fz-30'>
            {t('servicePage.inviteVendors')}
          </h2>
          <div className='row'>
            <div className='col-md-7'>
              <h5 className='title title__description'>
                {t('servicePage.increaseSales')}
              </h5>
              <div className='standards-list'>
                <div>
                  <img src={ico1} alt='' />
                  {t('servicePage.youGet')}
                </div>
                <div>
                  <img src={ico2} alt='' />
                  {t('servicePage.oneContract')}
                </div>
                <div>
                  <img src={ico3} alt='' />
                  {t('servicePage.willingBuy')}
                </div>
              </div>
            </div>
            <div className='col-md-5'>
              <FormConsultation email />
            </div>
          </div>
        </div>
      </section>
      <section className='section section__service-eight'>
        <div className='container'>
          <h2 className='title title__section fz-36'>
            {t('servicePage.becomePartner')}
          </h2>
          <div className='box-my-partner flex'>
            <FormConsultation />
            <div className='box-my-partner-list'>
              <div>{t('servicePage.get')}</div>
              <div>
                <img src={ico4} alt='' />
                {t('servicePage.customerFlow')}
              </div>
              <div>
                <img src={ico5} alt='' />
                {t('servicePage.surviveCrisis')}
              </div>
              <div>
                <img src={ico6} alt='' />
                {t('servicePage.companyKnow')}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section section__review-slider'>
        <div className='container'>
          <h2 className='title title__section title__section--white fz-30'>
            {t('servicePage.reviews')}
          </h2>
          <div className='tabs-contacts'>
            <Tabs>
              <TabList>
                <Tab> {t('servicePage.companies')}</Tab>
                <Tab>{t('servicePage.private')}</Tab>
              </TabList>
              <TabPanel>
                <div className='tab-contact'>
                  <Carousel
                    additionalTransfrom={0}
                    ssr
                    infinite={true}
                    arrows={true}
                    draggable={false}
                    responsive={sliderOptionsOne}
                    minimumTouchDrag={false}
                    focusOnSelect={false}
                  >
                    {reviewEducationalCenter.map((item, idx) => (
                      <div key={idx}>
                        <div className='review-training-card flex'>
                          <div className='review-training-card__preview'>
                            <img src={item.image} alt='' />
                          </div>
                          <div className='review-training-card__body'>
                            <div className='review-training-card__title-box'>
                              <div className='review-training-card__title fz-24'>
                                {t('servicePage.reviewTitle')}
                              </div>
                              <div className='review-training-card__text fz-18'>
                                {item.title}
                              </div>
                            </div>
                            <div className='review-training-card__title-box'>
                              <div className='review-training-card__title'>
                                {t('servicePage.purpose')}
                              </div>
                              <div className='review-training-card__text'>
                                {item.description}
                              </div>
                            </div>
                            <div className='review-training-card__title-box'>
                              <div className='review-training-card__title'>
                                {t('servicePage.aboutCourse')}
                              </div>
                              <div className='review-training-card__text'>
                                {item.review}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </Carousel>
                </div>
              </TabPanel>
              <TabPanel>
                <div className='tab-contact'>
                  <Carousel
                    additionalTransfrom={0}
                    ssr
                    infinite={true}
                    arrows={true}
                    draggable={false}
                    responsive={sliderOptionsOne}
                    minimumTouchDrag={false}
                    focusOnSelect={false}
                  >
                    {reviewEducationalCenter.map((item, idx) => (
                      <div key={idx}>
                        <div className='review-training-card flex'>
                          <div className='review-training-card__preview'>
                            <img src={item.image} alt='' />
                          </div>
                          <div className='review-training-card__body'>
                            <div className='review-training-card__title-box'>
                              <div className='review-training-card__title fz-24'>
                                {t('Заголовок отзыва')}
                              </div>
                              <div className='review-training-card__text fz-18'>
                                {item.title}
                              </div>
                            </div>
                            <div className='review-training-card__title-box'>
                              <div className='review-training-card__title'>
                                {t('Цель прихода на курс')}
                              </div>
                              <div className='review-training-card__text'>
                                {item.description}
                              </div>
                            </div>
                            <div className='review-training-card__title-box'>
                              <div className='review-training-card__title'>
                                {t('Отзыв о курсе')}
                              </div>
                              <div className='review-training-card__text'>
                                {item.review}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </Carousel>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </section>
      <section className='section section__service-ten'>
        <div className='container'>
          <h2 className='title title__section fz-30'>
            {t('servicePage.entrust')}
          </h2>
          <h5 className='title title__description'>
            {t('servicePage.worksWithSertificates')}
          </h5>
          <MasterSlider t={t} />
        </div>
      </section>
      <section className='section section__service-eleven'>
        <div className='container'>
          <h2 className='title title__section title__section--white fz-30'>
            {t('servicePage.serviseCenterContacts')}
          </h2>
          <div className='tabs-contacts'>
            <Tabs>
              <TabList>
                <Tab>{t('servicePage.main')}</Tab>
                <Tab>{t('servicePage.acceptancePoint')}</Tab>
              </TabList>
              <TabPanel>
                <div className='tab-contact flex'>
                  <div>
                    <div className='contact-box'>
                      <div className='contact-box__title'>
                        {t('servicePage.contacts')}
                      </div>
                      <div className='contact-box__des'>
                        {t('servicePage.telephone')}: (044) 492-29-09, 594-97-97
                        ({t('servicePage.multichannel')})
                      </div>
                      <div className='contact-box__des'>
                        E-mail: service@muk.ua
                      </div>
                    </div>
                    <div className='contact-box'>
                      <div className='contact-box__title'>
                        {t('servicePage.workingHours')}
                      </div>
                      <div className='contact-box__des'>
                        {t('servicePage.weekDays')},
                      </div>
                      <div className='contact-box__des'>
                        {t('servicePage.weekend')}
                      </div>
                    </div>
                    <div className='contact-box'>
                      <div className='contact-box__title'>
                        {t('servicePage.addressTitle')}
                      </div>
                      <div className='contact-box__des'>
                        {t('servicePage.address')}
                      </div>
                    </div>
                    <div className='contact-box'>
                      <div className='contact-box__title'>
                        {t('servicePage.driveThrough')}
                      </div>
                      <div className='contact-box__des'>
                        {t(
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget'
                        )}
                      </div>
                    </div>
                  </div>
                  <div>
                    <div className='karta'>
                      <Map
                        apiKey={apiKey}
                        initialCenter={{ lat: '41.7323738', lng: '44.698428' }}
                        zoom={12}
                      />
                    </div>
                    <div className='karta-btn-group flex'>
                      <SliderModal t={t} />
                      <a href='https://youtube.com/' className='karta-btn flex'>
                        <Icon component='youtube' />
                        {t('servicePage.webCamera')}
                      </a>
                    </div>
                  </div>
                </div>
              </TabPanel>
              <TabPanel>
                <div className='tab-contact flex'>
                  <div>
                    <div className='contact-box'>
                      <div className='contact-box__title'>{t('Контакты')}</div>
                      <div className='contact-box__des'>
                        {t('Телефон')}: (044) 492-29-09, 594-97-97 (
                        {t('многоканальные')})
                      </div>
                      <div className='contact-box__des'>
                        E-mail: service@muk.ua
                      </div>
                    </div>
                    <div className='contact-box'>
                      <div className='contact-box__title'>
                        {t('Время работы')}
                      </div>
                      <div className='contact-box__des'>
                        {t('Пн.-Пт.: 09:00-18:00')},
                      </div>
                      <div className='contact-box__des'>
                        {t('Сб.-Вс. выходные')}
                      </div>
                    </div>
                    <div className='contact-box'>
                      <div className='contact-box__title'>{t('Адрес')}</div>
                      <div className='contact-box__des'>
                        {' '}
                        {t(
                          '03151, г. Киев, ул. Смелянская 17а (район Караваевых дач)'
                        )}
                      </div>
                    </div>
                    <div className='contact-box'>
                      <div className='contact-box__title'>
                        {t('Как проехать')}
                      </div>
                      <div className='contact-box__des'>
                        {' '}
                        {t(
                          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget'
                        )}
                      </div>
                    </div>
                  </div>
                  <div>
                    <div className='karta'>
                      <Map
                        apiKey={apiKey}
                        initialCenter={{ lat: '21.7323738', lng: '55.698428' }}
                        zoom={12}
                      />
                    </div>
                    <div className='karta-btn-group flex'>
                      <SliderModal t={t} />
                      <a href='https://youtube.com/' className='karta-btn flex'>
                        <Icon component='youtube' />
                        {t('Онлайн web камера')}
                      </a>
                    </div>
                  </div>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </section>
    </Layout>
  )
}

Service.getInitialProps = async () => {
  resetIdCounter()
  return {
    namespacesRequired: ['common'],
    apiKey: process.env.GOOGLE_API_KEY,
  }
}

export default withTranslation('common')(Service)
