import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation, Link } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { journals } from 'constants/journals'
import { OurVendors } from 'components/vendors'

const JournalsCatalog = ({ t, list = [] }) => (
  <Layout>
    <section className='catalog-journals-header' />
    <section className='section section__catalog-journals'>
      <div className='container'>
        <Breadcrumbs
          className='catalog-journals-breadcrumbs'
          data={[
            { name: t('journalsPage.breadcrumbs1'), link: '/company' },
            { name: t('journalsPage.breadcrumbs2') },
          ]}
        />
        <h1 className='title title__section title__section--catalog-journals'>
          {t('journalsPage.title')}
        </h1>
        <div className='row journals'>
          {journals.map((journal) => (
            <div className='col-md-3' key={journal.id}>
              <div className='journal'>
                <div className='journal__title'></div>
                <div className='journal__preview'>
                  <img src={journal.img} alt={''} />
                </div>
                <div className='journal__btns flex align-items-center justify-content-between'>
                  <a
                    href='#'
                    onClick={() => window.open(journal.url)}
                    download='pdf'
                    className='link'
                  >
                    {t('journalsPage.download')}
                  </a>
                  <a
                    href={journal.url}
                    className='btn btn__red btn__red--journal'
                    target='_blank'
                  >
                    {t('journalsPage.readOnline')}
                  </a>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      <OurVendors t={t} list={list} />
    </section>
  </Layout>
)

JournalsCatalog.getInitialProps = async (ctx) => {
  try {
    const { getVendorList } = ctx.api.vendor
    const res = await getVendorList(ctx, { result: 'simple' })
    const { list } = res

    return {
      namespacesRequired: ['common'],
      list,
    }
  } catch (error) {
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(JournalsCatalog)
