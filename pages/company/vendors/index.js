import React, { useState, useEffect } from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { VendorPreview } from 'components/vendors'
import FormITConsultant from 'components/form/itConsultant'
import { SearchBlockSimple } from 'components/form'
import { debouncedDecorator } from 'utils/common'

const VendorCatalog = ({ t, list = [] }) => {
  const [vendors, setVendors] = useState([])

  useEffect(() => {
    setVendors(list)
  }, [list])

  const content = [
    ...vendors
      .sort((a, b) => a.name.localeCompare(b.name))
      .reduce((acc, element) => {
        const letter = element.name.charAt(0)
        if (acc.has(letter)) {
          acc.get(letter).push(element)
        } else {
          acc.set(letter, [element])
        }
        return acc
      }, new Map())
      .values(),
  ]

  const handleChange = debouncedDecorator((data) => {
    if (data === '') {
      setVendors(list)
      return
    }
    setVendors(
      list.filter((i) => i.name.toLowerCase().indexOf(data.toLowerCase()) > -1)
    )
  }, 400)

  return (
    <Layout>
      <section className='catalog-vendors pb-5'>
        <div className='container'>
          <Breadcrumbs
            data={[
              { name: t('journalsPage.breadcrumbs1'), link: '/company' },
              { name: t('vendorsPage.breadcrumbs1') },
            ]}
          />
          <h1 className='title title__section text-white pb-4'>
            {t('vendorsPage.title')}
          </h1>
          <SearchBlockSimple t={t} onChange={handleChange} />
        </div>
      </section>
      <div className='container mt-5'>
        {content.length > 0 ? (
          content.map((item, index) => {
            return (
              <div className='row mt-2' key={index}>
                <div className='col-12 mb-4'>
                  <strong>{item[0].name.charAt(0)}</strong>
                </div>
                {item.map((elem, ind) => (
                  <div key={ind} className='col-md-4 col-sm-6 col-12 mb-5'>
                    <VendorPreview {...elem} />
                  </div>
                ))}
              </div>
            )
          })
        ) : (
          <p>Вендоров не найдено</p>
        )}
      </div>
      <div className='container mt-5'>
        <div className='row'>
          <FormITConsultant t={t} />
        </div>
      </div>
    </Layout>
  )
}

VendorCatalog.getInitialProps = async (ctx) => {
  try {
    const { getVendorList } = ctx.api.vendor
    const res = await getVendorList(ctx)
    const { list, count } = res

    return {
      namespacesRequired: ['common'],
      list,
      count,
    }
  } catch (error) {
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(VendorCatalog)
