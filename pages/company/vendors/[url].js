import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import FormITConsultant from 'components/form/itConsultant'
import { products } from 'utils/data'
import { Categories } from 'components/category-list'
import { createMarkup } from 'utils/common'

const Vendor = (props) => {
  const { t, vendor = {} } = props
  const { name, description, img, site, address } = vendor

  return (
    <Layout header='white'>
      <section className='vendor pb-5'>
        <div className='container'>
          <Breadcrumbs
            mode='white'
            data={[
              { name: t('vendorsPage.breadcrumbs1'), link: '/company/vendors' },
              { name },
            ]}
          />
          <h1 className='title text-dark'>Продукция компании {name}</h1>
        </div>
      </section>
      <div className='container'>
        <div className='row mb-4'>
          <div className='col-sm-6'>
            <img className='img-responsive' src={img} alt={name} />
            {site && (
              <a className='link d-block mt-5' href={site}>
                {name}
              </a>
            )}
          </div>
          <div className='col-sm-6'>
            <p
              className='text'
              dangerouslySetInnerHTML={createMarkup(description)}
            />
          </div>
        </div>
        <div className='line' />
      </div>
      {/* TODO - for future */}
      {false && (
        <div className='container mb-4'>
          <Categories list={products} />
          <div className='line mt-3' />
        </div>
      )}
      {address && (
        <div className='container mb-4'>
          <div className='row'>
            <div className='col-md-8 col-12'>
              <h5 className='mb-3 h5'>{t('vendorsPage.officeAddress')}</h5>
              <p className='text'>{address}</p>
            </div>
          </div>
          <div className='line' />
        </div>
      )}
      {/* TODO - for future */}
      {false && (
        <div className='container mb-5'>
          <div className='row'>
            <div className='col-md-8 col-12'>
              <h5 className='mb-3 h5'>{t('vendorsPage.companyNews')}</h5>
              <p className='text'>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
                euismod bibendum laoreet. Proin gravida dolor sit amet lacus
                accumsan et viverra justo commodo. Proin sodales pulvinar sic
                tempor. Sociis natoque penatibus et magnis dis parturient
                montes, nascetur ridiculus mus. Nam fermentum, nulla luctus
                pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin
                sapien nunc accuan eget.
              </p>
            </div>
          </div>
        </div>
      )}
      <div className='container mt-5'>
        <FormITConsultant t={t} />
      </div>
    </Layout>
  )
}

Vendor.getInitialProps = async (ctx) => {
  try {
    const { getVendor } = ctx.api.vendor
    const res = await getVendor(ctx)
    const { vendor } = res

    return {
      namespacesRequired: ['common'],
      vendor,
    }
  } catch (error) {
    console.error('error', error)
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(Vendor)
