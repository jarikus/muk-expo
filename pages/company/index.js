import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { OurVendors } from '../../components/vendors'

const About = ({ t, list }) => (
  <Layout>
    <section className='catalog-journals-header' />
    <section className='section section__catalog-journals'>
      <div className='container'>
        <Breadcrumbs
          className='catalog-journals-breadcrumbs'
          data={[{ name: t('journalsPage.breadcrumbs1') }]}
        />
        <h1 className='title title__section title__section--catalog-journals'>
          {t('aboutPage.title')}
        </h1>
        <div className='row about'>
          <br />
          <br />
          <br />
          <br />
        </div>
      </div>
      <OurVendors t={t} list={list} />
    </section>
  </Layout>
)

About.getInitialProps = async (ctx) => {
  const { getVendorList } = ctx.api.vendor
  const { list } = await getVendorList(ctx, { result: 'simple' })
  return {
    namespacesRequired: ['common'],
    list,
  }
}

export default withTranslation('common')(About)
