import React, { useState } from 'react'
import { Layout } from 'components/layout'
import { i18n, withTranslation, Link } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import Carousel from 'react-multi-carousel'
import { responsiveCard, staffGallery, magazineGallery } from 'utils/data'
import { ButtonRed, Checkbox, CustomCheckbox } from 'components/form'
import { Card, CardBody, Spinner } from 'reactstrap'
import { VacancyCard } from 'components/vacancy-card'
import { FormVacancy } from 'components/form/formVacancy'
import { ProductModal } from 'components/modals'

import advantage1 from 'public/images/vacancies/icons/advantage-1.png'
import advantage2 from 'public/images/vacancies/icons/advantage-2.png'
import advantage3 from 'public/images/vacancies/icons/advantage-3.png'
import youtubeButton from 'public/images/vacancies/icons/youtube-play.svg'
import photoCamera from 'public/images/vacancies/icons/photo-camera.svg'
import { Formik } from 'formik'
import { ApiSrv } from 'services'
import { filteredArray } from 'utils/common'

function createVacancyCategories(array) {
  try {
    return [
      ...array
        .reduce((acc, element) => {
          const department = element.department
          if (!department) return acc
          if (acc.has(department)) {
            acc.get(department).push(element)
          } else {
            acc.set(department, [element])
          }
          return acc
        }, new Map())
        .values(),
    ]
  } catch (e) {
    console.error(e)
    return []
  }
}

const VacancyAll = ({
  t,
  vacanciesList = [],
  count,
  jobsCountry = [],
  jobsOptions = [],
  jobsDepartment = [],
}) => {
  const [isLoading, setLoading] = useState(false)
  const [list, setList] = useState(vacanciesList)
  const [countNumber, setCountNumber] = useState(count)
  const [vacancyCategories, setVacancyCategories] = useState(
    createVacancyCategories(vacanciesList)
  )
  const sliderOptions = responsiveCard({ desktopItems: 2 })
  const sliderOptionsOne = responsiveCard()
  const api = new ApiSrv({})

  const onSubmit = async ({ jobsOptions, jobsCountry, jobsDepartment }) => {
    let options = {}

    if (jobsCountry.length > 0) options.jobsCountry = jobsCountry
    if (jobsDepartment.length > 0) options.jobsDepartment = jobsDepartment
    if (jobsOptions.length > 0) options.jobsOptions = jobsOptions

    const result = await api.vacancies.getVacanciesList(
      { lang: i18n.language },
      options
    )
    if (result.list) {
      setList(result.list)
      setVacancyCategories(createVacancyCategories(result.list))
      setCountNumber(result.count)
    }
  }

  const addMore = async () => {
    try {
      setLoading(true)
      let options = { limit_count: countNumber }

      const result = await api.vacancies.getVacanciesList(
        { lang: i18n.language },
        options
      )
      if (result.list) {
        setList(result.list)
        setVacancyCategories(createVacancyCategories(result.list))
      }

      setLoading(false)
    } catch (e) {
      console.error(e)
      setLoading(false)
    }
  }

  return (
    <Layout className='vacancy-all'>
      <section className='big-dark vacancy text-white pb-5'>
        <div className='container'>
          <h2 className='title mb-3'>
            <span className='mb-3 d-block'>{t('vacanciesPage.title')}</span>
            {t('vacanciesPage.subtitle')}
          </h2>
          <h1 className='title text-uppercase pb-5'>
            {t('vacanciesPage.name')}
          </h1>
          <ul className='d-flex advantage-list flex-column flex-sm-row'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>{t('vacanciesPage.advantage1')}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>{t('vacanciesPage.advantage2')}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('vacanciesPage.advantage3')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container position-relative'>
          <Card className='quote'>
            <CardBody>{t('allVacanciesPage.comment')}</CardBody>
          </Card>
          <Breadcrumbs
            data={[{ name: t('vacanciesPage.breadCrumbs') }]}
            mode='white'
            className='breadcrumbs'
          />
          <h2 className='vacancy__title mt-5'>
            {t('vacanciesPage.foundVacancy')}
          </h2>
          <Formik
            initialValues={{
              jobsOptions: [''],
              jobsCountry: [''],
              jobsDepartment: [''],
            }}
            onSubmit={async (values, { setSubmitting }) => {
              await onSubmit({
                jobsCountry: filteredArray(values.jobsCountry),
                jobsDepartment: filteredArray(values.jobsDepartment),
                jobsOptions: filteredArray(values.jobsOptions),
              })
              setSubmitting(false)
            }}
          >
            {({ handleSubmit, setFieldValue, isSubmitting }) => (
              <form onSubmit={handleSubmit}>
                {jobsOptions.length ? (
                  <div className='row mt-3 mb-3'>
                    {jobsOptions.map(({ name, id }, index) => (
                      <div
                        className='col-md-2 col-6 d-flex  mt-3 mb-3'
                        key={id}
                      >
                        <Checkbox
                          name={`jobsOptions[${index}]`}
                          onChange={(e) =>
                            setFieldValue(
                              `jobsOptions[${index}]`,
                              e.target.checked && id
                            )
                          }
                        >
                          {name}
                        </Checkbox>
                      </div>
                    ))}
                  </div>
                ) : null}
                {jobsCountry.length ? (
                  <div className='row mt-4 mb-5 flex-wrap'>
                    <div className='col-12 mb-4'>{t('Выберите страну')}</div>
                    <div className='col-md-10 col-12 d-flex flex-wrap'>
                      {jobsCountry.map(({ name, iso }, index) => (
                        <div key={index} className='mr-3'>
                          <CustomCheckbox
                            name={`jobsCountry[${index}]`}
                            onChange={(e) =>
                              setFieldValue(
                                `jobsCountry[${index}]`,
                                e.target.checked && iso
                              )
                            }
                          >
                            {name}
                          </CustomCheckbox>
                        </div>
                      ))}
                    </div>
                  </div>
                ) : null}
                {jobsDepartment.length ? (
                  <div className='row mt-5 mb-5'>
                    <div className='col-12 mb-4'>
                      {t('Выберите направление')}
                    </div>
                    <div className='col-md-10 col-12 d-flex flex-wrap'>
                      {jobsDepartment.map(({ name, id }, index) => (
                        <div key={id} className='mr-3'>
                          <CustomCheckbox
                            name={`jobsDepartment[${index}]`}
                            onChange={(e) =>
                              setFieldValue(
                                `jobsDepartment[${index}]`,
                                e.target.checked && id
                              )
                            }
                          >
                            {name}
                          </CustomCheckbox>
                        </div>
                      ))}
                    </div>
                  </div>
                ) : null}
                <div className='mb-5 mt-3'>
                  <ButtonRed disabled={isSubmitting} type='submit'>
                    {t('vacanciesPage.find')}
                  </ButtonRed>
                </div>
              </form>
            )}
          </Formik>
        </div>
      </section>

      <div className='container'>
        <div className='d-flex fz-18'>
          {t('allVacanciesPage.founded')}{' '}
          <div className='font-weight-bold ml-1'> {countNumber}</div>
        </div>
        <div className='row my-5'>
          {list.map((item, index) => (
            <div key={index} className='col-12 mb-5'>
              <VacancyCard {...item} t={t} />
            </div>
          ))}
          {isLoading && (
            <div className='m-auto'>
              <Spinner style={{ width: '3rem', height: '3rem' }} />
            </div>
          )}
        </div>
        {countNumber > 10 && list.length !== countNumber && (
          <div className='mb-5'>
            <ButtonRed onClick={addMore}>{t('common.showMore')}</ButtonRed>
          </div>
        )}
      </div>
      <div className='container'>
        <div className='row'>
          {vacancyCategories.map((item, index) => {
            const title = jobsDepartment.find(
              (element) => element.id === item[0].department
            )
            return (
              <div className='col-md-4 col-sm-6 col-12 mb-5' key={index}>
                <div className='mb-3 fz-18 font-weight-bold'>
                  {title && title.name}
                </div>
                <ul>
                  {item.map((element, ind) => (
                    <li key={ind} className='mb-2'>
                      <Link
                        href={{
                          pathname: '/company/vacancyi/[id]',
                          query: { id: element.id },
                        }}
                        as={`/company/vacancyi/${element.id}`}
                      >
                        <a>{element.title}</a>
                      </Link>
                    </li>
                  ))}
                </ul>
              </div>
            )
          })}
        </div>
      </div>
      <section className='section vacancy-interests'>
        <Card className='no-vacancy text-center'>
          <h2 className='title text-uppercase text-dark'>
            {t('allVacanciesPage.notFounded')}
          </h2>
          <p className='text'>{t('allVacanciesPage.sendResume')}</p>
          <div>
            <ProductModal buttonLabel='Отправить резюме'>
              <ModalContent t={t} />
            </ProductModal>
          </div>
        </Card>
        <div className='container'>
          <div className='row'>
            <div className='col-12 col-md-7 dark-bg'>
              <h2 className='title'>{t('allVacanciesPage.title2')}</h2>
              <div className='text-white mt-3 d-block'>
                {t('allVacanciesPage.comfortWorkplace')}
              </div>
              <ul className='list mt-5 text-white'>
                <li className='list-item mb-5'>
                  {t('allVacanciesPage.comfort1')}
                </li>
                <li className='list-item mb-5'>
                  {t('allVacanciesPage.comfort2')}
                </li>
                <li className='list-item mb-5'>
                  {t('allVacanciesPage.comfort3')}
                </li>
                <li className='list-item'>{t('allVacanciesPage.comfort4')}</li>
              </ul>
              <div className='interests-bottom d-flex align-items-center w-100 text-white'>
                <div>
                  <img
                    src={youtubeButton}
                    alt=''
                    className='img-responsive mr-3'
                  />
                  {t('allVacanciesPage.watchVideo')}
                </div>
                <div className='ml-5'>
                  <img
                    width='40px'
                    className='img-responsive mr-3'
                    src={photoCamera}
                    alt=''
                  />
                  {t('allVacanciesPage.watchPhoto')}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container'>
          <div className='row'>
            <div className='col-12 col-md-5 order-md-1'>
              <div className='dashed-red text-center'>
                {t('allVacanciesPage.workWithUs')}
              </div>
            </div>
            <div className='col-12 col-md-7 order-md-0'>
              <h2 className='vacancy__title mt-5'>
                {t('allVacanciesPage.history')}
              </h2>
              <p className='mt-3'>{t('allVacanciesPage.results')}</p>
              <p className='text-secondary mt-4 mb-0'>
                {t('allVacanciesPage.oneOf')}
              </p>
            </div>
          </div>
          <div className='black-slider mt-3 pb-4'>
            <Carousel
              additionalTransform={0}
              ssr
              infinite={true}
              arrows={true}
              draggable={false}
              responsive={sliderOptions}
              minimumTouchDrag={false}
              focusOnSelect={false}
              sliderClass='pb-5'
            >
              {staffGallery.map((item, idx) => (
                <div className='slider-item text-center' key={idx}>
                  <div className='slider-item__preview'>
                    <img src={item.image} alt='' />
                    <strong className='d-block mt-3'>
                      {t('Иванова Мария')}
                    </strong>
                    <p className='mb-0'>{t('allVacanciesPage.position')}</p>
                  </div>
                </div>
              ))}
            </Carousel>
          </div>
        </div>
      </section>
      <section className='section mt-5 map-wrap'>
        <div className='container'>
          <div className='map-text'>{t('allVacanciesPage.workIn')}</div>
        </div>
      </section>
      <section className='section mt-5'>
        <div className='container'>
          <h2 className='title mb-5 font-weight-bold'>
            {t('allVacanciesPage.about')}
          </h2>
          <div className='row'>
            <div className='col-12 col-md-6 black-slider'>
              <Carousel
                additionalTransform={0}
                ssr
                infinite={true}
                arrows={true}
                draggable={false}
                responsive={sliderOptionsOne}
                minimumTouchDrag={false}
                focusOnSelect={false}
                sliderClass='pb-5'
              >
                {staffGallery.map((item, idx) => (
                  <div className='slider-item' key={idx}>
                    <div className='slider-item__preview'>
                      <p>
                        <span className='font-weight-bold'>
                          {t('Иванова Мария,')}
                        </span>{' '}
                        {t('компания IT brand, Украина')}
                      </p>
                      <img src={item.image} alt='' className='img-responsive' />
                    </div>
                  </div>
                ))}
              </Carousel>
            </div>
            <div className='col-12 col-md-6 black-slider'>
              <Carousel
                additionalTransform={0}
                ssr
                infinite={true}
                arrows={true}
                draggable={false}
                responsive={sliderOptionsOne}
                minimumTouchDrag={false}
                focusOnSelect={false}
                sliderClass='pb-5'
              >
                {magazineGallery.map((item, idx) => (
                  <div className='slider-item' key={idx}>
                    <div className='slider-item__preview'>
                      <p className='font-weight-bold'>{t(item.title)}</p>
                      <img src={item.image} alt='' className='img-responsive' />
                    </div>
                  </div>
                ))}
              </Carousel>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

const ModalContent = ({ t }) => {
  return (
    <div className='vacancy-modal'>
      <div className='title'>{t('allVacanciesPage.sendResume2')}</div>
      <div className='text mt-3 mb-4'>{t('allVacanciesPage.willOpen')}</div>
      <FormVacancy t={t} mode='modal' />
    </div>
  )
}

VacancyAll.getInitialProps = async (ctx) => {
  try {
    const { getVacanciesList, getJobsOptions } = ctx.api.vacancies
    const res = await getVacanciesList(ctx)
    const { jobsCountry, jobsOptions, jobsDepartment } = await getJobsOptions(
      ctx
    )
    const { list, count } = res

    return {
      namespacesRequired: ['common'],
      vacanciesList: list,
      count,
      jobsCountry,
      jobsOptions,
      jobsDepartment,
    }
  } catch (error) {
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(VacancyAll)
