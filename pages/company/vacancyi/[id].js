import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { Button } from 'reactstrap'
import { FormVacancy } from 'components/form/formVacancy'
import { SharingButtons } from 'components/sharing-buttons'
import Head from 'next/head'

import advantage1 from 'public/images/vacancies/icons/advantage-1.png'
import advantage2 from 'public/images/vacancies/icons/advantage-2.png'
import advantage3 from 'public/images/vacancies/icons/advantage-3.png'

import networking from 'public/images/fake-data/networking.jpg'
import { createMarkup } from 'utils/common'

const Vacancy = ({ t, vacancy = {} }) => {
  const { title, id, description, img, address } = vacancy

  return (
    <>
      <Head>
        {title && <title>{title}</title>}
        <meta property='og:title' content={title} key={title} />
      </Head>
      <Layout>
        <section className='big-dark vacancy text-white pb-5'>
          <div className='container'>
            <h2 className='title mb-3'>
              <span className='mb-3 d-block'>{t('vacanciesPage.title')}</span>
              {t('vacanciesPage.subtitle')}
            </h2>
            <h1 className='title text-uppercase pb-5'>
              {t('vacanciesPage.name')}
            </h1>
            <ul className='d-flex advantage-list flex-column flex-sm-row'>
              <li className='advantage-item d-flex d-sm-inline-block'>
                <img src={advantage1} alt='' className='img-responsive' />
                <div className='mt-3'>{t('vacanciesPage.advantage1')}</div>
              </li>
              <li className='advantage-item d-flex d-sm-inline-block'>
                <img src={advantage2} alt='' className='img-responsive' />
                <div className='mt-3'>{t('vacanciesPage.advantage2')}</div>
              </li>
              <li className='advantage-item d-flex d-sm-inline-block'>
                <img src={advantage3} alt='' className='img-responsive' />
                <div className='mt-3'>{t('vacanciesPage.advantage3')}</div>
              </li>
            </ul>
          </div>
        </section>
        <section className='section pt-4 vacancy__card'>
          <div className='container'>
            <Breadcrumbs
              data={[
                {
                  name: t('vacanciesPage.breadCrumbs'),
                  link: '/company/vacancyi',
                },
                { name: title },
              ]}
              mode='white'
            />
            <h2 className='vacancy__title mt-4 mb-4'>{title}</h2>
            <div>
              <strong>{address}</strong>
            </div>
            <div className='vacancy__number mt-2 mb-5'>
              {t('vacancyPage.number')}: {id}
            </div>
            <div className='row mb-5'>
              <div className='col-md-12'>
                <div
                  dangerouslySetInnerHTML={createMarkup(description)}
                  className='vacancy-description'
                />
              </div>
            </div>
            <div className='mb-4'>
              <h2 className='subtitle mb-3'>{t('vacancyPage.photo')}</h2>
              {address && <div>Адрес: {address}</div>}
            </div>
            <div className='d-flex flex-wrap'>
              {img && (
                <div className='photo overflow-hidden'>
                  <img
                    src={`https://backend.muk.ua/${img}`}
                    alt=''
                    className='img-responsive'
                  />
                </div>
              )}
              {false && (
                <div className='photo'>
                  <img src={networking} alt='' className='img-responsive' />
                </div>
              )}
            </div>
            <div className='row mt-5'>
              <div className='col-md-7'>
                <div className='subtitle'>{t('vacancyPage.leaveOrder')}</div>
                <div className='my-4'>{t('vacancyPage.wantPresent')}</div>
                <FormVacancy t={t} />

                <Button
                  color='link'
                  href={'/company/vacancyi'}
                  className='p-0 my-3 text-muted'
                >
                  {t('vacanciesPage.back')}
                </Button>
              </div>
              <div className='col-md-5'>
                <div className='subtitle mb-2'>
                  {t('vacancyPage.sayFriends')}
                </div>
                <SharingButtons />
              </div>
            </div>
          </div>
        </section>
      </Layout>
    </>
  )
}

Vacancy.getInitialProps = async (ctx) => {
  try {
    const { getVacancy } = ctx.api.vacancies
    const { vacancy, message } = await getVacancy(ctx)

    return {
      namespacesRequired: ['common'],
      vacancy,
      message,
    }
  } catch (error) {
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(Vacancy)
