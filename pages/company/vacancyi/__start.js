import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import Carousel from 'react-multi-carousel'
import { responsiveCard, staffGallery } from 'utils/data'
import { ButtonRed, Checkbox, CustomCheckbox } from 'components/form'
import { Button } from 'reactstrap'

import advantage1 from 'public/images/vacancies/icons/advantage-1.png'
import advantage2 from 'public/images/vacancies/icons/advantage-2.png'
import advantage3 from 'public/images/vacancies/icons/advantage-3.png'

const Vacancies = ({ t, list }) => {
  const sliderOptions = responsiveCard({ desktopItems: 2 })
  return (
    <Layout className='vacancy-all'>
      <section className='big-dark vacancy text-white pb-5'>
        <div className='container'>
          <h2 className='vacancy__title mb-3'>
            <span className='mb-3 d-block'>{t('vacanciesPage.title')}</span>
            {t('vacanciesPage.subtitle')}
          </h2>
          <h1 className='vacancy__title text-uppercase pb-5'>
            {t('vacanciesPage.name')}
          </h1>
          <ul className='d-flex advantage-list flex-column flex-sm-row'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>{t('vacanciesPage.advantage1')}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>{t('vacanciesPage.advantage2')}</div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('vacanciesPage.advantage3')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container'>
          <Breadcrumbs
            data={[{ name: t('vacanciesPage.breadCrumbs') }]}
            mode='white'
          />
          <div className='row'>
            <div className='col-12 col-md-7'>
              <h2 className='vacancy__title'>{t('vacanciesPage.freeTime')}</h2>
            </div>
          </div>
          <div className='black-slider mt-5'>
            <Carousel
              additionalTransform={0}
              ssr
              infinite={true}
              arrows={true}
              draggable={false}
              responsive={sliderOptions}
              minimumTouchDrag={false}
              focusOnSelect={false}
              sliderClass='pb-5'
            >
              {staffGallery.map((item, idx) => (
                <div className='slider-item' key={idx}>
                  <div className='slider-item__preview'>
                    <div className='slider-item__title'>{item.title}</div>
                    <img src={item.image} alt='' />
                  </div>
                </div>
              ))}
            </Carousel>
          </div>
        </div>
      </section>
      <div className='container mt-5'>
        <h2 className='vacancy__title'>{t('vacanciesPage.foundVacancy')}</h2>
        <div className='row mt-3 mb-3'>
          <div className='col-md-2 col-6 d-flex mt-3 mb-3'>
            <Checkbox>{t('vacanciesPage.checkbox1')}</Checkbox>
          </div>
          <div className='col-md-2 col-6 d-flex  mt-3 mb-3'>
            <Checkbox>{t('vacanciesPage.checkbox2')}</Checkbox>
          </div>
          <div className='col-md-2 col-12 d-flex mt-3 mb-3'>
            <Checkbox>{t('vacanciesPage.checkbox3')}</Checkbox>
          </div>
        </div>
        <div className='row mt-4 mb-5 flex-wrap'>
          <div className='col-12 mb-4'>{t('Выберите страну')}</div>
          <div className='col-md-10 col-12 d-flex flex-wrap'>
            {list.map((item, index) => (
              <div key={index} className='mr-3'>
                <CustomCheckbox>{item.label}</CustomCheckbox>
              </div>
            ))}
          </div>
        </div>
        <div className='row mt-5 mb-5'>
          <div className='col-12 mb-4'>{t('Выберите направление')}</div>
          <div className='col-md-10 col-12 d-flex flex-wrap'>
            {departments.map((item, index) => (
              <div key={index} className='mr-3'>
                <CustomCheckbox>{item.label}</CustomCheckbox>
              </div>
            ))}
          </div>
        </div>
        <div className='mb-5'>
          <ButtonRed>{t('vacanciesPage.find')}</ButtonRed>
          <br />
          <Button color='link' className='p-0 mt-5 text-muted'>
            {t('vacanciesPage.back')}
          </Button>
        </div>
      </div>
    </Layout>
  )
}

Vacancies.getInitialProps = async (ctx) => {
  try {
    const { getVacanciesList } = ctx.api.vacancies
    const res = await getVacanciesList(ctx)
    const { list, count } = res

    return {
      namespacesRequired: ['common'],
      list,
      count,
    }
  } catch (error) {
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(Vacancies)
