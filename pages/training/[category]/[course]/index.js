import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import { courseProgramList, forFromCourseList } from 'utils/data'
import CoursePrice from 'components/course-price'
import ReviewSlider from 'components/review-slider'

import skill from 'public/images/training/6.jpg'
import certificate from 'public/images/training/7.jpg'
import imgbg1 from 'public/images/training/training-bg1.png'
import imgbg2 from 'public/images/training/training-bg2.png'
import imgbg3 from 'public/images/training/training-bg3.png'
import imgbg4 from 'public/images/training/training-bg4.png'

const course = {
  title: 'AUTOCAD CIVIL 3D 2018/2019 – УГЛУБЛЕННЫЙ КУРС (ГЕНПЛАН)',
  description: 'Станьте экспертом за 5 дней',
  cod: 'ADSK-AC01',
  list: [
    {
      icon: 'youtube',
      title: 'Курс проходит в удобном видео формате',
    },
    {
      icon: 'certificate',
      title: 'Получение международного сертификата',
    },
  ],
  day: 5,
  hour: 5,
  date: '15.05.2020',
  price: '15 000',
  priceOld: '65 500',
  teacher: {
    name: 'Кирова Татьяна',
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.',
    worth:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.',
  },
}

const Training = ({ t }) => {
  return (
    <Layout header='white'>
      <section className='section section__training'>
        <div className='container'>
          <CoursePrice course={course} t={t} />
        </div>
      </section>
      <section className='section section__for-from-course'>
        <div className='container'>
          <h2 className='title title__section title__section--white fz-30'>
            {t('ДЛЯ КОГО ПОДХОДИТ КУРС')}
          </h2>
          <div className='for-from-course-list flex'>
            {forFromCourseList.map((item) => (
              <div key={item.id}>
                <div className='for-from-course'>
                  <div className='for-from-course__preview'>
                    <img src={item.img} alt={item.title} />
                  </div>
                  <div className='for-from-course__title'>{item.title}</div>
                </div>
              </div>
            ))}
          </div>
          <h2 className='title title__section title__section--white fz-30'>
            {t('ПОСЛЕ ОКОНЧАНИЯ КУРСА ВЫ СМОЖЕТЕ')}
          </h2>
          <div className='for-from-course for-from-course--body flex flex align-items-center'>
            <div className='for-from-course__preview'>
              <img src={skill} alt='' />
            </div>
            <div className='for-from-course__content'>
              <div className='for-from-course__title'>
                {t('Делать чертежи квартир, домов, мебели и других объектов')}
              </div>
              <div className='for-from-course__title'>
                {t(
                  'Найти хорошо оплачиваемую работу в сфере строительства или дизайна'
                )}
              </div>
              <div className='for-from-course__title'>
                {t('Ускорить процесс работы над чертежами')}
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section section__training-card'>
        <div className='container'>
          <h2 className='title title__section fz-36'>
            {t('КАК ПРОХОДИТ ОБУЧЕНИЕ?')}
          </h2>

          <div className='who-training flex'>
            <div>
              <div className='who-training__card'>
                <img src={imgbg1} />
                <div className='who-training__card-title'>{t('Занятия')}</div>
                <div className='who-training__card-description'>
                  {t('Занятия проходят в формате вебинаров')}
                </div>
                <div className='who-training__card-description'>
                  {t('Все хранится в личном кабинете')}
                </div>
                <div className='who-training__card-description'>
                  {t(
                    'Каждый урок доступен после того было принято домашнее задание'
                  )}
                </div>
              </div>
            </div>
            <div>
              <div className='who-training__card'>
                <img src={imgbg2} />
                <div className='who-training__card-title'>
                  {t('Домашнее задание')}
                </div>
                <div className='who-training__card-description'>
                  {t(
                    'После каждого урока дается практическое домашнее задание с проверкой и обратной связью от куратора'
                  )}
                </div>
              </div>
            </div>
            <div>
              <div className='who-training__card'>
                <img src={imgbg3} />
                <div className='who-training__card-title'>
                  {t('Сопровождение в процессе обучения')}
                </div>
                <div className='who-training__card-description'>
                  {t(
                    'Каждый студент может пообщаться задавать вопросы куратору курса и  получать ответы в личном кабинете'
                  )}
                </div>
              </div>
            </div>
            <div>
              <div className='who-training__card'>
                <img src={imgbg4} />
                <div className='who-training__card-title'>
                  {t('Защита собственного проекта')}
                </div>
                <div className='who-training__card-description'>
                  {t(
                    'После заврешения обучения вы сдаете эксзамен и получаете сертификат, подтверждающего компетенцию'
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className='section section__course-program'>
        <div className='container'>
          <h2 className='title title__section fz-36'>
            {t('ПРОГРАММА КУРСА ПО AUTOCAD')}
          </h2>
          <div className='course-program-list'>
            {courseProgramList.map((item, idx) => (
              <div key={idx}>
                <div className='course-program flex'>
                  <div className='course-program__preview'>
                    <img src={item.image} alt='' />
                  </div>
                  <div className='course-program__body'>
                    <h3 className='course-program__title'>{item.title}</h3>
                    {item.description.map((des, idx) => (
                      <p key={idx} className='course-program__description'>
                        {des}
                      </p>
                    ))}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
      <section className='section section__course-teacher'>
        <div className='container'>
          <div className='course-teacher-card'>
            <h2 className='title title__section fz-36'>
              {t('ПРЕПОДАВАТЕЛЬ КУРСА')}
            </h2>
            <div className='teacher'>
              <div className='teacher__title'>{course.teacher.name}</div>
              <div className='teacher__description'>{course.teacher.about}</div>
              <div className='teacher__title'>{t('Мои ценности')}</div>
              <div className='teacher__description'>{course.teacher.worth}</div>
            </div>
          </div>
        </div>
      </section>
      <section className='section section__course-small'>
        <div className='container'>
          <CoursePrice course={course} t={t} small={true} />
        </div>
      </section>
      <section className='section section__certificate-training'>
        <div className='container'>
          <div className='certificate-training flex align-items-center'>
            <div>
              <h2 className='title title__section fz-30'>
                {t('ПОЛУЧАЕТЕ СЕРТИФИКАТ СПЕЦИАЛИСТА ПО AUTOCAD')}
              </h2>
              <p className='title__description'>
                {t(
                  'После обучения вы получаете сертификат, подтвержающий ваши знания по AutoCAD'
                )}
              </p>
              <p className='title__description'>
                {t('Отправляем в электоронном виде по email, оригинал почтой')}
              </p>
            </div>
            <div className='flex align-items-center'>
              <div className='certificate-training__preview'>
                <img src={certificate} alt='' />
              </div>
              <div className='certificate-training__preview'>
                <img src={certificate} alt='' />
              </div>
            </div>
          </div>
        </div>
      </section>
      <ReviewSlider t={t} title='ОТЗЫВЫ ТЕХ, КТО ПРОШЕЛ ДАННЫЙ КУРС'/>
      <section className='section section__course-small shadow-inset'>
        <div className='container'>
          <CoursePrice course={course} t={t} small={true} />
        </div>
      </section>
    </Layout>
  )
}

export async function getServerSideProps(ctx) {
  const course = ctx.query.course
  // Pass data to the page via props
  return { props: { course } }
}

export default withTranslation('common')(Training)
