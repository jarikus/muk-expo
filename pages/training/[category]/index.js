import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation, Link } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { courseList } from 'utils/data'
import { Button, Form, FormGroup } from 'reactstrap'
import { PaginationComponent } from 'components/pagination'
import { CourseCard } from 'components/card'
import { CouponForm, FormITConsultantCourse, Select } from 'components/form'

import advantage1 from 'public/images/training/icons/advantage-1.png'
import advantage2 from 'public/images/training/icons/advantage-2.png'
import advantage3 from 'public/images/training/icons/advantage-3.png'

import businessmans from 'public/images/training/businessmans.jpg'
import corporate from 'public/images/training/corporate.jpg'

const courses = new Array(8).fill({
  title: 'Курсы по кластерным решениям',
  link: 'cluster-courses',
})

const options = [
  {
    value: 'Характеристика курса',
    label: 'Характеристика курса',
  },
  { value: 'Длительность курса', label: 'Длительность курса' },
]
const defaultOption = options[0]

const TrainingCategory = (props) => {
  const { t } = props

  return (
    <Layout>
      <section className='big-dark training text-white pb-5'>
        <div className='container'>
          <h1 className='title text-uppercase m-auto pb-4'>
            {t('КУРСЫ ПО AUTOCAD ОТ ЭКСПЕРТОВ УЧЕБНОГО ЦЕНТРА MUK')}
          </h1>
          <h2 className='vacancy__title mb-5'>
            <span className='d-block'>
              {t('Будьте всегда на волне новых знаний')}
            </span>
          </h2>
          <ul className='d-flex advantage-list flex-column flex-sm-row pt-5'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>
                {t(`Удобный формат оффлайн и онлайн обучения`)}
              </div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>
                {t('Получение международного сертификата')}
              </div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('Курсы с учетом реалий 2020 года')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container'>
          <Breadcrumbs data={[{ name: 'Курсы по Autocad' }]} mode='white' />
          <div className='row'>
            <div className='col-md-6 mb-4'>
              <img className='img-responsive' src={businessmans} alt='' />
            </div>
            <div className='col-md-6 mb-4'>
              <img className='img-responsive' src={corporate} alt='' />
            </div>
          </div>
        </div>
      </section>

      <section className='section catalog_courses mt-5 pb-5'>
        <div className='container'>
          <h3 className='font-weight-bold'>
            {t('Подберите курс под свои потребности')}
          </h3>
          <Form className='form-choose-server m-auto pb-0'>
            <FormGroup className='mb-4 pl-3 pr-3 d-flex justify-content-between align-items-center flex-wrap'>
              <div className='d-flex pt-2 pb-2 flex-wrap w-100'>
                <Select
                  name='needed'
                  options={options}
                  defaultOption={defaultOption}
                  value={defaultOption}
                />
                <Select
                  name='vendor'
                  options={options}
                  defaultOption={defaultOption}
                  value={defaultOption}
                />
                <Select
                  name='typeMount'
                  options={options}
                  defaultOption={defaultOption}
                  value={defaultOption}
                />
                <Select
                  name='typeMount2'
                  options={options}
                  defaultOption={defaultOption}
                  value={defaultOption}
                />
              </div>
            </FormGroup>
          </Form>
          <div className='row mt-5'>
            {courseList.map((item, index) => (
              <div className='col-md-6' key={index}>
                <CourseCard {...item} t={t} />
              </div>
            ))}
          </div>
          <div className='row justify-content-center'>
            {/* <PaginationComponent /> */}
          </div>
        </div>
      </section>

      <section className='section section__for-from-course mt-5 text-white py-5'>
        <div className='container'>
          <h3 className='title text-white h3 font-weight-bold mb-5'>
            {t('Часто покупают вместе')}
          </h3>
          <div className='tags flex pb-0'>
            {courses.map(({ title, link }, index) => (
              <div key={index} className=''>
                <Link href={`/category/${link}`}>
                  <a className='tag'>{title}</a>
                </Link>
              </div>
            ))}
          </div>
          <div className='text-right mt-3'>
            <Button color='link' className='text-white'>
              {t('Показать все')}
            </Button>
          </div>
        </div>
      </section>

      <div className='container mt-5'>
        <div className='row'>
          <FormITConsultantCourse t={t} mode='training' />
        </div>
      </div>

      <section className='section mt-5'>
        <CouponForm t={t} />
      </section>
    </Layout>
  )
}

TrainingCategory.getInitialProps = async () => ({
  namespacesRequired: ['common'],
})

export default withTranslation('common')(TrainingCategory)
