import React, { useState } from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'
import { courseList, categoryCourseList, newsItMarketStudy } from 'utils/data'
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  Row,
  Col,
  InputGroup,
  InputGroupAddon,
  Input,
} from 'reactstrap'
import { CourseCard } from 'components/card'
import {
  ButtonRed,
  SearchBlock,
  CouponForm,
  FormITConsultantCourse,
} from 'components/form'
import { Calendar } from 'components/calendar'
import clsx from 'clsx'
import BrandSlider from 'components/brand-slider'

import dynamic from 'next/dynamic'
const Map = dynamic(() => import('components/map'), {
  ssr: false,
})

import advantage1 from 'public/images/training/icons/advantage-1.png'
import advantage2 from 'public/images/training/icons/advantage-2.png'
import advantage3 from 'public/images/training/icons/advantage-3.png'

import businessmans from 'public/images/training/businessmans.jpg'
import corporate from 'public/images/training/corporate.jpg'
import standard1 from 'public/images/training/brands/pearson.png'
import standard2 from 'public/images/training/brands/red-hat.png'

const options = [
  { value: 'authorizedCourses', label: 'Авторизованные курсы' },
  { value: 'generalCourses', label: 'Общие курсы' },
]

const TrainingHome = (props) => {
  const { t, apiKey } = props
  const [activeTab, setActiveTab] = useState('1')

  const toggle = (tab) => activeTab !== tab && setActiveTab(tab)

  return (
    <Layout>
      <section className='big-dark training text-white pb-5'>
        <div className='container'>
          <h1 className='title text-uppercase m-auto pb-4'>
            {t('trainingPage.title')}
          </h1>
          <h2 className='vacancy__title mb-5'>
            <span className='d-block'>
              {t('trainingPage.subtitle')}
            </span>
          </h2>
          <ul className='d-flex advantage-list flex-column flex-sm-row'>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage1} alt='' className='img-responsive' />
              <div className='mt-3'>
                {t('trainingPage.advantage1')}
              </div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage2} alt='' className='img-responsive' />
              <div className='mt-3'>
              {t('trainingPage.advantage2')}
              </div>
            </li>
            <li className='advantage-item d-flex d-sm-inline-block'>
              <img src={advantage3} alt='' className='img-responsive' />
              <div className='mt-3'>{t('trainingPage.advantage3')}</div>
            </li>
          </ul>
        </div>
      </section>
      <section className='section pt-4'>
        <div className='container'>
          <Breadcrumbs data={[{ name: t('trainingPage.breadcrumbs') }]} mode='white' />
          <div className='row'>
            <div className='col-md-6 mb-4'>
              <img className='img-responsive' src={businessmans} alt='' />
            </div>
            <div className='col-md-6 mb-4'>
              <img className='img-responsive' src={corporate} alt='' />
            </div>
          </div>
          <div className='subtitle mb-4'>
            {t('trainingPage.brandTitle')}
          </div>
          <div className='row'>
            <div className='col-md-12 black-slider'>
              <BrandSlider />
            </div>
          </div>
        </div>
      </section>
      <section className='section section__for-from-course course-certifications mt-5 text-white'>
        <div className='container'>
          <div className='row'>
            <div className='col-md-7'>
              <h2 className='title title__section text-white fz-30 mb-1'>
                {t(
                  'trainingPage.sendStaffToStudy'
                )}
              </h2>
              <p className='subtitle'>
                {t(
                  'trainingPage.staffWithKnowledge'
                )}
              </p>
              <ul className='list my-4'>
                <li className='list-item fz-18 mb-3'>
                {t(
                  'trainingPage.listItem1'
                )}
                </li>
                <li className='list-item fz-18 mb-3'>
                {t(
                  'trainingPage.listItem2'
                )}
                </li>
                <li className='list-item fz-18'>
                {t(
                  'trainingPage.listItem3'
                )}
                </li>
              </ul>
              <Button color='link' className='p-0 mb-4'>
                <u>{t('trainingPage.howToStudy')}</u>
              </Button>
              <p>{t('trainingPage.getOrderInEmail')}
                <a href='mailto:nla@muk.ua'>nla@muk.ua</a> или
                <a href='mailto:training@muk.ua'>training@muk.ua</a> для Яцко
                Наталии <br />
                {t('trainingPage.callByPhone')}
                <a href='tel:+38 044 492 29229'>+38 044 492 29229</a>, внут.
                492. <br />
                {t('trainingPage.leaveOrderInForm')}
              </p>
            </div>
            <div className='col-md-5 justify-content-center align-items-center d-none d-md-flex'>
              <img src={standard1} alt='' className='img-responsive mx-2' />
              <img src={standard2} alt='' className='img-responsive mx-2' />
            </div>
            <div className='col-md-12 col-12'>
              <form className='form-phone m'>
                <InputGroup className='align-items-center'>
                  <Input placeholder={t('form.enterPhone')} className='input' />
                  <InputGroupAddon addonType='append'>
                    <ButtonRed type='button'>
                      {t('form.btnSubmit')}
                    </ButtonRed>
                  </InputGroupAddon>
                  <p>
                    {t('form.coupon-form.couponSent')}
                  </p>
                </InputGroup>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section className='section mt-5 pb-5'>
        <div className='container'>
          <h3 className='font-weight-bold'>{t('trainingPage.hotCourses')}</h3>
          <div className='subtitle fz-18'>
            {t('trainingPage.coursesWithSale' )}
          </div>
          <div className='row mt-5'>
            {courseList.map((item, index) => (
              <div className='col-md-6 col-12' key={index}>
                <CourseCard {...item} t={t} />
              </div>
            ))}
          </div>
          <h3 className='font-weight-bold mt-5'>
            <span className='text-danger'>{t('trainingPage.newCourses' )}</span> / {t('trainingPage.hitCourses')}
          </h3>
          <div className='row mt-5'>
            {courseList.map((item, index) => (
              <div className='col-md-6 col-12' key={index}>
                <CourseCard {...item} t={t} />
              </div>
            ))}
          </div>
          <div className='text-center mt-3'>
            <ButtonRed>{t('common.showMore')}</ButtonRed>
          </div>
        </div>
      </section>

      <section className='section mt-5'>
        <div className='container'>
          <h3 className='font-weight-bold mb-3'>
          {t('trainingPage.getCoursesForYou')}
          </h3>
          <SearchBlock
            options={options}
            input={{ placeholder: t('trainingPage.enterCourseName') }}
            boxShadow='none'
          />
          <div className='row mt-3'>
            {categoryCourseList.map((item, index) => (
              <div className='col-md-4' key={index}>
                <div className='d-flex category-course-card my-4'>
                  <div className='img-wrapper'>
                    <img
                      src={item.image}
                      alt=''
                      width='130'
                      className='img-responsive'
                    />
                  </div>
                  <div className='text-wrapper ml-3'>
                    <div className='font-weight-bold'>{item.name}</div>
                    <ul className='mt-3'>
                      {item.list.map((item, index) => (
                        <li key={index} className='mb-1'>
                          {item}
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>

      <section className='section mt-5 calendar-wrapper'>
        <div className='container py-5'>
          <h3 className='font-weight-bold mt-3'>
            {t('trainingPage.monthlySchedule')}
          </h3>
          <p className='mb-5'>
            {t('trainingPage.selectDay')}
          </p>
          <div className='mb-5'>
            <Calendar t={t} />
          </div>
        </div>
      </section>

      <section className='section mt-5'>
        <div className='container'>
          <h3 className='font-weight-bold'>{t('trainingPage.newsITMarket')}</h3>
          <div className='row'>
            {newsItMarketStudy.map((item, index) => (
              <div className='col-md-4' key={index}>
                <Card body className='my-3'>
                  <div className='title font-weight-bold'>{item.name}</div>
                  <div className='date'>
                    {t('Дата')}: {item.date}
                  </div>
                  <div className='desctiption mt-2'>{item.description}</div>
                </Card>
              </div>
            ))}
          </div>
          <div className='text-center mt-4'>
            <ButtonRed>{t('common.showMore')}</ButtonRed>
          </div>
        </div>
      </section>
      <div className='container mt-5 pb-5'>
        <FormITConsultantCourse t={t} />
      </div>

      <section className='section mt-5'>
        <CouponForm t={t} />
      </section>

      <section className='section section__for-from-course training-center-map mt-5 text-white'>
        <div className='container'>
          <h2 className='title title__section text-white fz-30'>
            {t('trainingPage.contactsTrainingCenters')}
          </h2>
          <div>
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={clsx({ active: activeTab === '1' })}
                  onClick={() => toggle('1')}
                >
                  Украина
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={clsx({ active: activeTab === '2' })}
                  onClick={() => toggle('2')}
                >
                  Азербайджан
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={clsx({ active: activeTab === '3' })}
                  onClick={() => toggle('3')}
                >
                  Беларусь
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={clsx({ active: activeTab === '4' })}
                  onClick={() => toggle('4')}
                >
                  Грузия
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={activeTab}>
              <TabPane tabId='1'>
                <Row className='my-5'>
                  <Col sm='5' className='mb-3'>
                    Марина Сокотунова / Maryna Sokotunova
                    <br />
                    Директор Учебного Центра/Training Center Director
                    <br />
                    e-mail: <a href='mailto:mso@muk.ua'>mso@muk.ua</a>
                    <br />
                    <a href='tel:+380444922929'>+38044 492-29-29</a>, ext. 491
                  </Col>
                  <Col sm='5' className='mb-3'>
                    Марина Сокотунова / Maryna Sokotunova
                    <br />
                    Директор Учебного Центра/Training Center Director
                    <br />
                    e-mail: <a href='mailto:mso@muk.ua'>mso@muk.ua</a>
                    <br />
                    <a href='tel:+380444922929'>+38044 492-29-29</a>, ext. 491
                  </Col>
                  <Col sm='5' className='mb-3'>
                    Марина Сокотунова / Maryna Sokotunova
                    <br />
                    Директор Учебного Центра/Training Center Director
                    <br />
                    e-mail: <a href='mailto:mso@muk.ua'>mso@muk.ua</a>
                    <br />
                    <a href='tel:+380444922929'>+38044 492-29-29</a>, ext. 491
                  </Col>
                  <Col sm='5' className='mb-3'>
                    Марина Сокотунова / Maryna Sokotunova
                    <br />
                    Директор Учебного Центра/Training Center Director
                    <br />
                    e-mail: <a href='mailto:mso@muk.ua'>mso@muk.ua</a>
                    <br />
                    <a href='tel:+380444922929'>+38044 492-29-29</a>, ext. 491
                  </Col>
                </Row>
                <Row>
                  <Col sm='4' className='mb-4'>
                    <div className='font-weight-bold mb-3'>
                      Украина, г. Киев, ул. Донецкая, 16/2 03151{' '}
                    </div>
                    <div className='d-flex'>
                      <span className='d-inline-block mr-3'>телефон:</span>
                      <span>
                        <a href='tel:+38 (044) 594-98-98'>
                          +38 (044) 594-98-98
                        </a>
                        <br />
                        <a href='tel:+38 (044) 492-29-29'>
                          +38 (044) 492-29-29
                        </a>
                      </span>
                    </div>
                    email: <a href='mailto:info@muk.ua'>info@muk.ua</a>
                  </Col>
                  <Col sm='8'>
                    <Map apiKey={apiKey} />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='2'>
                <Row>
                  <Col sm='12'>
                    <Map
                      apiKey={apiKey}
                      initialCenter={{ lat: '40.384839', lng: '49.839964' }}
                      zoom={15}
                    />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='3'>
                <Row>
                  <Col sm='12'>
                    <Map
                      apiKey={apiKey}
                      initialCenter={{ lat: '53.9055612', lng: '27.5459915' }}
                      zoom={15}
                    />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId='4'>
                <Row>
                  <Col sm='12'>
                    <Map
                      apiKey={apiKey}
                      initialCenter={{ lat: '41.7323738', lng: '44.698428' }}
                      zoom={12}
                    />
                  </Col>
                </Row>
              </TabPane>
            </TabContent>
          </div>
        </div>
      </section>
    </Layout>
  )
}

TrainingHome.getInitialProps = async () => ({
  namespacesRequired: ['common'],
  apiKey: process.env.GOOGLE_API_KEY,
})

export default withTranslation('common')(TrainingHome)
