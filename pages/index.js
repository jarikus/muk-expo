import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation, Link, Trans } from 'utils/with-i18next'
import { tags, catalogItems } from 'utils/data'
import map from 'public/images/maps.png'
import business from 'public/images/business.png'
import mapLoc from 'public/images/maps-location.png'
import humans from 'public/images/human.png'
import { WorldMap } from 'components/world-map'
import { Tabs, Tab, TabList, TabPanel, resetIdCounter } from 'react-tabs'
import FormConsultation from 'components/form/formConsultation'
import FormCommercialProposal from 'components/form/formCommercialProposal'
import { ButtonRed } from 'components/form'
import Certificates from 'components/certificates'
import shapesPhoto from 'public/images/action-photo.jpg'
import manager from 'public/images/manager.png'
import { CONTACTS } from '../constants/common'

function Home({ t, news, promo, cache }) {
  return (
    <Layout t={t} header='home'>
      <div className='section section__one'>
        <WorldMap t={t} />
        <div className='content'>
          <h1 className='title title__section title__section--white'>
            {t('s1.t')}
          </h1>
          <h5 className='title title__description title__description--white mb-0'>
            {t('s1.d')}
          </h5>
        </div>
      </div>
      <div className='section section__two'>
        <div className='container'>
          <div className='advantages flex'>
            <div>
              <div className='advantage'>
                <div className='advantage__ico'>
                  <img src={map} alt='' />
                </div>
                <div className='advantage__body'>
                  <div className='advantage__title'>
                    <Trans i18nKey='s2.advantages.a1.t'>
                      Более <span>20 лет</span> на рынке.
                    </Trans>
                  </div>
                  <div className='advantage__description'>
                    {t('s2.advantages.a1.d')}
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className='advantage'>
                <div className='advantage__ico'>
                  <img src={business} alt='' />
                </div>
                <div className='advantage__body'>
                  <div className='advantage__title'>
                    {/* tag cannot be first element in components Trans*/}
                    <Trans i18nKey='s2.advantages.a2.t'>
                      Более <span>70+</span> вендоров
                    </Trans>
                  </div>
                  <div className='advantage__description'>
                    {t('s2.advantages.a2.d')}
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className='advantage'>
                <div className='advantage__ico'>
                  <img src={mapLoc} alt='' />
                </div>
                <div className='advantage__body'>
                  <div className='advantage__title'>
                    <Trans i18nKey='s2.advantages.a3.t'>
                      Присутствие <span>в 15 странах</span>
                    </Trans>
                  </div>
                  <div className='advantage__description'>
                    {t('s2.advantages.a3.d')}
                  </div>
                </div>
              </div>
            </div>
            <div>
              <div className='advantage'>
                <div className='advantage__ico'>
                  <img src={humans} alt='' />
                </div>
                <div className='advantage__body'>
                  <div className='advantage__title'>
                    <Trans i18nKey='s2.advantages.a4.t.text1'>
                      Более <span>2000 партнеров</span>
                    </Trans>
                  </div>
                  <div className='advantage__description'>
                    {t('s2.advantages.a4.d')}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <h2 className='title font-weight-bold mb-4'>{t('s2.t')}</h2>
          <h5 className='title title__description mb-1'>{t('s2.d')}</h5>
          <h5 className='title title__description'>{t('s2.text1')}</h5>
          <div className='tabs-presentation'>
            <Tabs>
              <TabList>
                <Tab>{t('s2.tabs.tab1.t')}</Tab>
                <Tab>{t('s2.tabs.tab2.t')}</Tab>
                <Tab>{t('s2.tabs.tab3.t')}</Tab>
                <Tab>{t('s2.tabs.tab4.t')}</Tab>
              </TabList>
              <TabPanel>
                <div className='tab__body'>
                  <div className='tab__description'>{t('s2.tabs.tab1.d')}</div>
                  <div className='tab__description'>
                    {t('s2.tabs.tab1.list')}
                  </div>
                  <div className='tab__list row'>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>1</div>
                      <div className='tab__item'>{t('s2.tabs.tab1.text1')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>2</div>
                      <div className='tab__item'>{t('s2.tabs.tab1.text2')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>3</div>
                      <div className='tab__item'>{t('s2.tabs.tab1.text3')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>4</div>
                      <div className='tab__item'>{t('s2.tabs.tab1.text4')}</div>
                    </div>
                  </div>
                  <div className='tab__description'>
                    <strong>{t('s2.tabs.tab1.text5')}</strong>
                  </div>
                  <div className='tab__description mb-3'>
                    <strong>{t('s2.tabs.tab1.text6')}</strong>
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab1.text8')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab1.text9')}
                  </div>
                  <div className='tab__description mb-3'>
                    <strong>{t('s2.tabs.tab1.text10')}</strong>
                  </div>
                  <div className='tab__list row'>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>1</div>
                      <div className='tab__item'>
                        {t('s2.tabs.tab1.text11')}
                      </div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>2</div>
                      <div className='tab__item'>
                        {t('s2.tabs.tab1.text12')}
                      </div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>3</div>
                      <div className='tab__item'>
                        {t('s2.tabs.tab1.text13')}
                      </div>
                    </div>
                  </div>
                  <div className='tab__description'>
                    <Trans i18nKey='s2.tabs.tab1.text14'>
                      Для
                      <a href={'https://b2b.muk.ua/signup'} target='_blank'></a>
                    </Trans>
                  </div>
                  <div>
                    <FormConsultation
                      t={t}
                      title={t('s2.tabs.tab1.formTitle')}
                      image={manager}
                      inputs={{ phone: true, email: true, description: true }}
                      sendEmail=''
                    />
                  </div>
                </div>
              </TabPanel>
              <TabPanel>
                <div className='tab__body'>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab2.d')}
                  </div>
                  <div className='tab__description'>
                    {t('s2.tabs.tab2.list')}
                  </div>
                  <div className='tab__list row'>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>1</div>
                      <div className='tab__item'>{t('s2.tabs.tab2.text1')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>2</div>
                      <div className='tab__item'>{t('s2.tabs.tab2.text2')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>3</div>
                      <div className='tab__item'>{t('s2.tabs.tab2.text3')}</div>
                    </div>
                  </div>
                  <div className='tab__description font-italic font-weight-bold'>
                    {t('s2.tabs.tab2.text4')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab2.text5')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab2.text6')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab2.text7')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab2.text8')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab2.text9')}
                  </div>
                  <div className='tab__description'>
                    {t('s2.tabs.tab2.text10')}
                  </div>
                  <div>
                    <FormConsultation
                      t={t}
                      title={t('s2.tabs.tab2.formTitle')}
                      image={manager}
                      inputs={{ phone: true, email: true, description: true }}
                      sendEmail=''
                    />
                  </div>
                </div>
              </TabPanel>
              <TabPanel>
                <div className='tab__body'>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab3.d')}
                  </div>
                  <div className='tab__description'>
                    {t('s2.tabs.tab3.list')}
                  </div>
                  <div className='tab__list row'>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>1</div>
                      <div className='tab__item'>{t('s2.tabs.tab3.text1')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>2</div>
                      <div className='tab__item'>{t('s2.tabs.tab3.text2')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>3</div>
                      <div className='tab__item'>{t('s2.tabs.tab3.text3')}</div>
                    </div>
                  </div>
                  <div className='tab__description font-weight-bold'>
                    {t('s2.tabs.tab3.text4')}
                  </div>
                  <div className='tab__description font-weight-bold'>
                    {t('s2.tabs.tab3.text5')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab3.text6')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab3.text7')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab3.text8')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab3.text9')}
                  </div>
                  <div className='tab__description'>
                    {t('s2.tabs.tab3.text10')}
                  </div>
                  <div>
                    <FormConsultation
                      t={t}
                      title={t('s2.tabs.tab3.formTitle')}
                      image={manager}
                      inputs={{ phone: true, email: true, description: true }}
                      sendEmail=''
                    />
                  </div>
                </div>
              </TabPanel>
              <TabPanel>
                <div className='tab__body'>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab4.d')}
                  </div>
                  <div className='tab__description'>
                    {t('s2.tabs.tab4.list')}
                  </div>
                  <div className='tab__list row'>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>1</div>
                      <div className='tab__item'>{t('s2.tabs.tab4.text1')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>2</div>
                      <div className='tab__item'>{t('s2.tabs.tab4.text2')}</div>
                    </div>
                    <div className='col-md-3 col-sm-6 col-12'>
                      <div className='tab__item__index'>3</div>
                      <div className='tab__item'>{t('s2.tabs.tab4.text3')}</div>
                    </div>
                  </div>
                  <div className='tab__description font-weight-bold'>
                    {t('s2.tabs.tab4.text4')}
                  </div>
                  <div className='tab__description font-weight-bold'>
                    {t('s2.tabs.tab4.text5')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab4.text6')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab4.text7')}
                  </div>
                  <div className='tab__description mb-3'>
                    {t('s2.tabs.tab4.text8')}
                  </div>
                  <div className='tab__description mb-3'>
                    <Trans i18nKey='s2.tabs.tab4.text9'>
                      Если
                      <a href={`mailto:${CONTACTS.email1}`}>
                        {{ email: CONTACTS.email1 }}
                      </a>
                    </Trans>
                  </div>
                  <div className='tab__description'>
                    {t('s2.tabs.tab4.text11')}
                  </div>
                  <div>
                    <FormConsultation
                      t={t}
                      title={t('s2.tabs.tab4.formTitle')}
                      image={manager}
                      inputs={{ phone: true, email: true, description: true }}
                      sendEmail=''
                    />
                  </div>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </div>
      </div>
      <div className='section section__three'>
        <div className='container'>
          <h2 className='title title__section title__section--gray'>
            {t('s3.t')}
          </h2>
          <h4 className='title title__subtitle title__description--white'>
            {t('s3.subT')}
          </h4>
          <h5 className='title title__description title__description--white'>
            {t('s3.d')}
          </h5>
          <div className='about-info-list'>
            <div className='line__item line__item--gray'>{t('s3.text1')}</div>
            <div className='line__item line__item--gray'>{t('s3.text2')}</div>
            <div className='line__item line__item--gray'>{t('s3.text3')}</div>
          </div>
          <div className='after-work-steps flex'>
            <div className='after-work-steps__title'>{t('s3.steps.t')}</div>
            <div>
              <div className='step'>
                <div className='step__index'>1</div>
                <div className='step__description'>{t('s3.steps.step1')}</div>
              </div>
            </div>
            <div>
              <div className='step'>
                <div className='step__index'>2</div>
                <div className='step__description'>{t('s3.steps.step2')}</div>
              </div>
            </div>
            <div>
              <div className='step'>
                <div className='step__index'>3</div>
                <div className='step__description'>{t('s3.steps.step3')}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='section section__four'>
        <div className='container'>
          <h2 className='title title__section'>{t('s4.t')}</h2>
          <div className='tabs-direction'>
            <Tabs>
              <TabList>
                <Tab>{t('s4.tabs.tab1.btn')}</Tab>
                <Tab>{t('s4.tabs.tab2.btn')}</Tab>
                <Tab>{t('s4.tabs.tab3.btn')}</Tab>
              </TabList>
              <TabPanel>
                <div className='tab__body'>
                  <div className='tab__title'>{t('s4.tabs.tab1.t')}</div>
                  <div className='line__item-list flex'>
                    <div className='line__item line__item--red'>
                      {t('s4.tabs.tab1.text1')}
                    </div>
                    <div className='line__item line__item--red'>
                      {t('s4.tabs.tab1.text2')}
                    </div>
                  </div>
                  <p>{t('s4.tabs.tab1.content.p1')}</p>
                  <p>{t('s4.tabs.tab1.content.p2')}</p>
                  <p>{t('s4.tabs.tab1.content.p3')}</p>
                  <p>{t('s4.tabs.tab1.content.p4')}</p>
                  <p>{t('s4.tabs.tab1.content.p5')}</p>
                </div>
              </TabPanel>
              <TabPanel></TabPanel>
              <TabPanel></TabPanel>
            </Tabs>
          </div>
        </div>
      </div>
      <div className='section section__five'>
        <div className='container'>
          <h2 className='title title__section title__section--white'>
            {t('s5.t')}
          </h2>
          <h5 className='title title__description title__description--white'>
            {t('s5.d')}
          </h5>
          <div className='tags flex'>
            {tags.map((item, index) => (
              <div key={index}>
                <Link href='/'>
                  <a className='tag'>{item}</a>
                </Link>
              </div>
            ))}
          </div>
          <div className='catalog-items flex nowrap'>
            {catalogItems.map((item, index) => (
              <div key={index}>
                <div className='catalog-item'>
                  <div className='catalog-item__links'>
                    {item.links.map((el, index) => (
                      <div key={index}>{el}</div>
                    ))}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className='section section__six'>
        <div className='container'>
          <h2 className='title title__section'>{t('s6.t')}</h2>
          <h5 className='title title__description'>{t('s6.d')}</h5>
          <div className='cer__title'>{t('s6.certificates.t')}</div>
          <div className='cer__description'>{t('s6.certificates.d')}</div>
          <Certificates />
        </div>
      </div>
      <div className='section section__seven'>
        <div className='container'>
          <FormCommercialProposal t={t} />
        </div>
      </div>
      {promo.list && (
        <div className='section section__eight'>
          <div className='container'>
            <h2 className='title title__section'>{t('s8.t')}</h2>
            <div className='shares'>
              {promo.list.map((item, index) => (
                <div key={index}>
                  <div className='share flex'>
                    <div className='share__preview'>
                      <img src={item.image || shapesPhoto} alt={item.title} />
                    </div>
                    <div className='info-group__body'>
                      <div className='info-group__title'>
                        <Link
                          href={{
                            pathname: '/press/promo/[id]',
                            query: { id: item.id },
                          }}
                          as={`/press/promo/${item.id}`}
                        >
                          <a>{item.title}</a>
                        </Link>
                      </div>
                      <div className='info-group__date'>
                        {t('s8.text1')}
                        {item.date}
                      </div>
                      <div className='info-group__partners'>
                        {item.partners}
                      </div>
                      <div className='info-group__description'>
                        {item.description_short}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <ButtonRed href='/press/promo'>{t('s8.btnSeeMore')}</ButtonRed>
          </div>
        </div>
      )}
      {news.list && (
        <div className='section section__nine'>
          <div className='container'>
            <h2 className='title title__section title__section--white'>
              {t('s9.t')}
            </h2>
            <div className='news flex'>
              {news.list.map((item, index) => (
                <div key={index}>
                  <div className='new flex'>
                    <div className='info-group__body'>
                      <div className='info-group__title'>
                        <Link
                          href={{
                            pathname: '/press/news/[id]',
                            query: { id: item.id },
                          }}
                          as={`/press/news/${item.id}`}
                        >
                          <a>{item.title}</a>
                        </Link>
                      </div>
                      <div className='info-group__date'>
                        {t('s8.text1')}
                        {item.date}
                      </div>
                      <div className='info-group__partners'>
                        {item.partners}
                      </div>
                      <div className='info-group__description'>
                        {item.description_short}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
            <ButtonRed href='/press/news'>{t('s9.btnSeeMore')}</ButtonRed>
          </div>
        </div>
      )}
    </Layout>
  )
}
Home.getInitialProps = async (ctx) => {
  try {
    resetIdCounter()

    const { getNews } = ctx.api.news
    const news = await getNews(ctx, 'news', { limit_count: 6 })
    const promo = await getNews(ctx, 'promo', { limit_count: 3 })
    return {
      namespacesRequired: ['common'],
      news,
      promo,
    }
  } catch (e) {
    console.error(e)
    return {
      namespacesRequired: ['common'],
    }
  }
}

export default withTranslation('common')(Home)
