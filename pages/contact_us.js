import React from 'react'
import { Layout } from 'components/layout'
import { withTranslation } from 'utils/with-i18next'
import Breadcrumbs from 'components/breadcrumbs'

const Contacts = ({ t }) => (
  <Layout>
    <section className='catalog-journals-header' />
    <section className='section section__catalog-journals'>
      <div className='container'>
        <Breadcrumbs
          className='catalog-journals-breadcrumbs'
          data={[{ name: t('contactsPage.breadcrumbs') }]}
        />
        <h1 className='title title__section title__section--catalog-journals'>
          {t('contactsPage.title')}
        </h1>
        <div className='row about'>
          <br />
          <br />
          <br />
          <br />
        </div>
      </div>
    </section>
  </Layout>
)

Contacts.getInitialProps = async () => {
  return {
    namespacesRequired: ['common'],
  }
}

export default withTranslation('common')(Contacts)
