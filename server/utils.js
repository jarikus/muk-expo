const FormData = require('form-data')

const sendResume = async ({ fields, files }) => {
  try {
    const formData = new FormData()

    for (let field in fields) {
      formData.append(field, fields[field])
    }
    if (files.hasOwnProperty('file')) {
      formData.append('file', files['file'].path, files['file'].name)
    }

    const url = 'https://backend.muk.ua/jobsAddResume'

    const resp = await fetch(url, {
      method: 'POST',
      body: formData,
    })

    const json = await resp.json()
    return json
  } catch (error) {
    console.error(error)
  }
}

module.exports.sendResume = sendResume
