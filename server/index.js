const express = require('express')
const next = require('next')
const { createProxyMiddleware } = require('http-proxy-middleware')
const nextI18NextMiddleware = require('next-i18next/middleware').default
const nextI18next = require('../i18n')
const formidable = require('formidable')

const dev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 3030
const app = next({ dev })
const handle = app.getRequestHandler()
const { sendResume } = require('./utils')

const proxyUrl = [
  '/newsOne',
  '/newsList',
  '/vendors',
  '/vendorsList',
  '/jobsList',
  '/jobsOne',
  '/jobsOptions',
  '/jobsAddResume',
  '/feedback',
  '/catalog',
  '/search',
]

;(async () => {
  await app.prepare()
  const server = express()

  if (dev) {
    proxyUrl.forEach((item) => {
      server.use(
        item,
        createProxyMiddleware({
          target: 'https://backend.muk.ua',
          changeOrigin: true,
          secure: false,
        })
      )
    })
  }

  server.use(
    '/upload',
    createProxyMiddleware({
      target: 'https://muk.ua',
      changeOrigin: true,
      secure: false,
    })
  )

  await nextI18next.initPromise
  server.use(nextI18NextMiddleware(nextI18next))

  server.post('/api/resume', (req, res, next) => {
    const form = formidable({ multiples: true })

    form.parse(req, async (err, fields, files) => {
      if (err) {
        next(err)
        return
      }
      const resp = await sendResume({ fields, files })
      res.json({ ...resp })
    })
  })

  server.get('*', (req, res) => handle(req, res))
  server.post('*', (req, res) => handle(req, res))

  await server.listen(port)
  console.log(`> Ready on http://localhost:${port}`)
})()
